<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calendar".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property int $session_id
 * @property string $term
 * @property int $created_by
 * @property string $created_on
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'start_date'], 'required'],
            [['description', 'term'], 'string'],
            [['start_date', 'end_date', 'created_on'], 'safe'],
            [['created_by', 'session_id'], 'integer'],
            [['title'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'session_id' => 'Session',
            'term' => 'Term',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
        ];
    }
}
