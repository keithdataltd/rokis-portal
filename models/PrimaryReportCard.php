<?php

namespace app\models;

use Yii;
use app\models\SchoolClass;
use app\models\Student;
use app\models\Subject;
use app\models\PrimaryReportCardAttributes;
use app\models\ExamResult;
use app\models\ContinuousAssessmentResult;
use app\models\ContinuousAssessmentType;
use app\models\PrimaryReportCard;
use app\models\GeneralSettings;
use app\models\Session;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "primary_report_card".
 *
 * @property int $id
 * @property string $student_id
 * @property string $class_id
 * @property string $session_id
 * @property string $term
 * @property string $approval_status
 * @property int $approved_by
 * @property string $approval_date
 * @property string $creation_date
 * @property int $created_by
 */
class PrimaryReportCard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'primary_report_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'class_id', 'session_id', 'term', 'creation_date'], 'required'],
            [['creation_date', 'approval_date'], 'safe'],
            [['approval_status'], 'string'],
            [['approved_by', 'created_by'], 'integer'],
            [['student_id', 'class_id', 'session_id'], 'string', 'max' => 11],
            [['term'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Student',
            'class_id' => 'Class',
            'session_id' => 'Session',
            'term' => 'Term',
            'approval_status' => 'Approval Status',
            'approved_by' => 'Approved By',
            'approval_date' => 'Approval Date',
            'created_by' => 'Created By',
            'creation_date' => 'Creation Date',
        ];
    }
    
    public function fetchReportCard($reportCardId)
    {
        $reportCard = self::findOne($reportCardId);
        $reportCardAttributes = PrimaryReportCardAttributes::findOne(['primary_report_card_id' => $reportCardId]);
        $examResults = ExamResult::findAll(['report_card_id' => $reportCardId]);
        $reportCardResults = [];
        $incompleteCAs = [];
        $caTypes = ContinuousAssessmentType::find()->all();
        $reportCardCA = [];
        $allCAtotal = 0.00;
        $allExamTotal = 0.00;
        $completeness = 1;
        
        //Aggregate results from each subject
        foreach($examResults as $examResult)
        {
            $subjectTotalScore = Subject::computeSubjectTotalScore($examResult->subject_id, $reportCard->class_id, $reportCard->term, $reportCard->session_id);
            $numberOfStudentsInClass = self::getNumberOfStudentsInClass($reportCard->class_id, $reportCard->session_id, $reportCard->term);
            $ca = $this->getSubjectCAScores($reportCardId, $examResult->subject_id);
            $reportCardResults[$examResult->subject_id] = [
                "ca" => $ca,
                "exam" => $examResult->obtained_percentage, 
                "class_average_mark" => number_format($subjectTotalScore/$numberOfStudentsInClass, 2),
                "subject_position" => Subject::getStudentSubjectPosition($reportCard->student_id, $examResult->subject_id, $reportCard->class_id, $reportCard->term, $reportCard->session_id),
                "remark" => $this->getGradeRemark($ca['total'] + $examResult->obtained_percentage)
            ];

            //Gather incomplete CAs
            if($reportCardResults[$examResult->subject_id]['ca']['completeness'] == 0)
            {
                $incompleteCAs[$examResult->subject_id] = $reportCardResults[$examResult->subject_id]['ca']['caResults'];
                $completeness = $completeness * 0;
            }

            else
            {
                $allCAtotal += $reportCardResults[$examResult->subject_id]['ca']['total'];
                $allExamTotal += $examResult->obtained_percentage;
                $completeness = $completeness * 1;
            }

            
        }

        $reportCardResults['all_ca_total'] = $allCAtotal;
        $reportCardResults['all_exam_total'] = $allExamTotal;
        $reportCardResults['position_in_class'] = self::getStudentPositionInClass($reportCard->student_id, $reportCard->class_id, $reportCard->session_id, $reportCard->term);
        $reportCardResults['report_card_total'] = $allCAtotal + $allExamTotal;
        $reportCardResults['report_card_average'] = $reportCardResults['report_card_total']/count($examResults);
        $reportCardResults['incomplete_CAs'] = $incompleteCAs;
        $reportCardResults['completeness'] = $completeness;
        $reportCardResults['exam_results'] = $examResults;
        $reportCardResults['attributes'] = $reportCardAttributes;
        $reportCardResults['approval_status'] = $reportCard->approval_status;
        $reportCardResults['student'] = Student::findOne($reportCard->student_id);
        $reportCardResults['term'] = $reportCard->term;
        $reportCardResults['session'] = Session::findOne($reportCard->session_id);
        $reportCardResults['report_card_id'] = $reportCard->id;
        $reportCardResults['class'] = SchoolClass::findOne($reportCard->class_id)->class_name;
        $reportCardResults['number_of_students_in_class'] = self::getNumberOfStudentsInClass($reportCard->class_id, $reportCard->session_id, $reportCard->term);    

        return $reportCardResults;
    }

    public function getSubjectCAScores($reportCardId, $subjectId)
    {
        $caTypes = ContinuousAssessmentType::find()->all();
        $total = 0.0;
        $completeness = 1;
        $caResults = [];

        foreach($caTypes as $caType)
        {   
            $ca = ContinuousAssessmentResult::findOne([
                'report_card_id' => $reportCardId, 
                'subject_id' => $subjectId, 
                'ca_type' => $caType->ca_type
            ]);
            
            if(!empty($ca))
            {
                $total = $total + $ca->obtained_percentage;
                $caResults[$caType->ca_type] =  $ca->obtained_percentage;
                $completeness = $completeness * 1;
            }

            else
            {
                $caResults[$caType->ca_type] = null;
                $completeness = $completeness * 0;
            }
        }

        return [
            'caResults' => $caResults,
            'completeness' => $completeness,
            'total' => $total
        ];

    }

    public static function getNumberOfStudentsInClass($classId, $sessionId, $term)
    {
        $count = self::find()->where([
            'class_id' => $classId,
            'session_id' => $sessionId,
            'term' => $term
        ])->count();

        return $count;
    }

    public static function getReportCardTotals($classId, $sessionId, $term)
    {
        $connection = Yii::$app->getDb();

        $command = $connection->createCommand(
            "SELECT t.student_id, sum(t.obtained_percentage) as totalReportCardScore
            FROM (
                SELECT student_id, obtained_percentage FROM continuous_assessment_result WHERE class_id = $classId AND term = $term AND session_id = $sessionId 
                UNION ALL 
                SELECT student_id, obtained_percentage FROM exam_result WHERE class_id = $classId AND term = $term AND session_id = $sessionId) 
            t 
            GROUP by t.student_id 
            ORDER BY `totalReportCardScore` DESC"
        );

        $result = $command->queryAll();

        return $result;
    }

    public static function getStudentPositionInClass($studentId, $classId, $sessionId, $term)
    {
        $reportCardTotals = self::getReportCardTotals($classId, $term, $sessionId);

        //Rename array indexes to match StudentId
        $reportCardTotals = ArrayHelper::index($reportCardTotals, 'student_id');

        $i = array_search($studentId, array_keys($reportCardTotals));

        //var_dump($reportCardTotals);
        //exit();
        return $i+1;
    }

    public function getGradeRemark($score)
    {
        $remark = '';

        if ($score >= 90) 
        { 
            $remark = 'Distinction'; 
        } 
        else
        { 
            if ($score >= 70 && $score <= 89) 
            { 
                $remark = 'Excellent'; 
            }

            else 
            { 
                if ($score >= 60 && $score <= 69) 
                { 
                    $remark = 'Very Good'; 
                } 
                else 
                { 
                    if ($score >= 50 && $score <= 59)  
                    { 
                        $remark = 'Good'; 
                    } 
                    else 
                    { 
                        $remark = 'Fail'; 
                    } 
                } 
            } 
        }

        return $remark;
    }
}
