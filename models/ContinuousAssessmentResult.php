<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "continuous_assessment_result".
 *
 * @property int $id
 * @property int $report_card_id
 * @property int $student_id
 * @property int $subject_id
 * @property string $ca_type
 * @property string $obtained_score
 * @property string $obtainable_score
 * @property string $obtainable_percentage
 * @property string $obtained_percentage
 * @property string $ca_description
 * @property int $created_by
 * @property string $created_on
 */
class ContinuousAssessmentResult extends \yii\db\ActiveRecord
{
    public $isAbsent;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'report_card_id', 'student_id', 'subject_id', 'class_id', 
                'obtained_score', 'obtainable_score', 'obtainable_percentage', 'obtained_percentage', 
                'ca_type', 'ca_description', 'session_id', 'term',
                'created_on', 'created_by', 'isAbsent'
            ],
            self::SCENARIO_UPDATE => ['obtained_score', 'obtainable_score'],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'continuous_assessment_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_card_id', 'student_id', 'subject_id', 'class_id', 'created_by', 'session_id'], 'integer'],
            [['obtained_score'], 'required', 'message' => 'Please enter score'],
            [['obtained_score'], 'checkRecordUniqueness', 'on' => self::SCENARIO_CREATE],
            ['obtained_score', 'compare', 'compareAttribute' => 'obtainable_score', 'operator' => '<=', 'type' => 'number'],
            [['ca_type', 'ca_description', 'term'], 'string'],
            [['obtained_score', 'obtainable_score', 'obtainable_percentage', 'obtained_percentage'], 'number'],
            [['created_on'], 'safe'],
            ['isAbsent', 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_card_id' => 'Report Card ID',
            'student_id' => 'Student',
            'subject_id' => 'Subject',
            'ca_type' => 'C.A Type',
            'obtained_score' => 'Obtained Score',
            'obtainable_score' => 'Obtainable Score',
            'percentage' => 'Percentage',
            'obtained_percentage' => 'Obtained Percentage',
            'ca_description' => 'C.A Description',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
        ];
    }

    public function checkRecordUniqueness($attribute, $params)
    {
        $result = self::find()
                    ->where([
                        'report_card_id' => $this->report_card_id, 
                        'student_id' => $this->student_id,
                        'subject_id' => $this->subject_id,
                        'ca_type' => $this->ca_type
                        ])
                    ->one();
        
        if(!empty($result))
        {
            $this->addError($attribute, '!Duplicate Score entry');
        }            
    }

}
