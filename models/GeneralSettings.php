<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "general_settings".
 *
 * @property int $id
 * @property int $current_session
 * @property string $current_term
 * @property string $term_starts
 * @property string $term_ends
 */
class GeneralSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'general_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['current_session', 'current_term'], 'required'],
            [['current_session'], 'integer'],
            [['current_term'], 'string'],
            [['term_starts', 'term_ends'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'current_session' => 'Current Session',
            'current_term' => 'Current Term',
            'term_starts' => 'Term Starts',
            'term_ends' => 'Term Ends',
        ];
    }

    public static function getRandomString()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, 8);
    }
}
