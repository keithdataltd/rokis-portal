<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exam_result".
 *
 * @property int $id
 * @property int $report_card_id
 * @property int $student_id
 * @property int $subject_id
 * @property string $obtained_score
 * @property string $obtainable_score
 * @property string $obtained_percentage
 * @property string $obtainable_percentage
 * @property string $teachers_remark
 * @property string $created_on
 * @property int $created_by
 */
class ExamResult extends \yii\db\ActiveRecord
{
    
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => [
                'report_card_id', 'student_id', 'subject_id', 
                'obtained_score', 'obtainable_score', 'obtainable_percentage', 'obtained_percentage', 'session_id', 'term',
                'created_on', 'created_by', 'isAbsent'
            ],
            self::SCENARIO_UPDATE => ['obtained_score', 'obtainable_score'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exam_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['report_card_id', 'student_id', 'subject_id', 'created_on', 'created_by'], 'required'],
            [['id', 'report_card_id', 'student_id', 'subject_id', 'session_id','created_by'], 'integer'],
            [['obtained_score', 'obtainable_score', 'obtained_percentage', 'obtainable_percentage'], 'number'],
            ['obtained_score', 'compare', 'compareAttribute' => 'obtainable_score', 'operator' => '<=', 'type' => 'number'],
            [['obtained_score'], 'checkRecordUniqueness', 'on' => self::SCENARIO_CREATE],
            [['obtained_score'], 'number', 'min' => 0, 'max' => 100 ],
            [['obtainable_score'], 'number', 'min' => 1, 'max' => 100 ],
            [['teachers_remark', 'term'], 'string'],
            [['created_on'], 'safe'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_card_id' => 'Report Card ID',
            'student_id' => 'Student',
            'subject_id' => 'Subject',
            'obtained_score' => 'Obtained Score',
            'obtainable_score' => 'Obtainable Score',
            'obtained_percentage' => 'Obtained Percentage',
            'obtainable_percentage' => 'Obtainable Percentage',
            'teachers_remark' => 'Teacher\'s Remark',
            'session_id' => 'Session',
            'term' => 'Term',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
        ];
    }

    public function checkRecordUniqueness($attribute, $params)
    {
        $result = self::find()
                    ->where([
                        'report_card_id' => $this->report_card_id, 
                        'student_id' => $this->student_id,
                        'subject_id' => $this->subject_id
                        ])
                    ->one();
        
        if(!empty($result))
        {
            $this->addError($attribute, '!Duplicate Score entry');
        }            
    }

}
