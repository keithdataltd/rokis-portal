<?php

namespace app\models;

use Yii;
use app\models\Subject;

/**
 * This is the model class for table "primary_report_card_result".
 *
 * @property int $id
 * @property int $primary_report_card_id
 * @property int $subject_id
 * @property string $first_half_marks
 * @property string $second_half_marks
 * @property string $final_marks
 * @property int $position
 * @property string $teacher_remarks
 * @property int $student_id
 * @property int $class_id
 * @property int $session_id
 * @property int $term
 * @property string $creation_date
 */
class PrimaryReportCardResult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'primary_report_card_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject_id', 'first_half_marks', 'second_half_marks'], 'required'],
            [['primary_report_card_id', 'subject_id', 'position', 'student_id', 'class_id', 'session_id', 'term'], 'integer'],
            [
                ['first_half_marks'], 
                'number',
                'min' => 0,
                'max' => 40,             
            ],
            [
                ['second_half_marks'], 
                'number',
                'min' => 0,
                'max' => 60,             
            ],
            [['final_marks'], 'number'],
            [['teacher_remarks'], 'string'],
            [['creation_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'primary_report_card_id' => 'Primary Report Card ID',
            'subject_id' => 'Subject ID',
            'first_half_marks' => 'First Half Marks',
            'second_half_marks' => 'Second Half Marks',
            'final_marks' => 'Final Marks',
            'position' => 'Position',
            'teacher_remarks' => 'Teacher Remarks',
            'student_id' => 'Student ID',
            'class_id' => 'Class ID',
            'session_id' => 'Session ID',
            'term' => 'Term',
            'creation_date' => 'Creation Date',
        ];
    }

    public static function getSubjectClassAverage($subjectId, $classId, $sessionId, $term)
    {
        $entries = self::findAll([
            'subject_id' => $subjectId, 
            'class_id' => $classId, 
            'session_id' => $sessionId,
            'term' => $term]);
        
        $entriesCount = count($entries);
        
        $total = 0.0;
        foreach ($entries as $result) 
        {
            $total = $total + $result->final_marks;
        }

        return $total/$entriesCount;
    }

    public static function getStudentSubjectPosition($studentId, $subjectId, $classId, $sessionId, $term)
    {
        $index = 0;
        $totalScores = self::find()->where([
            'subject_id' => $subjectId, 
            'class_id' => $classId, 
            'session_id' => $sessionId,
            'term' => $term])->orderBy(['final_marks' => SORT_DESC])->all();

           $i = 1;
           foreach ($totalScores as $score) 
           {
               if($score['student_id'] == $studentId) return $i;
               else $i++;
           }       
        
    }

    public static function getStudentPosition($studentId, $classId, $sessionId, $term )
    {
        $totalScores = self::find()
            ->select('student_id, sum(final_marks) as total_score')
            ->where([
                'class_id' => $classId, 
                'session_id' => $sessionId,
                'term' => $term])            
            ->groupBy('student_id')
            ->orderBy(['final_marks' => SORT_DESC])->all();

           $i = 1;
           foreach ($totalScores as $score) 
           {
               if($score['student_id'] == $studentId) return $i;
               else $i++;
           }
    }

    public static function getNumberOfStudentsInClass($classId, $sessionId, $term)
    {
        $studentsInClass = self::find()
            ->select('student_id')
            ->where([
                'class_id' => $classId, 
                'session_id' => $sessionId,
                'term' => $term])            
            ->distinct()->count();

        return $studentsInClass;
    }

    public static function getReportCardTotal($reportCardId)
    {
       return self::find()->where(['primary_report_card_id' => $reportCardId])->sum('final_marks');
    }

    public static function getReportCardAverage($reportCardId)
    {
       return self::find()->where(['primary_report_card_id' => $reportCardId])->average('final_marks');
    }
}
