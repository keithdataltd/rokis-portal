<?php

namespace app\models;

use Yii;
use app\models\ParentStudent;
use app\models\SchoolClass;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "student".
 *
 * @property int $id
 * @property string $matric_number
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $address
 * @property string $sex
 * @property string $nationality
 * @property string $place_of_birth
 * @property string $state_of_origin
 * @property string $lga
 * @property string $religion
 * @property string $english_at_home
 * @property string $other_spoken_language
 * @property string $special_interests
 * @property string $position_in_family
 * @property string $carrying_instruction
 * @property string $medical_information
 * @property int $current_class
 * @property string $status
 * @property string $picture
 * @property string $created_on
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matric_number', 'first_name', 'last_name', 'date_of_birth', 'address', 'sex', 'nationality', 'religion', 'english_at_home', 'current_class'], 'required'],
            [['matric_number'], 'unique'],
            [['date_of_birth', 'created_on'], 'safe'],
            [['address', 'sex', 'english_at_home', 'special_interests', 'position_in_family', 'carrying_instruction', 'medical_information', 'status', 'picture'], 'string'],
            [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['current_class'], 'integer'],
            [['matric_number', 'first_name', 'middle_name', 'last_name'], 'string', 'max' => 20],
            [['nationality'], 'string', 'max' => 2],
            [['place_of_birth', 'religion', 'other_spoken_language'], 'string', 'max' => 100],
            [['state_of_origin'], 'string', 'max' => 30],
            [['lga'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matric_number' => 'Student Number',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'date_of_birth' => 'Date Of Birth',
            'address' => 'Address',
            'sex' => 'Gender',
            'nationality' => 'Nationality',
            'place_of_birth' => 'Place Of Birth',
            'state_of_origin' => 'State Of Origin',
            'lga' => 'L.G.A',
            'religion' => 'Religion',
            'english_at_home' => 'English Spoken At Home?',
            'other_spoken_language' => 'Other Spoken Language',
            'special_interests' => 'Special Interests',
            'position_in_family' => 'Position In Family',
            'carrying_instruction' => 'Carrying Instruction',
            'medical_information' => 'Medical Information',
            'current_class' => 'Current Class',
            'status' => 'Status',
            'picture' => 'Picture',
            'created_on' => 'Created On',
        ];
    }

    public function getParents()
    {
        $parents = ParentStudent::findAll(['student_id' => $this->id]);
        return $parents;
    }

    public function getFullName()
    {
        return ucwords($this->first_name .' '. $this->middle_name .' '. $this->last_name);
    }

    public function getClassName()
    {
        return SchoolClass::findOne($this->current_class)->class_name;
    }

    public static function getNurseryStudents()
    {
        return self::findAll(['current_class' => [1,2,3,4,5]]);
    }

    public static function getNurseryStudentsDropDownList($classId = null)
    {
        
        if($classId == null)
        {
            //Select all Primary classes
            $class = [1,2,3,4,5];
        } 

        else $class = $classId;

        return ArrayHelper::map(self::find()->where([ 'current_class' => $class ])->all(),
            'id',
            function($model){
                $class = SchoolClass::findOne($model->current_class)->class_name;
                return $model->getFullName() . " ({$class}) ";
            }
        ); 
        
        
    }

    public static function getPrimaryStudents()
    {
        return self::findAll(['current_class' => [6,7,8,9,10]]);
    }

    public static function getPrimaryStudentsDropDownList($classId = null)
    {
        
        if($classId == null)
        {
            //Select all Primary classes
            $class = [6,7,8,9,10];
        } 

        else $class = $classId;

        return ArrayHelper::map(self::find()->where([ 'current_class' => $class ])->all(),
            'id',
            function($model){
                $class = SchoolClass::findOne($model->current_class)->class_name;
                return $model->getFullName() . " ({$class}) ";
            }
        ); 
        
        
    }

    public static function getStudentCount()
    {
        return self::find()->count();
    }

    public static function getClassStudents($classId)
    {
        return self::findAll(['current_class' => $classId]);
    }

    /**
     * {@inheritdoc}
     */
    public function getMyReportCards()
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("(SELECT 
            nrc.id as rcId, 
            nrc.student_id as stdId, 
            nrc.term as _term, 
            nrc.session as sessionId,
            s.session_title as sessionTitle,
            nrc.class as classId,
            c.class_name as className,
            'NUR' as reportCardType,
            nrc.approval_status as approvalStatus
            FROM 
            nursery_report_card nrc
            INNER JOIN `rokis_portal`.`session` s on nrc.session = s.id
            INNER JOIN `rokis_portal`.`class` c on nrc.class = c.id
            WHERE student_id = $this->id
            GROUP BY classId
            ORDER BY sessionId, _term)
            UNION
            (SELECT 
            prc.id as rcId, 
            prc.student_id as stdId, 
            prc.term as _term, 
            prc.session_id as sessionId,
            s.session_title as sessionTitle,
            prc.class_id as classId,
            c.class_name as className,
            'PRI' as reportCardType,
            prc.approval_status as approvalStatus
            FROM 
            primary_report_card prc
            INNER JOIN `rokis_portal`.`session` s on prc.session_id = s.id
            INNER JOIN `rokis_portal`.`class` c on prc.class_id = c.id
            WHERE student_id = $this->id
            GROUP BY classId
            ORDER BY sessionId, _term)");

        $result = $command->queryAll();

        return $result;
        
    }

    

}
                                                                                                                                                                                                                                            