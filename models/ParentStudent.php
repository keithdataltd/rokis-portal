<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parent_student".
 *
 * @property int $parent_id
 * @property int $student_id
 * @property string $relationship
 * @property string $creation_date
 */
class ParentStudent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parent_student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'student_id', 'relationship'], 'required'],
            [['parent_id', 'student_id'], 'integer'],
            [['creation_date'], 'safe'],
            [['relationship'], 'string', 'max' => 50],
            [['parent_id', 'student_id'], 'checkDuplicate']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'parent_id' => 'Parent',
            'student_id' => 'Student',
            'relationship' => 'Relationship',
            'creation_date' => 'Creation Date',
        ];
    }

    public function checkDuplicate($attribute, $params)
    {
        $parentChildRelationship = self::find()->where(['parent_id'=>$this->parent_id, 'student_id'=>$this->student_id])->one();
        if (isset($parentChildRelationship) && $parentChildRelationship!=null)
            $this->addError($attribute, 'This child and parent are already linked.');
    }

    public static function primaryKey()
    {
        return [
            'parent_id',
            'student_id',
        ];
    }
}
