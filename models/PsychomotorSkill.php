<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "psychomotor_skill".
 *
 * @property int $id
 * @property string $skill_title
 * @property string $skill_description
 * @property string $creation_date
 */
class PsychomotorSkill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'psychomotor_skill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['skill_title'], 'required'],
            [['skill_description'], 'string'],
            [['creation_date'], 'safe'],
            [['skill_title'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'skill_title' => 'Skill Title',
            'skill_description' => 'Skill Description',
            'creation_date' => 'Creation Date',
        ];
    }
}
