<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NurseryReportCard;

/**
 * NurseryReportCardSearch represents the model behind the search form of `app\models\NurseryReportCard`.
 */
class NurseryReportCardSearch extends NurseryReportCard
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'student_id', 'total_attendance', 'days_absent', 'promoted_to', 'session', 'class'], 'integer'],            
            [['term', 'approval_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NurseryReportCard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'student_id' => $this->student_id,            
            'session' => $this->session,
            'class' => $this->class,
            'approval_status' => $this->approval_status
        ]);

        $query->andFilterWhere(['like', 'term', $this->term]);

        return $dataProvider;
    }
}
