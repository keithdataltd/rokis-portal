<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "class_term_history".
 *
 * @property int $id
 * @property string $term
 * @property int $session_id
 * @property string $start_date
 * @property string $end_date
 * @property string $next_term_start_date
 * @property string $creation_date
 */
class ClassTermHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'class_term_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['term', 'session_id'], 'required'],
            [['term'], 'string'],
            [['session_id', 'times_school_opened'], 'integer'],
            [['start_date', 'end_date', 'next_term_start_date', 'creation_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'term' => 'Term',
            'session_id' => 'Session',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'next_term_start_date' => 'Next Term Start Date',
            'times_school_opened' => 'No. of Times School Opened',
            'creation_date' => 'Creation Date',
        ];
    }
}
