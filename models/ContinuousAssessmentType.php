<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "continuous_assessment_type".
 *
 * @property string $ca_type
 * @property string $ca_title
 * @property string $ca_percentage
 */
class ContinuousAssessmentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'continuous_assessment_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ca_type', 'ca_title', 'ca_percentage'], 'required'],
            [['ca_percentage'], 'number'],
            [['ca_type'], 'string', 'max' => 15],
            [['ca_title'], 'string', 'max' => 250],
            [['ca_type'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ca_type' => 'C.A Type',
            'ca_title' => 'C.A Title',
            'ca_percentage' => 'C.A Percentage',
        ];
    }

    public static function getCaTypesList()
    {
        return ArrayHelper::map(self::find()->all(), 'ca_type', 'ca_title');
    }
}
