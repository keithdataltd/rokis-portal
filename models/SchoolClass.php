<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "class".
 *
 * @property int $id
 * @property string $class_name
 * @property int $class_division
 * @property int $class_teacher_id
 * @property int $assistant_class_teacher_id
 */
class SchoolClass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'class';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['class_name', 'class_division'], 'required'],
            [['class_division', 'class_teacher_id', 'assistant_class_teacher_id'], 'integer'],
            [['class_name'], 'string', 'max' => 40],
            [['class_teacher_id'], 'duplicateClassTeacherCheck']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_name' => 'Class Name',
            'class_division' => 'Class Division',
            'class_teacher_id' => 'Class Teacher',
            'assistant_class_teacher_id' => 'Assistant Class Teacher'
        ];
    }

    public static function getClassesList()
    {
        $classesList = self::find()->all();
        $classesList = ArrayHelper::map($classesList, 'id', 'class_name');
        return $classesList;
    }

    public static function getClassTimetable($classId)
    {
        $timetable = '';
        $title = '';

        switch ($classId) 
        {
            case '3':
                $timetable = '@app/views/timetables/pre-nursery-timetable.php';
                $title = 'Pre-nursery Timetable';
                break;
            
            case '4':
                $timetable = '@app/views/timetables/nursery-1-timetable.php';
                $title = 'Nursery One Timetable';

            case '5':
                $timetable = '@app/views/timetables/nursery-2-timetable.php';
                $title = 'Nursery Two Timetable';
        
            case '6':
                $timetable = '@app/views/timetables/grade-1-timetable.php';
                $title = 'Grade One Timetable';
            
            case '7':
                $timetable = '@app/views/timetables/grade-2-timetable.php';
                $title = 'Grade Two Timetable';
            
            case '8':
                $timetable = '@app/views/timetables/grade-3-timetable.php';
                $title = 'Grade Three Timetable';

            case '9':
                $timetable = '@app/views/timetables/grade-4-timetable.php';
                $title = 'Grade Four Timetable';

            case '10':
                $timetable = '@app/views/timetables/grade-5-timetable.php';
                $title = 'Grade Five Timetable';
            
            
            default:
                # code...
                break;
        }

        return ['title' => $title, 'timetable' => $timetable];
    }

    public function duplicateClassTeacherCheck($attribute, $params)
    {
        $result = self::find()
                    ->where([
                        'class_teacher_id' => $this->class_teacher_id, 
                        'id' => $this->id
                        ])
                    ->one();
        
        if(!empty($result))
        {
            $this->addError($attribute, "!Duplicate Class Teacher designation: this staff is already assigned to {$result->class_name}");
        }            
    }
}
