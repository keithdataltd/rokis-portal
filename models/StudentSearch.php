<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Student;

/**
 * StudentSearch represents the model behind the search form of `app\models\Student`.
 */
class StudentSearch extends Student
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'current_class'], 'integer'],
            [['matric_number', 'first_name', 'middle_name', 'last_name', 'date_of_birth', 'address', 'sex', 'nationality', 'place_of_birth', 'state_of_origin', 'lga', 'religion', 'english_at_home', 'other_spoken_language', 'special_interests', 'position_in_family', 'carrying_instruction', 'medical_information', 'status', 'picture', 'created_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Student::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_of_birth' => $this->date_of_birth,
            'current_class' => $this->current_class,
            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', 'matric_number', $this->matric_number])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'place_of_birth', $this->place_of_birth])
            ->andFilterWhere(['like', 'state_of_origin', $this->state_of_origin])
            ->andFilterWhere(['like', 'lga', $this->lga])
            ->andFilterWhere(['like', 'religion', $this->religion])
            ->andFilterWhere(['like', 'english_at_home', $this->english_at_home])
            ->andFilterWhere(['like', 'other_spoken_language', $this->other_spoken_language])
            ->andFilterWhere(['like', 'special_interests', $this->special_interests])
            ->andFilterWhere(['like', 'position_in_family', $this->position_in_family])
            ->andFilterWhere(['like', 'carrying_instruction', $this->carrying_instruction])
            ->andFilterWhere(['like', 'medical_information', $this->medical_information])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'picture', $this->picture]);

        return $dataProvider;
    }
}
