<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "subject".
 *
 * @property int $id
 * @property string $subject_name
 * @property string $description
 * @property int $division_id
 * @property string $first_half_obtainable_marks
 * @property string $second_half_obtainable_marks
 * @property string $created_on
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject_name', 'division_id', 'created_on'], 'required'],
            [['description'], 'string'],
            [['division_id'], 'integer'],
            [['first_half_obtainable_marks', 'second_half_obtainable_marks'], 'number'],
            [['created_on'], 'safe'],
            [['subject_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_name' => 'Subject Name',
            'description' => 'Description',
            'division_id' => 'Division',
            'first_half_obtainable_marks' => '1st Half Marks',
            'second_half_obtainable_marks' => '2nd Half Marks',
            'created_on' => 'Created On',
        ];
    }

    public static function getPrimarySubjects()
    {
       return ArrayHelper::map(self::find()->where(['division_id' => 2])->all(), 'id', 'subject_name');
    }

    public static function getNurserySubjects()
    {
       return ArrayHelper::map(self::find()->where(['division_id' => 1])->all(), 'id', 'subject_name');
    }

    public static function computeSubjectTotalScore($subjectId, $classId, $term, $sessionId)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT SUM(t.obtained_percentage) AS total_score 
            FROM (SELECT obtained_percentage FROM continuous_assessment_result WHERE 
            subject_id = $subjectId AND class_id = $classId AND term = $term AND session_id = $sessionId
            UNION ALL 
            SELECT obtained_percentage FROM exam_result WHERE subject_id = $subjectId AND class_id = $classId AND term = $term AND session_id = $sessionId) t
        ");

        $result = $command->queryScalar();

        return $result;
        
    }

    //Returns StudentID and Total Score of each student for a given subject, ordered highest to lowest 
    public static function getSubjectScores($subjectId, $classId, $term, $sessionId)
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT t.student_id, sum(t.obtained_percentage) as totalSubjectScore
            FROM (
                SELECT student_id, obtained_percentage FROM continuous_assessment_result WHERE subject_id = $subjectId AND class_id = $classId AND term = $term AND session_id = $sessionId 
                UNION ALL 
                SELECT student_id, obtained_percentage FROM exam_result WHERE subject_id = $subjectId AND class_id = $classId AND term = $term AND session_id = $sessionId) 
            t 
            GROUP by t.student_id 
            ORDER BY `totalSubjectScore` DESC
        ");

        $result = $command->queryAll();

        return $result;
    }

    public static function getStudentSubjectPosition($studentId, $subjectId, $classId, $term, $sessionId)
    {
        $subjectTotalScores = self::getSubjectScores($subjectId, $classId, $term, $sessionId);

        //Rename array indexes to match StudentId
        $subjectTotalScores = ArrayHelper::index($subjectTotalScores, 'student_id');

        $i = array_search($studentId, array_keys($subjectTotalScores));

        return $i+1;
    }
    

}
