<?php

namespace app\models;

use Yii;
use app\models\ParentStudent;

/**
 * This is the model class for table "parent".
 *
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $phone2
 * @property string $residence_phone
 * @property string $employer
 * @property string $office_phone
 * @property string $work_address
 * @property string $mailing_address
 * @property string $password
 * @property string $last_login
 * @property string $created_on
 */
class Parents extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    //public $password_confirm;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'address', 'phone', 'employer', 'work_address', 'email'], 'required'],
            [['address', 'work_address', 'mailing_address'], 'string'],
            [['last_login', 'created_on'], 'safe'],
            [['first_name', 'middle_name', 'last_name', 'phone'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 40],
            [['phone', 'email'], 'unique'],
            [['phone2', 'residence_phone', 'office_phone'], 'string', 'max' => 30],
            [['employer'], 'string', 'max' => 100],
            [['password'], 'string', 'min' => 8]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'address' => 'Address',
            'email' => 'Email',
            'phone' => 'Phone',
            'phone2' => 'Phone2',
            'residence_phone' => 'Residence Phone',
            'employer' => 'Employer',
            'office_phone' => 'Office Phone',
            'work_address' => 'Work Address',
            'mailing_address' => 'Mailing Address',
            'password' => 'Password',
            'password_confirm' => 'Confirm Password',
            'last_login' => 'Last Login',
            'created_on' => 'Created On',
        ];
    }

    public function validateAuthKey($authKey)
    {
        return true;
    }

    public function getAuthKey()
    {
        return 1;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function validatePassword($password)
    {
        return $this->password === hash('sha256', $password);       
    }

    public static function findByUsername($username)
    {
        return self::findOne(['email' => strtolower($username) ]);
    }

    public static function getRandomString()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, 6);
    }

    public function getWardsList()
    {
        $wards = ParentStudent::findAll(['parent_id' => $this->id]);
        return $wards;
    }

    public function getFullName()
    {
        return ucwords($this->first_name .' '. $this->middle_name .' '. $this->last_name);
    }

    public static function getParentCount()
    {
        return self::find()->count();
    }
}
