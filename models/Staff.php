<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "staff".
 *
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $current_designation
 * @property string $email
 * @property string $phone
 * @property string $picture
 * @property string $staff_role
 * @property string $password
 * @property string $last_login
 * @property string $status
 * @property string $creation_date
 * @property string $state_id
 * @property string $lga_id
 * @property string $year_of_enrolment
 */
class Staff extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';

    public $password_confirm;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * {@inheritdoc}
     */

    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['first_name', 'middle_name', 'last_name', 'current_designation', 'phone', 'staff_role', 'email', 'password', 'password_confirm', 'state_id', 'lga_id', 'year_of_enrolment', 'picture'],
            self::SCENARIO_UPDATE => ['first_name', 'middle_name', 'last_name', 'current_designation', 'phone', 'staff_role', 'state_id', 'lga_id', 'year_of_enrolment', 'picture'],
        ];
    }
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'current_designation', 'phone', 'staff_role'], 'required'],
            [['current_designation', 'picture', 'status'], 'string'],
            [['state_id', 'lga_id'], 'integer'],
            [['last_login', 'creation_date', 'year_of_enrolment'], 'safe'],
            [['first_name', 'middle_name', 'last_name', 'email'], 'string', 'max' => 20],
            [['email'], 'email'],
            [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 1000 * 1024 ], 
            [['email', 'phone'], 'unique'],
            [['phone'], 'string', 'max' => 20],
            [['password'], 'string', 'min' => 8],
            [['password_confirm'], 'compare', 'compareAttribute' => 'password']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'current_designation' => 'Current Designation',
            'email' => 'Email',
            'phone' => 'Phone',
            'picture' => 'Picture',
            'staff_role' => 'Access Role',
            'state_id' => 'State',
            'lga_id' => 'LGA',
            'year_of_enrolment' => 'Enrolment Date',
            'password' => 'Password',
            'password_confirm' => 'Confirm Password',
            'last_login' => 'Last Login',
            'status' => 'Status',
            'creation_date' => 'Creation Date',
        ];
    }

    public function getFullName()
    {
        return ucwords($this->first_name .' '. $this->middle_name .' '. $this->last_name);
    }

    public function validateAuthKey($authKey)
    {
        return true;
    }

    public function getAuthKey()
    {
        return 1;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function validatePassword($password)
    {
        return $this->password === hash('sha256', $password);       
    }

    public static function findByUsername($username)
    {
        return self::findOne(['phone' => strtolower($username) ]);
    }

    public static function getRandomString()
    {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($permitted_chars), 0, 6);
    }

    public static function getStaffCount()
    {
        return self::find()->count();
    }
}
