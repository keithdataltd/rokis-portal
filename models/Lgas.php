<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lgas".
 *
 * @property int $id
 * @property string $lga_name
 * @property int $state_id
 */
class Lgas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lgas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['state_id'], 'integer'],
            [['lga_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lga_name' => 'Lga Name',
            'state_id' => 'State ID',
        ];
    }
}
