<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "primary_report_card_attributes".
 *
 * @property int $id
 * @property int $primary_report_card_id
 * @property int $student_id
 * @property int $no_of_times_present attendance
 * @property int $no_of_times_absent attendance
 * @property string $_total
 * @property string $_average
 * @property string $handwriting psychomotor
 * @property string $verbal_fluency psychomotor
 * @property string $games psychomotor
 * @property string $sports psychomotor
 * @property string $handling_tools psychomotor
 * @property string $drawing_and_painting psychomotor
 * @property string $musical_skills psychomtor
 * @property string $punctuality affective_area
 * @property string $neatness affective_area
 * @property string $politeness affective_area
 * @property string $honesty affective_area
 * @property string $cooperation affective area
 * @property string $leadership affective area
 * @property string $helping_others affective area
 * @property string $emotional_stability affective area
 * @property string $health affective area
 * @property string $attitude_to_school_work affective area
 * @property string $attentiveness affective area
 * @property string $perseverance affective area
 * @property string $speaking_handwriting affective area
 * @property string $class_teacher_comment
 * @property string $headteacher_comment
 */
class PrimaryReportCardAttributes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'primary_report_card_attributes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['handwriting', 'verbal_fluency', 'games', 'sports', 'handling_tools', 'drawing_and_painting', 'musical_skills', 'punctuality', 'neatness', 'politeness', 'honesty', 'cooperation', 'leadership', 'helping_others', 'emotional_stability', 'health', 'attitude_to_school_work', 'attentiveness', 'perseverance', 'speaking_handwriting', 'class_teacher_comment', 'headteacher_comment'], 'required', 'message' => 'Enter Grade'],
            [['primary_report_card_id', 'student_id', 'no_of_times_present', 'no_of_times_absent'], 'integer'],
            [['_total', '_average'], 'number'],
            [['handwriting', 'verbal_fluency', 'games', 'sports', 'handling_tools', 'drawing_and_painting', 'musical_skills', 'punctuality', 'neatness', 'politeness', 'honesty', 'cooperation', 'leadership', 'helping_others', 'emotional_stability', 'health', 'attitude_to_school_work', 'attentiveness', 'perseverance', 'speaking_handwriting', 'class_teacher_comment', 'headteacher_comment'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'primary_report_card_id' => 'Primary Report Card ID',
            'student_id' => 'Student ID',
            'no_of_times_present' => 'No. Of Times Present',
            'no_of_times_absent' => 'No. Of Times Absent',
            '_total' => 'Total',
            '_average' => 'Average',
            'handwriting' => 'Handwriting',
            'verbal_fluency' => 'Verbal Fluency',
            'games' => 'Games',
            'sports' => 'Sports',
            'handling_tools' => 'Handling Tools',
            'drawing_and_painting' => 'Drawing And Painting',
            'musical_skills' => 'Musical Skills',
            'punctuality' => 'Punctuality',
            'neatness' => 'Neatness',
            'politeness' => 'Politeness',
            'honesty' => 'Honesty',
            'cooperation' => 'Cooperation',
            'leadership' => 'Leadership',
            'helping_others' => 'Helping Others',
            'emotional_stability' => 'Emotional Stability',
            'health' => 'Health',
            'attitude_to_school_work' => 'Attitude To School Work',
            'attentiveness' => 'Attentiveness',
            'perseverance' => 'Perseverance',
            'speaking_handwriting' => 'Speaking Handwriting',
            'class_teacher_comment' => 'Class Teacher Comment',
            'headteacher_comment' => 'Headteacher Comment',
        ];
    }
}
