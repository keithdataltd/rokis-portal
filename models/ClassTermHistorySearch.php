<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassTermHistory;

/**
 * ClassTermHistorySearch represents the model behind the search form of `app\models\ClassTermHistory`.
 */
class ClassTermHistorySearch extends ClassTermHistory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'session_id'], 'integer'],
            [['term', 'start_date', 'end_date', 'next_term_start_date', 'creation_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClassTermHistory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'session_id' => $this->session_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'next_term_start_date' => $this->next_term_start_date,
            'creation_date' => $this->creation_date,
        ]);

        $query->andFilterWhere(['like', 'term', $this->term]);

        return $dataProvider;
    }
}
