<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "session".
 *
 * @property int $id
 * @property string $session_title
 * @property string $start_date
 * @property string $end_date
 * @property string $creation_date
 */
class Session extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['session_title'], 'required'],
            [['start_date', 'end_date', 'creation_date'], 'safe'],
            [['session_title'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session_title' => 'Session Title',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'creation_date' => 'Creation Date',
        ];
    }

    public static function getSessionList()
    {
        $sessionsList = self::find()->all();
        $sessionsList = ArrayHelper::map($sessionsList, 'id', 'session_title');
        return $sessionsList;
    }
}
