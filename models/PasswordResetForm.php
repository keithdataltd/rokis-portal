<?php
    namespace app\models;
	use Yii;
	use yii\base\Model;


	class PasswordResetForm extends Model
	{
		public $password;
		public $password_confirm;

		public function rules()
		{
			return [

				[ ['password', 'password_confirm'], 'required' ],
				
				[ ['password', 'password_confirm'], 'trim' ],
				[ ['password', 'password_confirm'], 'string', 'length' => [6, 15]],				
				['password_confirm', 'compare', 'compareAttribute' => 'password'],
			];
		}
	}

?>