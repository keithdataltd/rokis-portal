<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nursery_report_card".
 *
 * @property int $id
 * @property int $student_id
 * @property double $weight
 * @property double $height
 * @property string $reading_activities activity
 * @property string $numbering_activities activity
 * @property string $creative_activities activity
 * @property string $nursery_rhymes activity
 * @property string $music activity
 * @property string $physical_activities activity
 * @property string $games activity
 * @property string $self_confidence personal quality
 * @property string $sociability personal quality
 * @property string $intelligence personal quality
 * @property string $interest_and_curiosity personal quality
 * @property string $comprehension personal quality
 * @property string $personal_cleanliness personal quality
 * @property int $days_absent
 * @property string $punctuality
 * @property string $special_remarks
 * @property int $promoted_to
 * @property string $next_term_begins
 * @property string $term
 * @property int $session
 * @property int $class
 * @property string $approval_status
 * @property int $approved_by
 * @property string $approval_date
 * @property string $created_on 
 * @property int $created_by
 */
class NurseryReportCard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nursery_report_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'weight', 'height', 'reading_activities', 'numbering_activities', 'creative_activities', 'nursery_rhymes', 'music', 'physical_activities', 'games', 'self_confidence', 'sociability', 'intelligence', 'interest_and_curiosity', 'comprehension', 'personal_cleanliness', 'total_attendance', 'days_absent', 'punctuality', 'special_remarks', 'term', 'session'], 'required'],
            [['student_id', 'days_absent', 'promoted_to', 'session', 'class', 'total_attendance', 'approved_by', 'created_by'], 'integer'],
            [['weight', 'height'], 'number'],
            [['reading_activities', 'numbering_activities', 'creative_activities', 'nursery_rhymes', 'music', 'physical_activities', 'games', 'self_confidence', 'sociability', 'intelligence', 'interest_and_curiosity', 'comprehension', 'personal_cleanliness', 'special_remarks', 'term', 'approval_status'], 'string'],
            [['next_term_begins', 'approval_date', 'created_on'], 'safe'],
            [['punctuality'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Student',
            'weight' => 'Weight',
            'height' => 'Height',
            'reading_activities' => 'Reading Activities',
            'numbering_activities' => 'Numbering Activities',
            'creative_activities' => 'Creative Activities',
            'nursery_rhymes' => 'Nursery Rhymes',
            'music' => 'Music',
            'physical_activities' => 'Physical Activities',
            'games' => 'Games',
            'self_confidence' => 'Self Confidence',
            'sociability' => 'Sociability',
            'intelligence' => 'Intelligence',
            'interest_and_curiosity' => 'Interest And Curiosity',
            'comprehension' => 'Comprehension',
            'personal_cleanliness' => 'Personal Cleanliness',
            'days_absent' => 'Days Absent',
            'punctuality' => 'Punctuality',
            'special_remarks' => 'Special Remarks',
            'promoted_to' => 'Promoted To',
            'next_term_begins' => 'Next Term Begins',
            'term' => 'Term',
            'session' => 'Session',
            'class' => 'Class',
            'approval_status' => 'Approval Status',
            'approved_by' => 'Approved By',
            'approval_date' => 'Approval Date',
            'created_on' => 'Created On',
            'created_by' => 'Created By'
        ];
    }
}
