<?php
$session = Yii::$app->session;
/* @var $this \yii\web\View */
/* @var $content string */

// use app\widgets\Alert;
 use yii\helpers\Html;
// use yii\bootstrap\Nav;
// use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\W3Asset;

W3Asset::register($this);
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html>
	<head>
		
		<title>ROKIS Portal</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/w3.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
		<style>
			html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
		</style>
		<?php $this->head() ?>
		<?= Html::csrfMetaTags() ?>
	</head>
		<body class="w3-white">
			<?php $this->beginBody() ?>
			<!-- Top container -->
			<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
			  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();">
			  	<i class="fa fa-bars"></i>  Menu
			  </button>
			  <span class="w3-bar-item w3-right">ROKIS STAFF PORTAL</span>
			</div>

			<!-- Sidebar/menu -->
			<nav class="w3-sidebar w3-collapse w3-black w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
			  <div class="w3-container w3-row">
			    
			    <div class="w3-col s12 w3-bar">
			      <span>Welcome, <strong><?= Yii::$app->staff->identity->first_name ?></strong></span><br>
			      <?= Html::a("<i class='fas fa-user fa-fw'></i> My Profile", ['/staff/default/my-profile'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
			      <?= Html::a("Logout", ['/staff/default/logout'], ['class' => 'w3-bar-item w3-button w3-round w3-red w3-padding-small']) ?>
			    </div>
			  </div>
			  <hr>
			  <div class="w3-container">
			    <h5>ROKIS Dashboard</h5>
			  </div>
			  <div class="w3-bar-block">
			    <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
			    <?= Html::a("<i class='fas fa-tachometer-alt'></i> Dashboard", ['/staff/default/dashboard'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				<?= Html::a("<i class='fas fa-user-graduate fa-fw'></i> Manage Pupils", ['/staff/student'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
			    <?= Html::a("<i class='fa fa-users fa-fw'></i> Manage Parents", ['/staff/parents'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
			    <?= Html::a("<i class='fas fa-chalkboard-teacher fa-fw'></i> Manage Staff", ['/staff/manage-staff'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
			    <?= Html::a("<i class='fas fa-book-open fa-fw'></i> Manage Subjects", ['/staff/subject'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				
				<?php if($session['staff_role'] == 'admin'): ?>
					<?= Html::a("<i class='fas fa-chalkboard'></i> Manage Classes", ['/staff/school-class'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				<?php endif; ?>

				<?php if($session['staff_role'] == 'admin'): ?>
					<?= Html::a("<i class='far fa-calendar-alt'></i> Manage Calendar", ['/staff/calendar'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				<?php endif; ?>

			    <div class="w3-dropdown-click">
				  <button onclick="navDropDown('resultDropDownLink')" class="w3-button">
				  	<i class='fas fa-list fa-fw'></i>
				  		Result Management
				  	<i class='fas fa-caret-down fa-fw'></i>
				  </button>
				  <div id="resultDropDownLink" class="w3-dropdown-content w3-bar-block w3-border">
				    <?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Nursery Report Cards", ['/staff/nursery-report-card'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Primary Report Cards", ['/staff/report-cards'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				  </div>
				</div>

				<div class="w3-dropdown-click">
				  <button onclick="navDropDown('timetableDropdownLink')" class="w3-button">
				  	<i class="fas fa-th"></i>
				  		Timetables
				  	<i class='fas fa-caret-down fa-fw'></i>
				  </button>
				  <div id="timetableDropdownLink" class="w3-dropdown-content w3-bar-block w3-border">
				    <?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Pre-Nursery", ['/staff/default/timetable', 'class' => 'pre-nursery'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				    <?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Nursery One", ['/staff/default/timetable', 'class' => 'nursery1'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Nursery Two", ['/staff/default/timetable', 'class' => 'nursery2'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Grade One", ['/staff/default/timetable', 'class' => 'grade1'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Grade Two", ['/staff/default/timetable', 'class' => 'grade2'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Grade Three", ['/staff/default/timetable', 'class' => 'grade3'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Grade Four", ['/staff/default/timetable', 'class' => 'grade4'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Grade Five", ['/staff/default/timetable', 'class' => 'grade5'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				  </div>
				</div>

				<?php if($session['staff_role'] == 'admin'): ?>
			    <div class="w3-dropdown-click">
				  <button onclick="navDropDown('navDropDownLink')" class="w3-button">
				  	<i class='fas fa-cogs fa-fw'></i>
				  		System Settings
				  	<i class='fas fa-caret-down fa-fw'></i>
				  </button>
				  <div id="navDropDownLink" class="w3-dropdown-content w3-bar-block w3-border">
				    <?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Session Configuration", ['/staff/default/term-session-status'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
					<?= Html::a("<i class='fas fa-caret-right fa-fw'></i> Term History Configuration", ['/staff/term-history/index'], ['class' => 'w3-bar-item w3-button w3-padding']) ?>
				  </div>
				</div>
			    <br><br>
				<?php endif ?>
			  </div>
			</nav>


			<!-- Overlay effect when opening sidebar on small screens -->
			<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

			<!-- !PAGE CONTENT! -->
			<div class="w3-main w3-white" style="margin-left:300px;margin-top:43px;">
				
				<div class="w3-container">
					<?= 
						Breadcrumbs::widget([
							'homeLink' => [ 
											'label' => Yii::t('yii', 'Dashboard'),
											'url' => ['default/dashboard'],
										],
							'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
						]) 
					?>
				</div>
				

			  <?= $content ?>

			  

			  

			  

			  <!-- Footer -->
			  <footer class="w3-container w3-padding-16 w3-light-white">			    
			    
			  </footer>

			  <!-- End page content -->
			</div>

			<script>
				// Get the Sidebar
				var mySidebar = document.getElementById("mySidebar");

				// Get the DIV with overlay effect
				var overlayBg = document.getElementById("myOverlay");

				// Toggle between showing and hiding the sidebar, and add overlay effect
				function w3_open() {
				  if (mySidebar.style.display === 'block') {
				    mySidebar.style.display = 'none';
				    overlayBg.style.display = "none";
				  } else {
				    mySidebar.style.display = 'block';
				    overlayBg.style.display = "block";
				  }
				}

				// Close the sidebar with the close button
				function w3_close() {
				  mySidebar.style.display = "none";
				  overlayBg.style.display = "none";
				}

				function navDropDown(elem) {
				  var x = document.getElementById(elem);
				  if (x.className.indexOf("w3-show") == -1) {
				    x.className += " w3-show";
				  } else { 
				    x.className = x.className.replace(" w3-show", "");
				  }
				}
			</script>
		<?php $this->endBody() ?>
		</body>
	</html>


<?php $this->endPage() ?>