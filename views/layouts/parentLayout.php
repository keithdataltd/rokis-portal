<?php
    $session = Yii::$app->session;
    /* @var $this \yii\web\View */
    /* @var $content string */

    // use app\widgets\Alert;
    use yii\helpers\Html;
    // use yii\bootstrap\Nav;
    // use yii\bootstrap\NavBar;
    // use yii\widgets\Breadcrumbs;
    use app\assets\W3Asset;

    W3Asset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en">
    <title>Home | ROKIS Parents Portal</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
        body {font-size:16px; background-image: url(/images/memphis-colorful.png)}
        .w3-half img{margin-bottom:-6px;margin-top:16px;opacity:0.8;cursor:pointer}
        .w3-half img:hover{opacity:1}
    </style>
    <?php $this->head() ?>
	<?= Html::csrfMetaTags() ?>
    <body>
        <?php $this->beginBody() ?>

        <!-- Sidebar/menu -->
        <nav class="w3-sidebar w3-yellow w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px">Close Menu</a>
            <div class="w3-container">
                <img src="/images/logo.png" height="160" width="160" style="margin-top: 15px;" class="w3-image w3-round" alt="Royal Kiddies">
                <span class="w3-text-blue">Welcome, <strong><?= Yii::$app->parent->identity->first_name ?></strong></span><br>
            </div>
            <div class="w3-container">
                <h5 class="w3-padding-16"><b>Royal Kiddies</b> Parents Portal</h5>
                
            </div>
            <div class="w3-bar-block">
                <?= Html::a("Home", 
                        ['/parents/default/home'], 
                        [
                            'class' => 'w3-bar-item w3-button w3-hover-blue w3-round-xxlarge', 
                            'onclick' => "w3_close()"
                        ]) 
                ?>
                <?= Html::a("My Kids", 
                        ['/parents/default/my-kids'], 
                        [
                            'class' => 'w3-bar-item w3-button w3-hover-blue w3-round-xxlarge', 
                            'onclick' => "w3_close()"
                        ]) 
                ?> 

                <?= Html::a("My Profile", 
                        ['/parents/default/my-profile'], 
                        [
                            'class' => 'w3-bar-item w3-button w3-hover-blue w3-round-xxlarge', 
                            'onclick' => "w3_close()"
                        ]) 
                ?>

                <?= Html::a("Calendar", 
                        ['/parents/default/calendar'], 
                        [
                            'class' => 'w3-bar-item w3-button w3-hover-blue w3-round-xxlarge', 
                            'onclick' => "w3_close()"
                        ]) 
                ?>

                <?= Html::a("Staff List", 
                        ['/parents/default/staff-list'], 
                        [
                            'class' => 'w3-bar-item w3-button w3-hover-blue w3-round-xxlarge', 
                            'onclick' => "w3_close()"
                        ]) 
                ?>
                
                <?= Html::a("Logout", 
                        ['/parents/default/logout'], 
                        [
                            'class' => 'w3-bar-item w3-button w3-red w3-round-xxlarge', 
                            'onclick' => "w3_close()"
                        ]) 
                ?>

                <!-- W3.CSS Container -->
                <div class="w3-bar-item w3-clear w3-margin-top"></div>
                    <p class="w3-small w3-center">
                        Powered by <a href="https://www.keithdata.com" title="Keith Data Ltd." target="_blank" class="w3-hover-opacity">
                        <br>
                            <img src="/images/kd-logo.svg" class="w3-image" height="45" width="80" alt="">
                        </a>
                    </p>
                
            </div>
        </nav>

        <!-- Top menu on small screens -->
        <header class="w3-container w3-top w3-hide-large w3-yellow w3-xlarge w3-padding">
            <a href="javascript:void(0)" class="w3-button w3-blue w3-margin-right" onclick="w3_open()">☰</a>
            <span>ROKIS Parents Portal</span>
        </header>

        <!-- Overlay effect when opening sidebar on small screens -->
        <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

        <!-- !PAGE CONTENT! -->
        <div class="w3-main" style="margin-left:340px;margin-right:40px;">
            <?= $content ?>       
        </div>
        <!-- End page content -->

        

        <script>
            // Script to open and close sidebar
            function w3_open()
            {
                document.getElementById("mySidebar").style.display = "block";
                document.getElementById("myOverlay").style.display = "block";
            }
            
            function w3_close()
            {
                document.getElementById("mySidebar").style.display = "none";
                document.getElementById("myOverlay").style.display = "none";
            }
        </script>

        <?php $this->endBody() ?>
    </body>
</html>

<?php $this->endPage() ?>
