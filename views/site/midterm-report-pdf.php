<?php

    use yii\helpers\Html;	
    use app\models\SchoolClass;
    use app\models\Student;
    use app\models\Subject;
    use app\models\PrimaryReportCardAttributes;
    use app\models\PrimaryReportCardResult;
    use app\models\PrimaryReportCard;
    use app\models\GeneralSettings;
    use app\models\ClassTermHistory;
    use app\models\Session;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        
    </style>
</head>
<body>
    <div class="container">
        <br>
        <div class="text-center">
            <img src="/images/logo.png" alt="" height="90px" width="90px">
            <h3>ROYAL KIDDIES INTERNATIONAL SCHOOL</h1>
            <h5>PLOT 4, ORO-AGO CRESCENT, GARKI II, ABUJA</h3>
            <h6>CRECHE | NURSERY | PRIMARY</h5>
            <strong>www.royalkiddiesschool.com</strong>
        </div>
       
        <div>
            <h2>MID-TERM REPORT CARD</h2>
            <h5>CONTINUOUS ASSESSMENT SHEET</h5>
            <table class="table">
                <tr>
                    <th>
                        <h5>NAME: <?= Student::findOne($reportCard->student_id)->getFullName()  ?></h5>
                    </th>
                    <th>CLASS: <?= SchoolClass::findOne($reportCard->class_id)->class_name ?></th>
                    <th>TERM: <?= Yii::$app->formatter->asOrdinal($reportCard->term) . " Term" ?></th>
                    <th>SESSION: <?= Session::findOne($reportCard->session_id)->session_title ?></th>
                </tr>
            </table>
        </div>
        
         
    </div>

    <div class="container">
        <!-- ** COGNITIVE ABILITY -->
        <div class="" >            
            <table class="table table-bordered">
                <thead class="thead-light">                    
                    <tr>
                        <th>Subject</th>
                        <th>Marks Obtainable</th>

                        <th>Marks Obtained</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach($scores as $k => $v): ?>
                        <tr>
                            <th>
                                <?= Subject::findOne($k)->subject_name ?>
                            </th>

                            <td>
                                40
                            </td>
                            <th>
                                <?= $v['total'] ?>
                            </th>                            
                        </tr>
                    <?php endforeach ?>
			    </tbody>
            </table>
        </div>
        <!-- ## COGNITIVE ABILITY -->
    
    </div>

    
</body>
</html>