<?php

    use yii\helpers\Html;	
    use app\models\SchoolClass;
    use app\models\Student;
    use app\models\Subject;
    use app\models\PrimaryReportCardAttributes;
    use app\models\PrimaryReportCardResult;
    use app\models\PrimaryReportCard;
    use app\models\GeneralSettings;
    use app\models\ClassTermHistory;
    use app\models\Session;

    $nextTermStartDate = ClassTermHistory::findOne(['term' => $reportCard['term'] , 'session_id' => $reportCard['session']->id ])->next_term_start_date;
    //var_dump($reportCard['exam_results']); exit();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        
    </style>
</head>
<body>
    <div class="container">
        <br>
        <div class="text-center">
            <img src="/images/logo.png" alt="" height="90px" width="90px">
            <h3>ROYAL KIDDIES INTERNATIONAL SCHOOL</h1>
            <h5>PLOT 4, ORO-AGO CRESCENT, GARKI II, ABUJA</h3>
            <h6>CRECHE | NURSERY | PRIMARY</h5>
            <strong>www.royalkiddiesschool.com</strong>
        </div>
       
        <div>
            <table class="table">
                <tr>
                    <th>
                        <h5>NAME: <?= $reportCard['student']->getFullName()  ?></h5>
                    </th>
                    <th>CLASS: <?= $reportCard['class'] ?></th>
                    <th>TERM: <?= Yii::$app->formatter->asOrdinal($reportCard['term']) . " Term" ?></th>
                    <th>SESSION: <?= $reportCard['session']->session_title ?></th>
                </tr>
            </table>
        </div>
        
        <!-- ** ATTENDANCE AND TOTAL -->
        <div class="" >                      
            <div class="float-left">                
                <table class="table table-bordered">
                    <caption><strong>ATTENDANCE</strong></caption>
                    <tr>
                        <th class="bg-light">Frequencies</th> <th class="bg-light">Schools Attendance</th>
                    </tr>
                    <tr>
                        <th class="bg-light">No. of Times School Opened</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th class="bg-light">No. of Times Present</th>
                        <th><?= $reportCard['attributes']->no_of_times_present ?></th>
                    </tr>
                    <tr>
                        <th class="bg-light">No. of Times Absent</th>
                        <th><?= $reportCard['attributes']->no_of_times_absent ?></th>
                    </tr>
                </table>
            </div>

            <div class="float-right">                
                <table class="table table-bordered">
                    <caption><strong>TOTAL</strong></caption>
                    <tr>
                        <th class="bg-light">Total</th>
                        <th><?= $reportCard['report_card_total'] ?></th>
                    </tr>
                    <tr>
                        <th class="bg-light">Average</th>
                        <th><?= number_format($reportCard['report_card_average'], 2) ?></th>
                    </tr>
                </table>
            </div>
                                   
            
        </div>
        <!-- ## ATTENDANCE AND TOTAL -->        
    </div>

    <div class="container">
        <!-- ** COGNITIVE ABILITY -->
        <div class="" >            
            <table class="table table-bordered">
                <caption><strong>COGNITIVE ABILITY</strong></caption>
                <thead class="thead-light">
                    <tr>
                        <th rowspan='2'>
                            <i>Motto: Capture and develop their intellectual capabilities</i>
                        </th>
                        <th colspan='2'>1st Half</th>
                        <th colspan='2'>2nd Half</th>
                        <th colspan='2'>Final Result</th>
                        <th colspan='3'></th>
                    </tr>
                    <tr>
                        <th>Marks Obtainable</th>
                        <th>Marks Obtained</th>

                        <th>Marks Obtainable</th>
                        <th>Marks Obtained</th>

                        <th>Marks Obtainable</th>
                        <th>Marks Obtained</th>

                        <th>Class Average Marks</th>
                        <th>Position</th>
                        <th>Teacher's Remarks</th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach($reportCard['exam_results'] as $result): ?>
                        <tr>
                            <th>
                                <?= Subject::findOne($result->subject_id)->subject_name ?>
                            </th>

                            <td>
                                <?= Subject::findOne($result->subject_id)->first_half_obtainable_marks ?>
                            </td>
                            <th>
                                <?= $reportCard[$result->subject_id]['ca']['total'] ?>
                            </th>

                            <td>
                                <?= Subject::findOne($result->subject_id)->second_half_obtainable_marks ?>
                            </td>
                            <th>
                                <?= $reportCard[$result->subject_id]['exam'] ?>
                            </th>

                            <td>100</td>

                            <th>
                                <?= $reportCard[$result->subject_id]['ca']['total'] + $reportCard[$result->subject_id]['exam']  ?>
                            </th>

                            <td>
                                <?= $reportCard[$result->subject_id]['class_average_mark'] ?>
                            </td>

                            <th>
                                <?= Yii::$app->formatter->asOrdinal($reportCard[$result->subject_id]['subject_position']) ?>
                            </th>
                            <td>
                                <?= $reportCard[$result->subject_id]['remark'] ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
			    </tbody>
            </table>
        </div>
        <!-- ## COGNITIVE ABILITY -->
    
    </div>

    <div class="container">
        <div class="float-left">
            <table class="table table-bordered">
                <caption><strong>PSYCHOMOTOR SKILLS</strong></caption>
                <tr>
                    <th></th>
                    <th class="bg-light">Grade</th>
                </tr>
                <tr>
                    <th class="bg-light">Handwriting</th>
                    <td> <?= $reportCard['attributes']->handwriting ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Verbal Fluency</th>
                    <td> <?= $reportCard['attributes']->verbal_fluency ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Games</th>
                    <td> <?= $reportCard['attributes']->games ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Sports</th>
                    <td> <?= $reportCard['attributes']->sports ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Handling Tools</th>
                    <td> <?= $reportCard['attributes']->handling_tools ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Drawing &amp; Painting</th>
                    <td> <?= $reportCard['attributes']->drawing_and_painting ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Musical Skills</th>
                    <td> <?= $reportCard['attributes']->musical_skills ?> </td>
                </tr>
            </table>

            <table class="table table-bordered">
                <tr>
                    <th class="bg-light">Number of Pupils in Class</th>
                    <td><?= $reportCard['number_of_students_in_class'] ?></td>
                </tr>
                <tr>
                    <th class="bg-light">Class Teacher's Comment</th>
                    <td><?= $reportCard['attributes']->class_teacher_comment ?></td>
                </tr>
                <tr>
                    <th class="bg-light">Head-teacher's Comment</th>
                    <td><?= $reportCard['attributes']->headteacher_comment ?></td>
                </tr>
            </table>

                   
        </div>
 
        <div class="float-left">
            <table class="table table-bordered ml-5 table-dark">
                <caption><strong>SCALE</strong></caption>
                <tr>
                    <td>5</td> <td>Excellent</td>                   
                </tr>
                <tr>
                    <td>4</td> <td>Good</td>                   
                </tr>
                <tr>
                    <td>3</td> <td>Fair</td>                   
                </tr>
                <tr>
                    <td>2</td> <td>Poor</td>                   
                </tr>
                <tr>
                    <td>1</td> <td>Very Poor</td>                   
                </tr>
            </table>
        </div>

        <div class="float-right">
            <table class="table table-bordered ml-2">
                <caption><strong>AFFECTIVE AREAS</strong></caption>
                <tr>
                    <th></th>
                    <th class="bg-light">Grade</th>
                </tr>
                <tr>
                    <th class="bg-light">Punctuality</th>
                    <td> <?= $reportCard['attributes']->punctuality ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Neatness</th>
                    <td> <?= $reportCard['attributes']->neatness ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Politeness</th>
                    <td> <?= $reportCard['attributes']->politeness ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Honesty</th>
                    <td> <?= $reportCard['attributes']->honesty ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Co-operation with Others</th>
                    <td> <?= $reportCard['attributes']->cooperation ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Leadership</th>
                    <td> <?= $reportCard['attributes']->leadership ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Helping Others</th>
                    <td> <?= $reportCard['attributes']->helping_others ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Emotional Stability</th>
                    <td> <?= $reportCard['attributes']->emotional_stability ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Health</th>
                    <td> <?= $reportCard['attributes']->health ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Attitude To School</th>
                    <td> <?= $reportCard['attributes']->attitude_to_school_work ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Attentiveness</th>
                    <td> <?= $reportCard['attributes']->attentiveness ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Perseverance</th>
                    <td> <?= $reportCard['attributes']->perseverance ?> </td>
                </tr>
                <tr>
                    <th class="bg-light">Speaking/Handwriting</th>
                    <td> <?= $reportCard['attributes']->speaking_handwriting ?> </td>
                </tr>
            </table>
        </div>

        <b>Next Term Begins:  <?= empty($nextTermStartDate) ? '--' : date_format(date_create($nextTermStartDate), 'l d F, Y') ?></b>
    </div>
</body>
</html>