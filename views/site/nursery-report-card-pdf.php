<?php

    use yii\helpers\Html;	
    use app\models\SchoolClass;
    use app\models\Student;
    use app\models\Subject;
    use app\models\PrimaryReportCardAttributes;
    use app\models\PrimaryReportCardResult;
    use app\models\PrimaryReportCard;
    use app\models\GeneralSettings;
    use app\models\ClassTermHistory;
    use app\models\Session;


    $nextTermStartDate = ClassTermHistory::findOne(['term' => $reportCard->term , 'session_id' => $reportCard->session ])->next_term_start_date;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">  
</head>

<body>
    <div class="container">
        <br>
        <div class="text-center">
            <img src="/images/logo.png" alt="" height="90px" width="90px">
            <h3>ROYAL KIDDIES INTERNATIONAL SCHOOL</h1>
            <h5>PLOT 4, ORO-AGO CRESCENT, GARKI II, ABUJA</h3>
            <h6>CRECHE | NURSERY | PRIMARY</h5>
            <strong>www.royalkiddiesschool.com</strong>
        </div>

        <div class="float-left">
            <table class="table table-bordered">
                <tr>
                    <th>NAME</th>
                    <th><?= Student::findOne($reportCard->student_id)->getFullName() ?></th>  
                </tr>
                <tr>
                    <th>WEIGHT</th>
                    <th><?= $reportCard->weight . '(kg)'  ?></th>  
                </tr>
                <tr>
                    <th>HEIGHT</th>
                    <th><?= $reportCard->height . '(cm)'  ?></th>  
                </tr>
            </table>
        </div>

        <div class="float-right">
            <table class="table table-bordered">
                <tr>
                    <th>CLASS</th>
                    <th><?= SchoolClass::findOne($reportCard->class)->class_name  ?></th>  
                </tr>
                <tr>
                    <th>TERM</th>
                    <th><?= Yii::$app->formatter->asOrdinal($reportCard->term) . " Term"  ?></th>  
                </tr>
                <tr>
                    <th>SESSION</th>
                    <th><?= Session::findOne($reportCard->session)->session_title  ?></th>  
                </tr>
            </table>
        </div>      
    </div>

    <div class="container">
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>ACTIVITIES</th>
                    <th>TEACHER'S OBSERVATION</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>READING ACTIVITIES</th>
                    <td><?= $reportCard->reading_activities  ?></td>
                </tr>
                <tr>
                    <th>NUMBERING ACTIVITIES</th>
                    <td><?= $reportCard->numbering_activities  ?></td>
                </tr>
                <tr>
                    <th>CREATIVE ACTIVITIES</th>
                    <td><?= $reportCard->creative_activities ?></td>
                </tr>
                <tr>
                    <th>NURSERY RHYMES</th>
                    <td><?= $reportCard->nursery_rhymes  ?></td>
                </tr>
                <tr>
                    <th>MUSIC</th>
                    <td><?= $reportCard->music  ?></td>
                </tr>
                <tr>
                    <th>PHYSICAL ACTIVITIES</th>
                    <td><?= $reportCard->physical_activities  ?></td>
                </tr>
                <tr>
                    <th>GAMES</th>
                    <td><?= $reportCard->games  ?></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="container">
        <table class="table table-bordered">
            <thead class="thead-light">
                <tr>
                    <th>PERSONAL QUALITIES</th>
                    <th>TEACHER'S OBSERVATION</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>SELF CONFIDENCE</th>
                    <td><?= $reportCard->self_confidence  ?></td>
                </tr>
                <tr>
                    <th>SOCIABILITY</th>
                    <td><?= $reportCard->sociability  ?></td>
                </tr>
                <tr>
                    <th>INTELLIGENCE</th>
                    <td><?= $reportCard->intelligence ?></td>
                </tr>
                <tr>
                    <th>INTEREST AND CURIOSITY</th>
                    <td><?= $reportCard->interest_and_curiosity  ?></td>
                </tr>
                <tr>
                    <th>COMPREHENSION</th>
                    <td><?= $reportCard->comprehension ?></td>
                </tr>
                <tr>
                    <th>PERSONAL CLEANLINESS</th>
                    <td><?= $reportCard->personal_cleanliness  ?></td>
                </tr>                
            </tbody>
        </table>
    </div>
    <div class="container">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th>TOTAL ATTENDANCE</th>
                    <td>
                        <sup>DAYS ABSENT</sup>
                        <?= $reportCard->days_absent ?>
                    </td>
                    <td>
                        <sup>PUNCTUALITY</sup>
                        <?= $reportCard->punctuality ?>
                    </td>
                </tr>
                <tr>
                    <th>SPECIAL REMARKS</th>
                    <td colspan=2><?= $reportCard->special_remarks ?></td>
                </tr>
                <tr>
                    <th>PROMOTED TO</th>
                    <td colspan=2><?= $reportCard->promoted_to ?></td>
                </tr>
                <tr>
                    <th>NEXT TERM BEGINS</th>
                    <td colspan=2><?= empty($nextTermStartDate) ? '--' : date_format(date_create($nextTermStartDate), 'l d F, Y') ?></td>
                </tr>              
            </tbody>
        </table>
    </div>
</body>