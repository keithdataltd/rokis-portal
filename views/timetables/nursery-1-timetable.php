<div class="w3-container w3-padding w3-responsive">
    <h1 class="w3-center"><?= $title ?></h1>
    <table class="w3-table-all">
        <thead >
            <tr class="w3-blue" >
                <th>Time</th> 
                <th class="w3-center">7:55 <br>8:10 </th> 
                <th class="w3-center">8:10 <br> 8:40</th> 
                <th class="w3-center">8:40 <br> 9:10</th> 
                <th class="w3-center">9:10 <br> 9:40</th> 
                <th class="w3-yellow w3-center">9:40 <br> 10:10</th> 
                <th class="w3-yellow w3-center">10:10 <br> 10:40</th> 
                <th class="w3-center">10:40 <br> 11:10</th> 
                <th class="w3-center">11:10 <br> 11:40</th> 
                <th class="w3-center">11:40 <br> 12:10</th> 
                <th class="w3-center">12:10 <br> 12:40</th> 
                <th class="w3-center">12:40 <br> 1:10</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <!-- MONDAY -->
                <th class="w3-light-blue">Monday</th>
                <td class="w3-border"> <span class="w3-tag w3-round">Assembly </span></td> 
                <td class="w3-border">Music</td> 
                <td class="w3-border">Numbers</td> 
                <td>Phonetics</td> 
                <td colspan="2" class="w3-yellow w3-center">B R E A K</td> 
                <td class="w3-border">Basic Science</td> 
                <td class="w3-border w3-center">Colouring and Painting</td> 
                <td class="w3-border w3-center">Social Studies</td> 
                <td class="w3-border">STORY</td>  
                <td>SNACK <i class="fas fa-hamburger"></i> <i class="fas fa-coffee"></i></td>
            </tr>

            <tr>
                <!-- TUESDAY -->
                <th class="w3-light-blue">Tuesday</th>                
                <td class="w3-border"><span class="w3-tag w3-round w3-light-green">Sport <i class="fas fa-running"></i></span></td> 
                <td class="w3-border">Rhyme</td> 
                <td class="w3-border">Letters</td> 
                <td>Numbers</td> 
                <td colspan="2" class="w3-yellow w3-center">B R E A K</td> 
                <td class="w3-center">Health Education/French</td> 
                <td colspan="2" class="w3-border w3-center">Creative Arts</td> 
                <td class="w3-border">Rhyme</td>  
                <td>SNACK <i class="fas fa-hamburger"></i> <i class="fas fa-coffee"></i></td>
            </tr>

            <tr>
                <!-- WEDNESDAY -->
                <th class="w3-light-blue">Wednesday</th>                
                <td class="w3-border"> <span class="w3-tag w3-round">Assembly </span></td> 
                <td class="w3-border">Music</td> 
                <td class="w3-border">Numbers</td> 
                <td>Letters</td> 
                <td colspan="2" class="w3-yellow w3-center">B R E A K</td> 
                <td class="w3-border">Social Science</td> 
                <td class="w3-border w3-center">Colouring and Painting</td> 
                <td class="w3-border">Basic Science</td> 
                <td class="w3-border">French</td>  
                <td>SNACK <i class="fas fa-hamburger"></i> <i class="fas fa-coffee"></i></td>
            </tr>

            <tr>
                <!-- THURSDAY --> 
                <th class="w3-light-blue">Thursday</th> 
                <td class="w3-border"><span class="w3-tag w3-round w3-light-green">Sport <i class="fas fa-running"></i></span></td> 
                <td class="w3-border">Rhyme</td> 
                <td class="w3-border">Letters</td> 
                <td>Numbers</td> <td colspan="2" class="w3-yellow w3-center">B R E A K</td> 
                <td class="w3-border w3-center">Creative Art</td> 
                <td class="w3-border w3-center">Health Education</td> 
                <td colspan="2" class="w3-border w3-center">S T O R Y</td> 
                <td>SNACK <i class="fas fa-hamburger"></i> <i class="fas fa-coffee"></i></td>
            </tr>

            <tr class="w3-blue">
                <th >Time</th> 
                <th class="w3-center">7:45 <br> 8:00</th> 
                <th class="w3-center">8:00 <br> 8:30</th> 
                <th class="w3-center">8:30 <br> 9:00</th> 
                <th class="w3-center">9:00 <br> 9:30</th> 
                <th class="w3-center w3-yellow">9:30 <br> 10:00</th> 
                <th class="w3-center w3-yellow">10:30 <br> 11:00</th> 
                <th class="w3-center">11:00 <br> 11:30</th> 
                <th class="w3-center">11:30 <br> 12:00</th> 
                <th colspan="3"></th>
            </tr>

            <tr>
                <!-- FRIDAY -->
                <th class="w3-light-blue">Friday</th> 
                <td class="w3-border"> <span class="w3-tag w3-round">Assembly </span></td> 
                <td class="w3-border">Rhyme</td> 
                <td class="w3-border">Numbers</td> 
                <td>Letters</td> <td colspan="2" class="w3-yellow w3-center">B R E A K</td> 
                <td class="w3-border w3-center">French</td> 
                <td class="w3-border w3-center">Creative Art</td> 
                <td colspan="3"></td>
            </tr>
        </tbody>
    </table>

    <div class="w3-half w3-margin-top">
        <h3>Clubs &amp; Activities</h3>
        <table class="w3-table-all">
            <tr>
                <th class="w3-light-blue">Wednesday</th>
                <td>Karate <strong>8:30 - 9:00</strong></td>
            </tr>
            <tr> 
                <th class="w3-light-blue">Thursday</th>
                <td>
                    Ballet <strong>1:00 - 2:00</strong> <br>
                    Computer <strong>2:00 - 3:00</strong> <br>
                    Poetry <strong>2:00 - 3:00</strong>
                </td>
            </tr>
            <tr>
                <th class="w3-light-blue">Friday</th>
                <td>Computer <strong>12:00 - 1:00</strong></td>
            </tr>
        </table>
    </div>
</div>