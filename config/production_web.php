<?php

$params = require __DIR__ . '/production_params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ylvZZth-5W0jNUyH2Eol7AarOYCcIaia',
        ],

        'toolbox' => [
            'class' =>  'app\components\Toolbox',
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],

        'staff' => [
            'identityClass' => 'app\models\Staff',
            'class'=>'yii\web\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['staff/default/login'],
            //'identityCookie' => [
            //    'name' => '_panelAdmin',
            //]
        ],

        'parent' => [
            'identityClass' => 'app\models\Parents',
            'class'=>'yii\web\User',
            'enableAutoLogin' => false,
            'loginUrl' => ['parents/default/index'],
            //'identityCookie' => [
            //    'name' => '_panelAdmin',
            //]
        ],
        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'n3plcpnl0280.prod.ams3.secureserver.net',  
                'username' => 'info@royalkiddiesschool.com',
                'password' => '4oroagostreet',
                'port' => '465',
                'encryption' => 'ssl'
            ]
        ],
        
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [                
                'staff' => 'staff/default/index',                                
                'parents-portal' => 'parents/default/index',
            ],
        ],
        
    ],
    'modules' => [
        'staff'   => ['class' => 'app\modules\staff\Module'],
        'parents' => ['class' => 'app\modules\parents\Module'],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
