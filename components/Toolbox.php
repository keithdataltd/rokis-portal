<?php
    
    namespace app\components;
   

    use Yii;
    use yii\base\Component;
    use yii\base\InvalidConfigException;
    use app\models\SchoolClass;
    
    class Toolbox extends Component
    {
        public function isAdmin()
        {
            if(Yii::$app->session['staff_role'] == 'admin') return true;
            else return false;            
        }

        public function convertScoreToPercentage($obtainedScore, $obtainableScore, $obtainablePercentage)
        {
            $obtainedPercentage = (float)( ($obtainedScore/$obtainableScore) * $obtainablePercentage );
            return $obtainedPercentage;
        }

        public function isClassTeacher($staffId)
        {
            $class = SchoolClass::findOne(['class_teacher_id' => $staffId]);
            if($class == null) return false;
            else return $class->id;
        }
    }


?>