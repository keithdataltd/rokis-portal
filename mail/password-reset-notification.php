
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;}
		table{
			 border-collapse: collapse;
			 width: 80%;
		}

		th, td {
			border: 1px solid #ddd;
  			padding: 8px;  			
		}

		.red-text{
			color:red;
		}
		
	</style>
</head>
<body>
	
	<p>
		Dear <strong><?= $parent->first_name .' '. $parent->last_name ?></strong>
	</p>

	<h4><u>Parent Portal Password Reset</u></h4>

	<p>
		Your password has been reset. Please see below, your new login password.
	</p>

	<p>
		<strong>Password:</strong> <?= $password ?>
	</p>

	<strong>Please change your password upon successful login.</strong>
	<p>Thank You.</p>
	<p>
		<h6 class="red-text">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY TO THIS EMAIL. FOR FURTHER INFORMATION KINDLY CONTACT SCHOOL ADMIN ON:</h6>
		<h6>
			<a href="tel:08160596849">0816 059 6849</a> | 
			<a href="mailto:info@royalkiddiesschool.com">info@royalkiddiesschool.com</a>
			<a href="mailto:royalkiddiesabj@gmail.com">royalkiddiesabj@gmail.com</a>
		</h6>
	</p>
	

	<p>
		<h5>Royal Kiddies International School</h5>		
	</p>
</body>
</html>