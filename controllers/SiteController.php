<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\SchoolClass;
use app\models\Student;
use app\models\Subject;
use app\models\PrimaryReportCardAttributes;
use app\models\PrimaryReportCardResult;
use app\models\PrimaryReportCard;
use app\models\NurseryReportCard;
use app\models\GeneralSettings;
use app\models\Session;
use yii\base\Model;
use kartik\mpdf\Pdf;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->redirect(['/parents/default/index']);
    }

    public function actionReportcardPdf($id, $type)
    {
        $this->layout = false;
        
        if($type == 'PRI')
        {
            $reportCard = new PrimaryReportCard();
            $reportCard = $reportCard->fetchReportCard($id);
            //var_dump($reportCard); exit();
            $content = $this->renderPartial('primary-report-card-pdf', ['reportCard' => $reportCard]);

            $pdf = new Pdf([
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER,
                'cssInline' => 'caption {
                    caption-side: top;
                }',          
                'options' => ['title' => 'Report Card'],
                'content' => $content
            ]);

            return $pdf->render();
        }
        elseif ($type == 'NUR') {
            $reportCard = NurseryReportCard::findOne($id);
            $content = $this->renderPartial('nursery-report-card-pdf', ['reportCard' => $reportCard]);

            $pdf = new Pdf([
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER,
                'cssInline' => 'caption {
                    caption-side: top;
                }',          
                'options' => ['title' => 'Report Card'],
                'content' => $content
            ]);

            return $pdf->render();
        }
    }

    public function actionMidtermReportPdf($id)
    {
        $subjects = Subject::find()->where(['division_id' => 2])->all();

        $ca = [];
        $isReportSheetComplete = 1;
        $reportCard = new PrimaryReportCard();

        foreach($subjects as $subject)
        {
            $ca[$subject->id] = $reportCard->getSubjectCAScores($id, $subject->id);

            $isReportSheetComplete = $ca[$subject->id]['completeness'] == 0 ? 
               $isReportSheetComplete * 0 : $isReportSheetComplete * 1 ;
        }

        if($isReportSheetComplete)
        {
            $content = $this->renderPartial('midterm-report-pdf', [
                'reportCard' => PrimaryReportCard::findOne($id),
                'scores' => $ca
            ]);

            $pdf = new Pdf([
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER,
                'cssInline' => 'caption {
                    caption-side: top;
                }',          
                'options' => ['title' => 'Midterm Report'],
                'content' => $content
            ]);

            return $pdf->render();
        }

        else
        {
            $this->layout = false;
            //Render exception
            return $this->render('@app/modules/staff/views/primary-report-card/reportcard-error', [
                'error' => '<center>Report Sheet Pending</center>',
                'message' => '<center style="font-size: 18px;">The report sheet you are requesting is under processing. Kindly check back later or contact school admin for more information on availability. <br/> Thank you.
                <br/>
                <br/>
                <button style="width: 200px; padding: 20px; color: white; background-color:red; cursor: pointer; border-radius: 12px; font-size: 20px;" onclick="window.close()">Close </button> </center>'
            ]);
        }

        
    }
    

   

   

    
}
