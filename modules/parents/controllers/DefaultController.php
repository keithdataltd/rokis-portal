<?php

    namespace app\modules\parents\controllers;

    use Yii;
    use yii\web\Controller;
    use app\models\Parents;
    use app\models\Student;
    use app\models\Staff;
    use app\models\Calendar;
    use app\models\ParentLoginForm;
    use app\models\PasswordResetForm;

/**
 * Default controller for the `parents` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = false;
        $model = new ParentLoginForm();

        if($model->load(Yii::$app->request->post()) && $model->login())
        {
           return $this->redirect(['home']);
        }

        $model->password = '';
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionHome()
    {
        $this->layout = '@app/views/layouts/parentLayout';

        return $this->render('home');
    }

    public function actionMyKids()
    {
        $this->layout = '@app/views/layouts/parentLayout';

        $parent = Parents::findOne(Yii::$app->parent->id);

        $kids = $parent->getWardsList();
        $wardsList = [];

        foreach($kids as $kid)
        {   
            $kid = Student::findOne($kid->student_id);
            $wardsList[] = $kid;
        }
        
        return $this->render('my-kids', ['kids' => $wardsList]);
    }

    public function actionTimetable($id)
    {
        $this->layout = '@app/views/layouts/parentLayout';

        $student = Student::findOne($id);

        $currentClass = $student->current_class;
        list($page, $title) = explode('|', $this->getTimetableFile($currentClass));
               
        return $this->render($page, ['title' => $title]);
    }

    public function actionProfile($id)
    {
        $this->layout = '@app/views/layouts/parentLayout';
        $student = Student::findOne($id);

        return $this->render('profile', ['student' => $student]);
    }

    public function actionMyProfile()
    {
        $this->layout = '@app/views/layouts/parentLayout';
        $parent = Parents::findOne(Yii::$app->parent->id);
        

        return $this->render('my-profile', ['parent' => $parent]);
    }

    public function actionUpdateMyProfile()
    {
        $this->layout = '@app/views/layouts/parentLayout';
        $parent = Parents::findOne(Yii::$app->parent->id);

        if ($parent->load(Yii::$app->request->post()) && $parent->save()) {
            return $this->redirect(['my-profile']);
        }

        return $this->render('update-my-profile', ['parent' => $parent]);
    }

    public function actionChangeMyPassword()
    {
        $this->layout = '@app/views/layouts/parentLayout.php';
        $model = new PasswordResetForm();
        $parent = Parents::findOne(Yii::$app->parent->id);

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $hash = hash('sha256', $model->password);
            $parent->password = $hash;
            
            if($parent->save(false)) Yii::$app->session->setFlash('success', "Password reset successful!");
         
            return $this->render('change-my-password', ['model' => $model]);

        }
        return $this->render('change-my-password', ['model' => $model]);
    }

    public function actionReportCards($id)
    {
        $this->layout = '@app/views/layouts/parentLayout';
        $student = Student::findOne($id);
        $reportCards = $student->getMyReportCards();

        return $this->render('report-cards', [
            'student' => $student,
            'reportCards' => $reportCards
        ]);
    }

    public function actionCalendar()
    {
        $this->layout = '@app/views/layouts/parentLayout';
        $calendar = Calendar::find()->orderBy(['start_date' => SORT_ASC])->all();

        return $this->render('calendar', [
            'calendar' => $calendar
        ]); 
    }

    public function actionStaffList()
    {
        $this->layout = '@app/views/layouts/parentLayout';
        $staffList = Staff::findAll(['status' => 'active']);

        return $this->render('staff-list', ['staffList' => $staffList]);
    }

    public function actionLogout()
    {
        Yii::$app->parent->logout();

        return $this->redirect(['index']);
    }

    public function getTimetableFile($class)
    {
        switch ($class) 
        {
            case '3':
                $page = '@app/views/timetables/pre-nursery-timetable';
                $title = "Pre-Nursery Timetable";
                break;
            
            case '4':
                $page = '@app/views/timetables/nursery-1-timetable';
                $title = "Nursery One Timetable";               
                break;
            
            case '5':
                $page = '@app/views/timetables/nursery-2-timetable';
                $title = "Nursery Two Timetable";                
                break;
            
            case '6':
                $page = '@app/views/timetables/grade-1-timetable';
                $title = "Grade One Timetable";
                break;
            
            case '7':
                $page = '@app/views/timetables/grade-2-timetable';
                $title = "Grade Two Timetable";
                break;

            case '8':
                $page = '@app/views/timetables/grade-3-timetable';
                $title = "Grade Three Timetable";
                break;
            
            case '9':
                $page = '@app/views/timetables/grade-4-timetable';
                $title = "Grade Four Timetable";
                break;
            
            case '10':
                $page = '@app/views/timetables/grade-5-timetable';
                $title = "Grade Five Timetable";
                break;

            default:
                $page = '@app/views/timetables/other-timetable';
                $title = "";
                break;
        }

        return $page . '|' . $title;
    }
}
