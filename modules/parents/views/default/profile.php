<?php

use yii\helpers\Html;
use app\models\SchoolClass;
use app\models\Countries;
use app\models\States;
use app\models\Lgas;

?>

<div class="w3-container" style="margin-top:80px" id="showcase">
    <h3 class="w3-jumbo"><b>Pupil Profile</b></h3>
    <h5 class="w3-xxlarge w3-text-blue"><b><?= "{$student->first_name}'s profile" ?></b></h5>
<hr style="width:50px;border:5px solid red" class="w3-round">
</div>

<div class="w3-row-padding w3-white">
    <div class="w3-third w3-white">
        <?= 
            Html::img( (empty($student->picture) ? '/images/student-avatar.png'  : $student->picture) , 
            [   'class' => 'w3-image w3-border w3-center w3-round',
                'style' => 'width:80%',
                'alt' => $student->getFullName()
            ]) 
        ?>
    </div>

    <div class="w3-third w3-white">
        <label class="w3-label" for="">First Name:</label> <?= $student->first_name ?> <br>
        <label class="w3-label" for="">Middle Name:</label> <?= $student->middle_name ?> <br>
        <label class="w3-label" for="">Last Name:</label> <?= $student->last_name ?> <br>
        <label class="w3-label" for="">Student Number:</label> <?= $student->matric_number ?> <br>
        <label class="w3-label" for="">Current Class:</label> <?= SchoolClass::findOne($student->current_class)->class_name ?>  <br>
        <label class="w3-label" for="">Date of Birth:</label> <?= $student->date_of_birth ?> <br>
        <label class="w3-label" for="">Place of Birth:</label> <?= $student->place_of_birth ?> <br>
        <label class="w3-label" for="">Nationality:</label> <?= empty($student->nationality) ? '--' : Countries::findOne(['country_code' => $student->nationality])->country_name ?> <br>
        <label class="w3-label" for="">State of Origin:</label> <?= empty($student->state_of_origin) ? '--' : States::findOne(['id' => $student->state_of_origin])->name ?> <br>
        <label class="w3-label" for="">Local Government Area:</label> <?= empty($student->lga) ? '--' : Lgas::findOne(['id' => $student->lga])->lga_name ?> <br>
        
    </div>

    <div class="w3-third">
        <label class="w3-label" for="">Religion:</label> <?= $student->religion ?> <br>
        <label class="w3-label" for="">Position in Family:</label> <?= $student->position_in_family ?> <br>
        <label class="w3-label" for="">English Spoken at Home?:</label> <?= $student->english_at_home ?> <br>
        <label class="w3-label" for="">Other Spoken Language(s):</label> <?= $student->other_spoken_language ?> <br>      
        <hr>
        <label class="w3-label" for="">Address:</label> <?= $student->address ?> <br>
    </div>
</div>

<div class="w3-row-padding w3-white">
    
    <div class="w3-panel w3-twothird w3-border w3-border-red w3-round">
        <header class="w3-red w3-container">
            Medical Information
        </header>
        <p>
            <?= nl2br(
                empty($student->carrying_information) ? 
                '*NOTE: Dear Parent/Guardian, please ensure that relevant medical information is accurate and up-to-date. 
                Kindly contact school management to update should you need to provide new information ' : 
                $student->medical_information
            ) ?> 
        </p>    
    </div>

    <div class="w3-panel w3-twothird w3-border w3-border-blue w3-round">
        <header class="w3-blue w3-container">
            Carrying Instruction
        </header>
        <p>
            <?= nl2br($student->carrying_instruction) ?> 
        </p>    
    </div>

    <div class="w3-panel w3-twothird w3-border w3-border-green w3-round">
        <header class="w3-green w3-container">
            Special Interests
        </header>
        <p>
            <?= nl2br($student->special_interests) ?> 
        </p>    
    </div>
    
</div>