<?php

use yii\helpers\Html;
use app\models\SchoolClass;
use app\models\Staff;

?>

<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b>My Kids</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>


<div class="w3-container  w3-col">
    <?php if(empty($kids)): ?>
        <span class="w3-tag w3-xlarge w3-round w3-light-blue">Dear Parent/Guardian, kindly contact the school admin to link your ward(s) to your account.</span>
    <?php endif; ?>
    <ul class="w3-ul">            
        <?php foreach($kids as $kid): ?>
            <li class="w3-bar w3-border w3-pale-green w3-border-blue w3-round w3-margin-bottom">
                <?= 
                    Html::img( (empty($kid->picture) ? '/images/student-avatar.png'  : $kid->picture) , 
                    [   'class' => 'w3-bar-item w3-circle w3-border w3-hide-small',
                        'style' => 'width:120px',
                        'alt' => $kid->getFullName()
                    ]) 
                ?>
                
                <div class="w3-bar-item">
                    <span class="w3-xlarge">
                        <b><?= $kid->getFullName() ?></b>
                    </span> <br>
                    <span class="w3-text-red">
                        <strong><?= SchoolClass::findOne($kid->current_class)->class_name ?></strong>                        
                    </span> 
                    
                    <br>

                    <span class="w3-text-deep-orange">
                        <label class="w3-label-orange" for="">Class Teacher:</label>
                        <?= empty(SchoolClass::findOne($kid->current_class)->class_teacher_id) ? '--' : 
                            Staff::findOne(SchoolClass::findOne($kid->current_class)->class_teacher_id)->getFullName()
                        ?>
                    </span> 

                    <br>

                    <span class="w3-text-teal">
                        <label class="w3-label" for="">Assistant Class Teacher:</label>
                        <?= empty(SchoolClass::findOne($kid->current_class)->assistant_class_teacher_id) ? '--' : 
                            Staff::findOne(SchoolClass::findOne($kid->current_class)->assistant_class_teacher_id)->getFullName()
                        ?>
                    </span>
                    
                </div>

                <div class="w3-clear"></div>

                <div class="w3-container w3-center w3-padding-small">
                    <?= Html::a("Timetable", 
                        ['timetable', 'id' => $kid->id], 
                        ['class' => 'w3-button w3-blue w3-hover-light-blue w3-round-xxlarge w3-margin-right']) 
                    ?>

                    <?= Html::a( "{$kid->first_name}'s Profile", 
                        ['profile', 'id' => $kid->id], 
                        ['class' => 'w3-button w3-purple w3-hover-indigo w3-round-xxlarge w3-margin-right']) 
                    ?>

                    <?= Html::a("Report Cards", 
                        ['report-cards', 'id' => $kid->id], 
                        ['class' => 'w3-button w3-amber w3-hover-yellow w3-round-xxlarge w3-margin-right']) 
                    ?>

                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>