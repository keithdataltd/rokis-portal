<?php

use yii\helpers\Html;
use app\models\SchoolClass;
use app\models\Countries;
use app\models\States;

?>

<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b>Staff List</b></h1>    
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>


<div class="w3-container w3-white w3-col">
    <ul class="w3-ul w3-xlarge w3-card-4">
        <?php foreach($staffList as $staff): ?>
            <li class="w3-bar w3-leftbar w3-border w3-border-red">
                
                <img src="/<?= $staff->picture?>" class="w3-bar-item w3-round w3-img w3-hide-small" style="width:100px;" alt="">
                <div class="w3-bar-item">
                    <span class="w3-xlarge"><?= $staff->getFullName() ?></span> <br>
                    <span class="small">
                        <span class="w3-tag w3-round w3-pale-red"><?= $staff->current_designation ?></span> | 
                        <a class="w3-text-green" href="tel:<?= $staff->phone ?>"><?= $staff->phone ?></a>
                        <?php if(!empty($staff->email)): ?> 
                            | <a class="w3-text-indigo" href="mailto:<?= $staff->email ?>"><?= $staff->email ?></a>
                        <?php endif; ?>
                    </span>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
