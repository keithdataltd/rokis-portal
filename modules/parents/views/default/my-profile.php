<?php
    use yii\helpers\Html;

?>

<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b>My Profile</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>

<div class="w3-card w3-twothird w3-white w3-round w3-border w3-border-grey">
    <header class="w3-container w3-dark-grey w3-round">
        <h3><?= $parent->getFullName() ?></h3>
    </header>

    <div class="w3-container">
        <h5 class="w3-center"><strong>Personal Contact Information</strong></h5>
        <p><label class="w3-tag w3-round w3-light-blue" for=""><i class="fas fa-at"></i> Email</label> <?= $parent->email ?></p>
        <p>
            <label class="w3-tag w3-round w3-blue" for=""><i class="fas fa-mobile-alt"></i> Phone</label> <?= $parent->phone ?> <br>
            <label class="w3-tag w3-round w3-blue" for=""><i class="fas fa-mobile-alt"></i> Phone2</label> <?= $parent->phone2 ?> <br>
            <label class="w3-tag w3-round w3-blue" for=""><i class="fas fa-phone"></i> Residential Phone</label> <?= $parent->residence_phone ?>
        </p>

        <p>
            <label class="w3-tag w3-round w3-indigo" for=""><i class="fas fa-map-marker-alt"></i> Address</label> <?= $parent->address ?>
        </p>

        <hr style="width:100%;border:2px solid grey" class="w3-round">

        <h5  class="w3-center">
            <strong>
                Work Contact Information
            </strong>
        </h5>
        <p><label class="w3-tag w3-round w3-lime" for="">Employer</label> <?= $parent->employer ?></p>
        <p><label class="w3-tag w3-round w3-light-green" for="">Office Phone</label> <?= $parent->office_phone ?></p>
        <p>
            <label class="w3-tag w3-round w3-teal" for="">Work Address</label> <?= $parent->work_address ?> <br>
            <label class="w3-tag w3-round w3-teal" for="">Mailing Address</label> <?= $parent->mailing_address ?>
        </p>
    </div>

    <footer class="w3-light-grey w3-padding">
        <?= Html::a("Update", 
            ['update-my-profile'], 
            ['class' => 'w3-button w3-indigo w3-round-xxlarge w3-margin-right']) 
        ?>

        <?= Html::a("Change Password", 
            ['change-my-password'], 
            ['class' => 'w3-button w3-amber w3-round-xxlarge w3-margin-right']) 
        ?>
    </footer>

</div>