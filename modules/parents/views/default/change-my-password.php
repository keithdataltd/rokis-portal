<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

?>

<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b>Change My Password</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>

<div class="w3-container w3-half">

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="w3-panel w3-green w3-display-container w3-round">
            <span onclick="this.parentElement.style.display='none'" class="w3-button w3-large w3-display-topright">&times;</span>
            <h4><i class="far fa-check-circle"></i> &nbsp;Password Updated!</h4>
            <p><?= Yii::$app->session->getFlash('success') ?></p>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([ 'id' => 'password-reset-form','enableClientValidation' => true, ]); ?>

        
        <div class="w3-panel w3-light-grey w3-padding w3-border w3-border-grey w3-round">
            <label for="password">Password</label>			
            <?= $form->field($model, 'password')->passwordInput(['class' => 'w3-input w3-border w3-round-large'])->label(false) ?>
               
            <label for="password_confirm">Confirm Password</label>
            <?= $form->field($model, 'password_confirm')->passwordInput(['class' => 'w3-input w3-border w3-round-large'])->label(false) ?>

            <?= Html::submitButton('Submit', ['class' => 'w3-btn w3-blue w3-round']) ?>
        </div>
        
        

        
    <?php $form = ActiveForm::end() ?>

</div>