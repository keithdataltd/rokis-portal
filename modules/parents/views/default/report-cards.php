<?php

    use yii\helpers\Html;
    use app\models\SchoolClass;
    use app\models\Countries;
    use app\models\States;

?>
<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b><?= "{$student->first_name}'s Report Cards" ?></b></h1>    
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>

<div class="w3-container w3-responsive">
    <table class="w3-table-all w3-striped w3-hover-sand">
        <tr class="w3-blue">
            <th>Class</th>
            <th>Term</th>
            <th>Session</th>
            <th></th>
        </tr>
        <tbody>
            <?php foreach($reportCards as $reportCard): ?>
                <tr class="w3-hover-pale-yellow">
                    <td><?= $reportCard['className'] ?></td>
                    <td><?= Yii::$app->formatter->asOrdinal($reportCard['_term']) . ' Term' ?></td>
                    <td><?= $reportCard['sessionTitle'] ?></td>
                    <td>
                        <?php 
                            if($reportCard['reportCardType'] == 'PRI')
                            {
                               echo Html::a("Midterm Report Card", 
                                        ['/site/midterm-report-pdf', 'id' => $reportCard['rcId'] ], 
                                        [
                                            'class' => 'w3-button w3-green w3-hover-teal w3-round-xxlarge',
                                            'target' => '_blank'
                                    ]);
                            }
                        
                        ?>
                        <?php 
                            if($reportCard['approvalStatus'] == 'approved')
                            {
                                echo Html::a("Terminal Report Card", 
                                        ['/site/reportcard-pdf', 'id' => $reportCard['rcId'], 'type' => $reportCard['reportCardType'] ], 
                                        [
                                            'class' => 'w3-button w3-blue w3-hover-light-blue w3-round-xxlarge',
                                            'target' => '_blank'
                                        ]
                                    );
                            } 
                        
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>