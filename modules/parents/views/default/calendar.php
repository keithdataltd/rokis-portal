<?php

use yii\helpers\Html;
use app\models\SchoolClass;

?>

<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b>Calendar</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>

<div class="w3-container w3-white w3-col">
    <ul class="w3-ul w3-xlarge w3-card-">
        <?php foreach($calendar as $event): ?>
            <li class="w3-bar w3-leftbar w3-border w3-border-purple">
                <div class="w3-right">
                    <span class="w3-left w3-small w3-tag w3-round w3-deep-purple w3-padding-small w3-margin-right">
                        <?= date_format(date_create($event->start_date), 'l d F, Y') .
                            (empty($event->end_date) || $event->start_date===$event->end_date ? '' : 
                            ' - ' . date_format(date_create($event->end_date), 'l d F, Y'))
                        ?>
                    </span>           
                </div>
                
                <div class="w3-bar-item w3-left">
                    <span class="w3-xlarge"><?= $event->title ?></span>                    
                    <br>
                    <span class="w3-small"><?= $event->description ?></span>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>