<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b>Home</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>

<div class="w3-row-padding">
    <div class="w3-half">
        <a href="my-kids" style="text-decoration:none">
            <div class="w3-card-2 w3-padding-32 w3-sand w3-hover-shadow w3-border w3-border-yellow w3-round">
                <div class="w3-container">
                    <i class="fas fa-child w3-xxxlarge w3-text-yellow"></i>
                    <h4><b>My Kids</b></h4>
                    <p>View child's profile, results and timetable</p>
                </div>            
            </div>
        </a>
        
        <br>

        <a href="staff-list" style="text-decoration:none">
            <div class="w3-card-2 w3-padding-32 w3-pale-red w3-hover-shadow w3-border w3-border-red w3-round">
                <div class="w3-container">
                    <i class="fas fa-chalkboard-teacher w3-xxxlarge w3-text-red"></i>
                    <h4><b>Staff List</b></h4>
                    <p>See all school staff</p>
                </div>            
            </div>
        </a>

        <br>      
    </div>

    <div class="w3-half">
        <a href="my-profile" style="text-decoration:none">
            <div class="w3-card-2 w3-padding-32 w3-pale-blue w3-hover-shadow w3-border w3-border-blue w3-round">
                <div class="w3-container">
                    <i class="fas fa-user-circle w3-xxxlarge w3-text-blue"></i>
                    <h4><b>My Profile</b></h4>
                    <p>Update my profile information, Change my password</p>
                </div>            
            </div>
        </a>

        <br>

        <a href="calendar" style="text-decoration:none">
            <div class="w3-card-2 w3-padding-32 w3-pale-green w3-hover-shadow w3-border w3-border-light-green w3-round">
                <div class="w3-container">
                    <i class="fas fa-calendar-alt w3-xxxlarge w3-text-light-green"></i>
                    <h4><b>Calendar</b></h4>
                    <p>See school calendar and upcoming events</p>
                </div>            
            </div>
        </a>

        <br>
    </div>
</div>