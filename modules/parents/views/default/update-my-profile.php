<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

?>

<div class="w3-container" style="margin-top:80px" id="showcase">
    <h1 class="w3-jumbo"><b>Update My Profile</b></h1>
    <hr style="width:50px;border:5px solid red" class="w3-round">
</div>

<div class="w3-container">
    <div class="w3-panel w3-amber w3-round w3-border w3-border-orange">
        <h3>NOTE!</h3>
        <p>Your <strong>email</strong> address is also your login username, and the mail to which notifications will be sent. 
        Ensure to use an active address and one which you can remember easily if you need to change it. </p>
    </div>
</div>

<?php $form = ActiveForm::begin([
        'id' => 'parent-update-form',
        'options' => [
            'class' => 'w3-container w3-padding w3-margin w3-round w3-border w3-border-grey w3-light-grey',
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off'
        ]
    ]); 
?>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($parent, 'first_name')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?> 
        </div>

        <div class="w3-third">
            <?= $form->field($parent, 'middle_name')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($parent, 'last_name')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($parent, 'address')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?>
        </div>      
    </div>

    <div class="w3-row-padding">

        <div class="w3-quarter">
            <?= $form->field($parent, 'email')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?> 
        </div>

        <div class="w3-quarter">
            <?= $form->field($parent, 'phone')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?> 
        </div>

        <div class="w3-quarter">
            <?= $form->field($parent, 'phone2')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?>
        </div>

        <div class="w3-quarter">
            <?= $form->field($parent, 'residence_phone')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?>
        </div>
    </div>

    <hr>

    <div class="w3-row-padding">

        <div class="w3-twothird">
            <?= $form->field($parent, 'employer')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?> 
        </div>

        <div class="w3-third">
            <?= $form->field($parent, 'office_phone')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?> 
        </div>

    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($parent, 'work_address')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?>
        </div>

        <div class="w3-col">
            <?= $form->field($parent, 'mailing_address')
            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-round-large']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= Html::submitButton('Update', ['class' => 'w3-btn w3-blue w3-round']) ?>
        </div>
    </div>



<?php ActiveForm::end(); ?>