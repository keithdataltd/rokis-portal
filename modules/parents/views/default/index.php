<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>
<!DOCTYPE html>
<html>
    <title>ROKIS Parent Portal</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <style>
        body,h1 {font-family: "Raleway", sans-serif}
        body, html {height: 100%}
        .bgimg {
        background-image: url('/images/kids.jpg');
        min-height: 100% ;
        background-position: center;
        background-size: cover;
        }
    </style>
    <body>

        <div class="bgimg w3-display-container w3-mobile w3-text-white">
        
            <div class="w3-display-middle">
                
                <div class="w3-card-4  w3-padding w3-white w3-border w3-border-blue w3-round">
                    <h3 class="w3-blue w3-padding-small w3-border w3-border-yellow w3-round">Royal Kiddies Parents Portal</h3>
                    <div class="w3-center w3-margin-top"><img class="w3-center" src="/images/logo.png" height="90" width="90" alt="ROKIS Logo"></div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options' => ['class' => '']
                        ]); ?>
                    

                        <p>
                            <?= $form->field($model, 'username')
                            ->textInput(['maxlength' => true, 'class' => 'w3-input w3-pale-yellow w3-border w3-border-amber w3-round-large'])->label(false) ?>
                            <label>Email</label>
                        </p>
                        
                        <p>
                            <?= $form->field($model, 'password')
                            ->passwordInput(['class' => 'w3-input w3-pale-yellow w3-border w3-border-amber w3-round-large'])->label(false) ?>
                            <label>Password</label>
                        </p>                        

                        <p>
                            <?= Html::submitButton('Login', ['class' => 'w3-button w3-section w3-blue w3-ripple w3-round']) ?>
                        </p>

                    <?php ActiveForm::end(); ?>                   
                </div>
                
            </div>
            <div class="w3-display-bottomleft w3-padding-large">
                Powered by 
                <a href="http://www.keithdata.com" target="_blank">
                    Keith Data
                </a>
            </div>
        </div>

    </body>
</html>
