<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\States;
use app\models\Lgas;
use yii\jui\DatePicker;
use yii\web\View;


?>

<div class="staff-form">

    <?php $form = ActiveForm::begin([
        'id' => 'staff-creation-form',
        'options' => ['class' => 'w3-container w3-padding w3-border w3-light-grey']
    ]); ?>

    <div class="w3-row-padding">
        <div class="w3-third">
            <img id="imagePreview" src="<?= $model->picture ?>" class="w3-border w3-margin-top w3-img" alt="<?= $model->getFullName() ?>" height="200" width="150">            
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'picture')->fileInput([
                'id' => 'imageUpload',
                'class' => 'w3-input w3-border',
                'onChange' => 'readURL(this)'
            ]) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
           <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?> 
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-half">
            <?= $form->field($model, 'current_designation')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-half">
        <?= $form->field($model, 'staff_role')->dropDownList(['admin' => 'Admin', 'staff' => 'Non-Admin Staff'], ['prompt' => '-- Select Role --', 'class' => 'w3-select w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'state_id')->dropDownList(
                ArrayHelper::map(States::find()->all(), 'id', 'name'), 
                [
                    'prompt' => 'Select State of Origin', 
                    'class' => 'w3-select w3-border',
                    'onChange' => 'getLgas($(this).val())'
                ]) 
            ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'lga_id')->dropDownList($lgas,['class' => 'w3-select w3-border' ]) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'year_of_enrolment')->widget(DatePicker::className(),
            ['clientOptions' => ['changeYear' => true],
            'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-half">
           <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?> 
        </div>

        <div class="w3-half">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

    </div>        

    <div class="w3-row-padding">
        <div class="w3-half">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green w3-margin-bottom']) ?>
        </div>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerJs(
        "function readURL(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext == 'png' || ext == 'jpeg' || ext == 'jpg')) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imagePreview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }else{
                 $('#imagePreview').attr('src', '/assets/no_preview.png');
            }
        }

        function getLgas(id){
            $.post('/staff/default/get-state-lgas', {'id': id}, function(data){
                $('select#staff-lga_id').html(data)});
        }",
        View::POS_END,
        'selected-image-preview'
    );

?>
