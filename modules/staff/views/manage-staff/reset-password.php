<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'Reset Password';
    $this->params['breadcrumbs'][] = ['label' => 'Manage Staff', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $staff->getFullName(), 'url' => ['view', 'id' => $staff->id]];
    $this->params['breadcrumbs'][] = $this->title;
?>


<div class="w3-container w3-half">

    <h1><?= $this->title ?></h1>
    <h3><?= $staff->getFullName() ?></h3>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="w3-panel w3-green w3-display-container w3-round">
            <span onclick="this.parentElement.style.display='none'" class="w3-button w3-large w3-display-topright">&times;</span>
            <h4><i class="far fa-check-circle"></i> &nbsp;Password Updated!</h4>
            <p><?= Yii::$app->session->getFlash('success') ?></p>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([ 'id' => 'password-reset-form','enableClientValidation' => true, ]); ?>

        
        <div class="w3-panel w3-light-grey w3-padding w3-border w3-border-grey">
            <label for="password">Password</label>			
            <?= $form->field($model, 'password')->passwordInput(['class' => 'w3-input w3-border'])->label(false) ?>
               
            <label for="password_confirm">Confirm Password</label>
            <?= $form->field($model, 'password_confirm')->passwordInput(['class' => 'w3-input w3-border'])->label(false) ?>

            <?= Html::submitButton('Submit', ['class' => 'w3-btn w3-blue']) ?>
        </div>
        
        

        
    <?php $form = ActiveForm::end() ?>

</div>