<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

$this->title = 'Update Staff';
$this->params['breadcrumbs'][] = ['label' => 'Manage Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getFullName(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container w3-padding w3-twothird">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $model->getFullName() ?></h3>
    <br>
    
    <?= $this->render('_update_form', [
        'model' => $model,
        'lgas'  => $lgas
    ]) ?>

</div>
