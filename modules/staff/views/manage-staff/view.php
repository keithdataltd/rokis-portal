<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\States;
use app\models\Lgas;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Manage Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="w3-container w3-half">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'w3-btn w3-green w3-padding']) ?>
        <?= Html::a('Reset Password', ['reset-password', 'id' => $model->id], ['class' => 'w3-btn w3-blue w3-padding']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'w3-table-all'],
        'attributes' => [
            [
                'attribute'=>'picture',
                'value'=> $model->picture,
                'format' => ['image',['width'=>'150','height'=>'200']],
            ],
            'first_name',
            'middle_name',
            'last_name',
            'current_designation:ntext',
            'email:email',
            'phone',
            [
                'label' => 'State of Origin',
                'attribute' => 'state_id',
                'value' => empty(States::findOne($model->state_id)->name) ? '--' :   States::findOne($model->state_id)->name             
            ],
            [
                'label' => 'L.G.A',
                'attribute' => 'lga_id',
                'value' => empty(Lgas::findOne($model->lga_id)->lga_name) ? '--' :   Lgas::findOne($model->lga_id)->lga_name             
            ],
            [
                'label' => 'Enrolment Date',
                'attribute' => 'year_of_enrolment',
                'value' => date_format(date_create($model->year_of_enrolment), "M d, Y")             
            ],
            'staff_role',
            'last_login:datetime',
            'status',
            [
                'label' => 'Created On',
                'attribute' => 'creation_date',
                'value' => date_format(date_create($model->creation_date), "M d, Y")               
            ],
        ],
    ]) ?>

</div>
