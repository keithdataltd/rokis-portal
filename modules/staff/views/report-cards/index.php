<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Session;
use app\models\SchoolClass;
use app\models\Student;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PrimaryReportCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Primary Report Cards';
$this->params['breadcrumbs'][] = 'Result Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Enter C.A Result', ['primary-report-card/create-continuous-assessment'], ['class' => 'w3-btn w3-blue']) ?>

        <?= Html::a('Enter Exam Result ', ['primary-report-card/create-exam-result-card'], ['class' => 'w3-btn w3-blue']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'w3-table-all'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Name',
                'attribute' => 'student_name',
                'value' => function($data){
                    return Student::findOne($data->student_id)->getFullName();
                }
            ],

            [
                'label' => 'Class',
                'attribute' => 'class_id',
                'value' => function($data){
                    return SchoolClass::findOne($data->class_id)->class_name;
                },
                'filter' => ArrayHelper::map( SchoolClass::find()->where([ 'class_division' => '2'])->all(), 'id', 'class_name')
            ],

            [
                'label' => 'Session',
                'attribute' => 'session_id',
                'value' => function($data){
                    return Session::findOne($data->session_id)->session_title;
                },
                'filter' => Session::getSessionList()
            ],

            [
                'attribute' => 'term',
                'value' => function($data){
                    return Yii::$app->formatter->asOrdinal($data->term) .' Term';
                },
                'filter' => [
                    1 => '1st Term',
                    2 => '2nd Term',
                    3 => '3rd Term'
                ]
            ],
            
            [
                'attribute' => 'approval_status',
                'value' => function($data){
                    return ucwords($data->approval_status);
                },
                'filter' => [
                    'pending' => 'Pending',
                    'approved' => 'Approved'  
                ]
            ],

            [
                'class' => 'yii\grid\ActionColumn',

                'template' => ' 
                    <span class="w3-tag w3-light-blue">{view-continuous-assessment}</span> 
                    <span class="w3-tag w3-blue">{view-exam-sheet}</span> 
                    <span class="w3-tag w3-cyan">{report-card}</span>',

                'buttons' => [

                    'view-continuous-assessment' => function($url, $model){
                        return Html::a('C.A', ['primary-report-card/view-continuous-assessment', 'id' => $model->id ], ['title' => 'View Continuous Assessment' ]);
                    },

                    'view-exam-sheet' => function($url, $model){
                        return Html::a('Exam', ['primary-report-card/view-exam-sheet', 'id' => $model->id ], ['title' => 'View Exam Sheet' ]);
                    },                    

                    'report-card' => function($url, $model){
                        return Html::a('Full Report Card', ['primary-report-card/view-report-card', 'id' => $model->id ], ['title' => 'Full Report Card' ]);
                    }
                ]
            
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
