<?php
	use app\models\Session;
	use yii\helpers\Html;

	$this->title = "Current Settings";
	$this->params['breadcrumbs'][] = 'System Settings';
	$this->params['breadcrumbs'][] = ['label' => 'Session Configuration', 'url' => ['default/term-session-status']];
	
?>
<div class="w3-container w3-half">
	<h1><?= $this->title ?></h1>
	<p class="w3-border w3-border-amber w3-red w3-padding">
		!!! Ensure the DATE, TERM and SESSION are configured correctly, as this is what is used by the system for REPORT CARD preparation
	</p>
	<div class="w3-panel w3-padding w3-border w3-light-grey">
		<p>
			Current System Date is : <span class="w3-tag w3-round"> <?= date('d M, Y') ?> </span>
		</p>

		<p>
			The current system TERM is: <span class="w3-tag w3-round"> <?= Yii::$app->formatter->asOrdinal($currentSettings->current_term) . ' TERM' ?> </span> 
		</p>

		<p>
			The current system SESSION is: <span class="w3-tag w3-round"> <?= Session::findOne($currentSettings->current_session)->session_title ?> </span>
		</p>
	</div>

	<p>If parameters are incorrect, click on the <b>Configure</b> button below to update appropriate values</p>
	<?= Html::a("Configure", ['configure-term-session'], ['class' => 'w3-bar-item w3-button w3-padding w3-blue']) ?>

</div>