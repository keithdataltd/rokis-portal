<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Royal Kiddies Staff Portal</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/w3.css">
        <style type="text/css">
            body {
                height: 100vh;
              background: url(/images/chalk.jpg);
              background-repeat: no-repeat;
              background-size: cover;
              min-height: ;
            }
        </style>
    </head>
    <body>

        <header class="w3-container w3-blue">
          <h1>ROKIS Staff Portal</h1>
        </header>

        <div class=" w3-half w3-card-4 w3-round-large w3-margin w3-yellow">

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'w3-container  ']
        ]); ?>
        

            <p>
            <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border'])->label(false) ?>
            <label>Username</label></p>
            <p>
            <?= $form->field($model, 'password')->passwordInput(['class' => 'w3-input w3-border'])->label(false) ?>
            <label>Password</label></p>
            

            <p>
                <?= Html::submitButton('Login', ['class' => 'w3-button w3-section w3-blue w3-ripple']) ?>
            </p>

        <?php ActiveForm::end(); ?>

        </div>

    </body>
</html> 

