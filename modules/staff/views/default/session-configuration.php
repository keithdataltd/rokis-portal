<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\jui\DatePicker;
    use yii\helpers\ArrayHelper;
    use app\models\Session;

    $this->title = "Update Configuration";
	$this->params['breadcrumbs'][] = 'System Settings';
	$this->params['breadcrumbs'][] = ['label' => 'Session Configuration', 'url' => ['default/term-session-status']];
	$this->params['breadcrumbs'][] = $this->title;

?>



<div class="w3-container w3-half">
    <h2><?= $this->title ?></h2>
    
    <?php $form = ActiveForm::begin([ 'id' => 'session-settings-configuration-form','enableClientValidation' => true, ]); ?>

        
        <div class="w3-panel w3-light-grey w3-padding w3-border">
            <div class="w3-row-padding">
                <div class="w3-half">
                    <label>Current Session</label>			
                    <?= 
                        $form->field($model, 'current_session')->dropDownList(
                        ArrayHelper::map(Session::find()->all(), 'id', 'session_title'), 
                        [ 'class' => 'w3-select w3-border' ])->label(false) 
                    ?>
                </div>

                <div class="w3-half">
                    <label>Current Term</label>
                    <?= 
                        $form->field($model, 'current_term')->dropDownList(
                        [
                            1 => '1st Term',
                            2 => '2nd Term',
                            3 => '3rd Term'
                        ], 
                        [ 'class' => 'w3-select w3-border' ])->label(false)
                    ?>
                </div>
            </div>
             
            <div class="w3-row-padding">
                <div class="w3-half">
                    <label>Term Start Date</label>
                    <?= $form->field($model, 'term_starts')->widget(DatePicker::className(),
                        ['clientOptions' => ['changeYear' => true],
                        'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border'])->label(false) 
                    ?>
                </div>
                
                <div class="w3-half">
                    <label>Term End Date</label>
                    <?= $form->field($model, 'term_ends')->widget(DatePicker::className(),
                        ['clientOptions' => ['changeYear' => true],
                        'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border'])->label(false) 
                    ?>
                </div>
            </div>
                   
            <br>
            <div class="w3-row-padding">
                <div class="w3-third">
                    <?= Html::submitButton('Submit', ['class' => 'w3-btn w3-green']) ?>
                </div>
            </div>
            
        </div>

    <?php $form = ActiveForm::end() ?>

</div>