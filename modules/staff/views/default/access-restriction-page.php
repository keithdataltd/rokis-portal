<div class="w3-container w3-center">

    <div class='w3-pale-red w3-col w3-margin-top w3-padding w3-border w3-border-red'>
        <h3>You do not have permission to view this page !!!</h3>
        <i class="fas fa-user-lock w3-xxxlarge"></i>
    </div>
</div>