<?php
    use yii\helpers\Html;
    use app\models\Student;
    use app\models\Parents;
    use app\models\Staff;
?>

<div class="w3-container">
    <header class="w3-container" style="padding-top:22px">
        <h1><b><i class="fa fa-dashboard"></i> ROKIS Dashboard</b></h1>
    </header>

    <div class="w3-row-padding">
        <h2 class="w3-center">Stats</h2>

        <div class="w3-quarter">
            <div class="w3-container w3-black w3-border w3-padding-16">
                <div class="w3-left"><i class="far fa-clock w3-xxxlarge"></i></div>
                <div class="w3-left w3-margin-left">
                    <h3>Today is:</h3>
                </div>
                <div class="w3-clear"></div>
                <h4> <?= date('l d F, Y') ?> </h4>
            </div>
        </div>

        <div class="w3-quarter">
            <div class="w3-container w3-red w3-padding-16">
                <div class="w3-left"><i class="fas fa-user-graduate fa-fw w3-xxxlarge"></i></div>
                <div class="w3-right">
                    <h3><?= Student::getStudentCount() ?></h3>
                </div>
                <div class="w3-clear"></div>
                <h4>No. of Students</h4>
            </div>
        </div>

        <div class="w3-quarter">
            <div class="w3-container w3-light-blue w3-padding-16">
                <div class="w3-left"><i class="fas fa-users fa-fw w3-xxxlarge"></i></div>
                <div class="w3-right">
                    <h3><?= Parents::getParentCount() ?></h3>
                </div>
                <div class="w3-clear"></div>
                <h4>No. of Parents</h4>
            </div>
        </div>

        <div class="w3-quarter">
            <div class="w3-container w3-teal w3-padding-16">
                <div class="w3-left"><i class="fas fa-chalkboard-teacher fa-fw w3-xxxlarge"></i></div>
                <div class="w3-right">
                    <h3><?= Staff::getStaffCount() ?></h3>
                </div>
                <div class="w3-clear"></div>
                <h4>No. of Staff</h4>
            </div>
        </div>
    </div>

    <div class="w3-row-padding w3-margin-top">
        <h2 class="w3-center">Quick Links</h2>
        <div class="w3-third">
            <?= Html::a("Enrol Student <i class='fas fa-plus'></i>", ['/staff/student/create'], ['class' => 'w3-button w3-block w3-white w3-border w3-border-red w3-text-red w3-round']) ?>
        </div>

        <div class="w3-third">
            <?= Html::a("Enrol Parent <i class='fas fa-plus'></i>", ['/staff/parents/create'], ['class' => 'w3-button w3-block w3-white w3-border w3-border-light-blue w3-text-light-blue w3-round']) ?>
        </div>

        <div class="w3-third">
            <?= Html::a("Enrol Staff <i class='fas fa-plus'></i>", ['/staff/manage-staff/create'], ['class' => 'w3-button w3-block w3-white w3-border w3-border-teal w3-text-teal w3-round']) ?>
        </div>
    </div>
</div>