<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */

$this->title = 'My Profile';
$this->params['breadcrumbs'][] = ['label' => 'Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="w3-container w3-half">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update-my-profile'], ['class' => 'w3-btn w3-green w3-padding']) ?>
        <?= Html::a('Change My Password', ['change-my-password'], ['class' => 'w3-btn w3-blue w3-padding']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'w3-table-all'],
        'attributes' => [
            'first_name',
            'middle_name',
            'last_name',
            'current_designation:ntext',
            'email:email',
            'phone',
            'last_login',
            'status',
            [
                'label' => 'Created On',
                'attribute'=>'creation_date',
                'value'=>date_format(date_create($model->creation_date), "M d, Y")               
            ],
        ],
    ]) ?>

</div>
