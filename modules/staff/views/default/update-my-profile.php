<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update My Profile';
?>

<div class="w3-container w3-twothird w3-padding">

    <h1><?= Html::encode($this->title) ?></h1>
    <br>

    <?php $form = ActiveForm::begin([
        'id' => 'staff-creation-form',
        'options' => ['class' => 'w3-container w3-border w3-padding w3-light-grey']
    ]); ?>

    

    <div class="w3-row-padding">
        <div class="w3-third">
           <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?> 
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'current_designation')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-half">
           <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border w3-grey', 'readonly' => 'true']) ?> 
        </div>

        <div class="w3-half">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

    </div>

    <div class="w3-row-padding">
        <div class="w3-half">
            <?= Html::submitButton('Update', ['class' => 'w3-btn w3-green']) ?>   
        </div>        
    </div>

    <?php ActiveForm::end(); ?>

</div>
