<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>



<div class="w3-container w3-half">
<h2>Change My Password</h2>
<br>
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="w3-panel w3-green w3-display-container">
        <span onclick="this.parentElement.style.display='none'"
        class="w3-button w3-large w3-display-topright">&times;</span>
        <h4><i class="glyphicon glyphicon-ok-sign"></i> &nbsp;Password Updated!</h4>
        <p><?= Yii::$app->session->getFlash('success') ?></p>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([ 'id' => 'password-reset-form','enableClientValidation' => true, ]); ?>

        
        <div class="w3-panel w3-light-grey w3-padding w3-border">
            <label for="password">Password</label>			
            <?= $form->field($model, 'password')->passwordInput(['class' => 'w3-input w3-border'])->label(false) ?>
               
            <label for="password_confirm">Confirm Password</label>
            <?= $form->field($model, 'password_confirm')->passwordInput(['class' => 'w3-input w3-border'])->label(false) ?>

            <br>
            <?= Html::submitButton('Submit', ['class' => 'w3-btn w3-blue', 'name' => 'submit']) ?>
        </div>
        
        

        
    <?php $form = ActiveForm::end() ?>

</div>