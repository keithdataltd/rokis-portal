<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subject */

$this->title = 'Update Subject';
$this->params['breadcrumbs'][] = ['label' => 'Manage Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subject_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $model->subject_name ?></h3>
    <br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
