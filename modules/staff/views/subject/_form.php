<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Subject */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="w3-half">

    <?php $form = ActiveForm::begin([
        'id' => 'student-creation-form',
        'options' => [
            'class' => 'w3-container w3-border w3-light-grey w3-padding',            
            'autocomplete' => 'off'
        ]
    ]); ?>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'subject_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>
    
    <div class="w3-row-padding">
        <div class="w3-col">
           <?= $form->field($model, 'description')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?> 
        </div>
    </div>
    
    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'division_id')->dropDownList([ 1 => "Nursery", 2 => "Primary"], 
                [
                    'prompt' => 'Select Division', 
                    'class' => 'w3-select w3-border'
                ]) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'first_half_obtainable_marks')->textInput(['maxlength' => true, 'class' => 'w3-select w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'second_half_obtainable_marks')->textInput(['maxlength' => true, 'class' => 'w3-select w3-border']) ?>
        </div>
    </div>   

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green w3-padding']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
