<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Division;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Subjects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container w3-half">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Subject', ['create'], ['class' => 'w3-btn w3-green w3-padding']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'w3-table-all'],
        'emptyCell' => '--',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'subject_name',            
            [
                'label' => 'Division',
                'attribute' => 'division_id',
                'value' => function($data){
                    return Division::findOne($data->division_id)->division_name;
                },
                'filter' => [1 => "Nursery", 2 => "Primary"]
            ],            
            

            [
                'class' => 'yii\grid\ActionColumn',

                'template' => '{view} {update}',

                'buttons' => [

                    'view' => function($url, $model){
                        return Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->id ], ['title' => 'View' ]);
                    },

                    'update' => function($url, $model){
                        return Html::a('<i class="far fa-edit"></i>', ['update', 'id' => $model->id ], ['title' => 'Update' ]);
                    }
                ]
            
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
