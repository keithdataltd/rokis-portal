<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Division;

/* @var $this yii\web\View */
/* @var $model app\models\Subject */

$this->title = $model->subject_name;
$this->params['breadcrumbs'][] = ['label' => 'Subjects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="w3-container w3-third">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'w3-btn w3-green w3-padding']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'w3-table-all'],        
        'attributes' => [
            'subject_name',
            'description:ntext',
            [
                'attribute'=>'division_id',
                'value'=>function($data){
                    return Division::findOne($data->division_id)->division_name;
                }               
            ],
            'first_half_obtainable_marks',
            'second_half_obtainable_marks',
            [
                'label' => 'Created On',
                'attribute'=>'created_on',
                'value'=>date_format(date_create($model->created_on), "M d, Y")               
            ],
        ],
    ]) ?>

</div>
