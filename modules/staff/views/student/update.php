<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = 'Update Pupil';
$this->params['breadcrumbs'][] = ['label' => 'Manage Pupils', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getFullName(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $model->getFullName() ?></h3>

    <?= $this->render('_update_form', [
        'model' => $model,
        'lgas' => $lgas
    ]) ?>

</div>
