<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="w3-panel w3-padding w3-leftbar w3-border-blue" style="width: 75%">
    <p>
        <label class="w3-label ">Name:</label>
        <strong class="w3-text-blue">
        	<?= $parent->first_name . ' ' . $parent->middle_name . ' ' . $parent->last_name?>
        </strong>
    </p>

    <p>
        <label class="w3-label ">Relationship:</label>
        <strong class="w3-text-blue">
        	<?= $relationship ?>
        </strong>
    </p>

    <div class="w3-cell-row">
    	<div class="w3-container w3-cell">
    		<h3 class="w3-green w3-padding-small w3-round">Home Contact Info</h3>
    		<p>
		        <label class="w3-label ">Phone (Main Number):</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->phone?>
		        </strong>
		    </p>

		    <p>
		        <label class="w3-label ">Phone (Alternate Number):</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->phone2 ?>
		        </strong>
		    </p>

		    <p>
		        <label class="w3-label ">Residence Phone:</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->residence_phone ?>
		        </strong>
		    </p>

		    <p>
		        <label class="w3-label ">Email:</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->email ?>
		        </strong>
		    </p>

		    <p>
		        <label class="w3-label ">Home Address:</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->address ?>
		        </strong>
		    </p>
    	</div>

    	<div class="w3-container w3-cell">
    		<h3 class="w3-red w3-padding-small w3-round">Work Contact Info</h3>
    		<p>
		        <label class="w3-label ">Employer</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->employer?>
		        </strong>
		    </p>

		    <p>
		        <label class="w3-label ">Office Phone</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->office_phone?>
		        </strong>
		    </p>

		    <p>
		        <label class="w3-label ">Work Address</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->work_address?>
		        </strong>
		    </p>

		    <p>
		        <label class="w3-label ">Mailing Address</label>
		        <strong class="w3-text-blue">
		        	<?= $parent->mailing_address ?>
		        </strong>
		    </p>
    	</div>
    </div>
</div>