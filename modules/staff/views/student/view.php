<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\SchoolClass;
use app\models\Countries;
use app\models\States;
use app\models\Lgas;
use app\models\Parents;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = ucwords($model->getFullName());
$this->params['breadcrumbs'][] = ['label' => 'Manage Pupils', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$parents = $model->getParents();
$picture = empty($model->picture) ? '/images/student-avatar.png' : $model->picture

?>
<div class="w3-container">
           
    <h1>
        <?= Html::encode($this->title) ?>
        <?= Html::a("Update", ['update', 'id' => $model->id], ['class' => 'w3-btn w3-green w3-large']) ?>        
    </h1>
    <label class="w3-label w3-light-blue w3-padding-small w3-round"><?= $model->matric_number ?></label>

    
    <div class="w3-cell-row">
        <div class="w3-third">
            <img id="imagePreview" src="<?= $picture ?>" class="w3-border" alt="" height="200" width="150">            
        </div>            
    </div>
    <div class="w3-cell-row">
        <div class="w3-container w3-cell">
            <p>
                <label class="w3-label ">Current Class:</label> <br>
                <strong class="w3-text-blue"><?= SchoolClass::findOne($model->current_class)->class_name  ?></strong>
            </p>
            <p>
                <label class="w3-label ">Gender:</label> <br>
                <strong class="w3-text-blue"><?= ucfirst($model->sex)  ?></strong>
            </p>

            <p>
                <label class="w3-label ">Date of Birth:</label> <br>
                <strong class="w3-text-blue">
                    <?= date_format(date_create($model->date_of_birth), "M d, Y")  ?>
                </strong>                    
            </p>

            <p>
                <label class="w3-label ">Religion:</label> <br>
                <strong class="w3-text-blue"><?= ucfirst($model->religion)  ?></strong>
            </p>
            
        </div>

        <div class="w3-container w3-cell">
            <p>
                <label class="w3-label ">Address:</label> <br>
                <strong class="w3-text-blue"><?= $model->address  ?></strong>
            </p>
            <p>
                <label class="w3-label ">Nationality:</label> <br>
                <strong class="w3-text-blue">
                    <?= 
                        empty(Countries::findOne(["country_code" => $model->nationality])->country_name) ?
                        '--' :
                        Countries::findOne(["country_code" => $model->nationality])->country_name ;  
                    ?>
                </strong>
            </p>

            <p>
                <label class="w3-label ">State/LGA:</label> <br>
                <strong class="w3-text-blue">
                    <?= empty($model->state_of_origin) ? '' : States::findOne($model->state_of_origin)->name  ?> /
                    <?= empty($model->lga) ? '' : Lgas::findOne($model->lga)->lga_name  ?>
                </strong>
                
            </p>               
        </div>

        <div class="w3-container w3-cell">
            <p>                    
                <label class="w3-label ">Is English Spoken at Home?:</label> <br>
                <strong class="w3-text-blue"><?= $model->english_at_home  ?></strong>
            </p>
            <p>                    
                <label class="w3-label ">Other Spoken Language(s):</label> <br>
                <strong class="w3-text-blue"><?= $model->other_spoken_language  ?></strong>
            </p>

            <p>
                <label class="w3-label ">Position In Family:</label> <br>
                <strong class="w3-text-blue"><?= $model->position_in_family  ?></strong>
            </p>               
        </div>
    </div>
   

    <div class="w3-panel w3-padding w3-leftbar w3-border-red" style="width: 75%">
        <h5>
            <i class="fas fa-user-md w3-xlarge w3-text-red"></i>
            Medical Information
        </h5>
        <p>
            <?= $model->medical_information ?>
        </p>
    </div>

    <div class="w3-panel w3-padding w3-leftbar w3-border-blue" style="width: 75%">
        <h5>
            <i class="fas fa-info-circle w3-xlarge w3-text-blue"></i>
            Carrying Instruction
        </h5>
        <p>
            <?= $model->carrying_instruction ?>
        </p>
    </div>

    <div class="w3-panel w3-padding w3-leftbar w3-border-green" style="width: 75%">
        <h5>
            <i class="far fa-thumbs-up w3-xlarge w3-text-green"></i>
            Special Interests
        </h5>
        <p>
            <?= $model->special_interests ?>
        </p>
    </div>

    <h2 class="w3-blue w3-padding-small">Parents &amp; Guardians</h2>
    
    <?php if(empty($parents)){ ?>
        <p>No Parent/Guardian linked to this student</p>
    <?php }  else { 

        foreach($parents as $parent)
        {
           echo $this->render('_parental_information_form', [
           'parent' => Parents::findOne($parent->parent_id), 
           'relationship' => $parent->relationship
       ]); 
        }
        
     ?>
    <?php } ?>
        

</div>
