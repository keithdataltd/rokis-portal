<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'matric_number') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'middle_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'date_of_birth') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'nationality') ?>

    <?php // echo $form->field($model, 'place_of_birth') ?>

    <?php // echo $form->field($model, 'state_of_origin') ?>

    <?php // echo $form->field($model, 'lga') ?>

    <?php // echo $form->field($model, 'religion') ?>

    <?php // echo $form->field($model, 'english_at_home') ?>

    <?php // echo $form->field($model, 'other_spoken_language') ?>

    <?php // echo $form->field($model, 'special_interests') ?>

    <?php // echo $form->field($model, 'position_in_family') ?>

    <?php // echo $form->field($model, 'carrying_instruction') ?>

    <?php // echo $form->field($model, 'medical_information') ?>

    <?php // echo $form->field($model, 'current_class') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'picture') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
