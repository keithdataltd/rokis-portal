<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Countries;
use app\models\States;
use app\models\Lgas;
use app\models\SchoolClass;
use yii\web\View;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="w3-col">

    <?php $form = ActiveForm::begin([
        'id' => 'student-creation-form',
        'options' => [
            'class' => 'w3-border w3-light-grey w3-padding',
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off'
        ]
    ]); ?>

    <div class="w3-row-padding">
        <div class="w3-third">
            <img id="imagePreview" src="<?= empty($model->picture) ? '/images/student-avatar.png' : $model->picture ?>" class="w3-border" alt="" height="150" width="130">            
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'picture')->fileInput([
                'id' => 'imageUpload',
                'class' => 'w3-input w3-border',
                'onChange' => 'readURL(this)'
            ]) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'matric_number')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'current_class')->dropDownList(
                ArrayHelper::map(SchoolClass::find()->all(), 'id', 'class_name'), 
                [
                    'prompt' => 'Select Class', 
                    'class' => 'w3-select w3-border'
                ]) ?>
        </div>


    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
           <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?> 
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">

        <div class="w3-third">
            <?= $form->field($model, 'sex')->radioList([ 'female' => 'Female',  'male' => 'Male']) ?>
        </div>
        

        <div class="w3-third">
            <?= $form->field($model, 'date_of_birth')->widget(DatePicker::className(),
            ['clientOptions' => ['changeYear' => true],
            'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'place_of_birth')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>        
        
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'nationality')->dropDownList(
                ArrayHelper::map(Countries::find()->all(), 'country_code', 'country_name'), 
                [
                    'options' => ['NG' => ['Selected' => true]],
                    'prompt' => 'Select Nationality', 
                    'class' => 'w3-select w3-border'
                ]) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'state_of_origin')->dropDownList(
                ArrayHelper::map(States::find()->all(), 'id', 'name'), 
                [
                    'prompt' => 'Select State of Origin', 
                    'class' => 'w3-select w3-border',
                    'onChange' => 'getLgas($(this).val())',                    
                ]) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'lga')->dropDownList($lgas,
                [
                    'class' => 'w3-select w3-border' 
                ]) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'address')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        

        <div class="w3-quarter">
            <?= $form->field($model, 'english_at_home')->radioList([ 'YES' => 'YES', 'NO' => 'NO' ]) ?>
        </div>

        <div class="w3-quarter">
            <?= $form->field($model, 'other_spoken_language')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-quarter">
            <?= $form->field($model, 'religion')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-quarter">
            <?= $form->field($model, 'position_in_family')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'special_interests')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'carrying_instruction')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'medical_information')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'status')->dropDownList(
                ["active" => "Active", "inactive" => "Inactive"], 
                ['class' => 'w3-select w3-border']) ?>
        </div>
    </div> 
    
    <div class="w3-row-padding">
        <div class="w3-third">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green']) ?>
        </div>        
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
    $this->registerJs(
        "function readURL(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0] && (ext == 'png' || ext == 'jpeg' || ext == 'jpg')) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imagePreview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }else{
                 $('#imagePreview').attr('src', '/images/no_preview.png');
            }
        }

        function getLgas(id){
            $.post('/staff/default/get-state-lgas', {'id': id}, function(data){
                $('select#student-lga').html(data)});
        }",
        View::POS_END,
        'selected-image-preview'
    );

?>
