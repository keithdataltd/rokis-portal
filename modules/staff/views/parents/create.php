<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Parents */

$this->title = 'Enrol Parent';
$this->params['breadcrumbs'][] = ['label' => 'Manage Parents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container w3-padding">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
