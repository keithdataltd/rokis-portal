<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = "Reset Password";
$this->params['breadcrumbs'][] = ['label' => 'Manage Parents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->getFullName(), 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = 'Reset Password';

?>



<div class="w3-container w3-half">
<h1><?= $this->title ?></h1>
<h3><?= $user->getFullName() ?></h3>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="w3-panel w3-green w3-display-container">
        <span onclick="this.parentElement.style.display='none'"
        class="w3-button w3-large w3-display-topright">&times;</span>
        <h4><i class="glyphicon glyphicon-ok-sign"></i> &nbsp;Password Updated!</h4>
        <p><?= Yii::$app->session->getFlash('success') ?></p>
        </div>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([ 'id' => 'password-reset-form','enableClientValidation' => true, ]); ?>
        
        <div class="w3-panel w3-light-grey w3-padding w3-border">
            <br>
            <p>Clicking on the <b>Reset Password</b> button below will automatically generate a new password and forward it to the user's email address</p>
            <?= Html::submitButton('Reset Password', ['class' => 'w3-btn w3-blue']) ?>
        </div>  
    
    <?php $form = ActiveForm::end() ?>

</div>