<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Parents */

$this->title = 'Update Parent';
$this->params['breadcrumbs'][] = ['label' => 'Manage Parents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getFullName(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $model->getFullName() ?></h3>

    <br>

    <?= $this->render('_update_form', [
        'model' => $model,
    ]) ?>

</div>
