<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Parents;
use app\models\Student;
use yii\web\View;
use kartik\select2\Select2;

$this->title = "Parents &amp; Wards";
$this->params['breadcrumbs'][] = ['label' => 'Manage Parents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $parent->getFullName(), 'url' => ['view', 'id' => $parent->id]];
$this->params['breadcrumbs'][] = 'Link Parent to Ward';
?>

<div class="w3-container w3-padding">
	
	<div class="w3-cell-row">
		<div class="w3-container w3-cell"></div>
		<div class="w3-container w3-cell">
			<h1><?= $this->title ?></h1>
			<?php $form = ActiveForm::begin([
		        'id' => 'parent-ward-form',
		        'options' => [
		            'class' => 'w3-border w3-light-grey',
		            'enctype' => 'multipart/form-data',
		            'autocomplete' => 'off'
		        ]
		    ]); ?>

		    <div class="w3-panel">
			    <h3 class="w3-blue w3-padding w3-margin-bottom">Link Parent to Ward</h3>
			    <div class="w3-row-padding">
			    	<div class="w3-third">
			    		<p>
			    			<label class="w3-label">Parent Name:</label>
			    			<h5><?= $parent->getFullName(); ?></h5>
			    		</p>
			    		<?= $form->field($model, 'parent_id')->hiddenInput([
			    			'value' => $parent->id
			    		]
			                )->label(false) ?>
			    	</div>

			    	<div class="w3-third">
						<?= 
							$form->field($model, 'student_id')->widget(Select2::classname(), [
								'data' => $studentList,                    
								'options' => ['placeholder' => 'Select student ...']								      
							])->label('Student');
						?>
			    	</div>

			    	<div class="w3-third">		    		
			    		<?= $form->field($model, 'relationship')->textInput(['class' => 'w3-input w3-border']) ?>
			    	</div>
			    </div>

		    	<div class="w3-row-padding">
		        <div class="w3-third">
		            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green']) ?>
		        </div>        
		    	</div>
		    </div>

		    <?php ActiveForm::end(); ?>
		</div>
		<div class="w3-container w3-cell"></div>
	</div>
</div>