<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ParentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Parents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Enrol Parent', ['create'], ['class' => 'w3-btn w3-green w3-padding']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'w3-table-all'],
        'emptyCell' => '--',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'first_name',
            'middle_name',
            'last_name',            
            'phone',
            [
                'class' => 'yii\grid\ActionColumn',

                'template' => '{view} {update}',

                'buttons' => [

                    'view' => function($url, $model){
                        return Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->id ], ['title' => 'View' ]);
                    },

                    'update' => function($url, $model){
                        return Html::a('<i class="far fa-edit"></i>', ['update', 'id' => $model->id ], ['title' => 'Update' ]);
                    }
                ]
            
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
