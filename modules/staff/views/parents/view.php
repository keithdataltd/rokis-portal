<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Student;
use app\models\SchoolClass;


/* @var $this yii\web\View */
/* @var $model app\models\Parents */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Manage Parents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


    $wards = $model->getWardsList();

?>
<div class="w3-container">

    <div class="w3-row-padding">
    
        <div class="w3-half">

            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'w3-btn w3-green w3-padding']) ?>
                <?= Html::a('Link Child/Ward To Parent', ['link-ward', 'id' => $model->id], ['class' => 'w3-btn w3-blue w3-padding']) ?>
                <?= Html::a('Reset Password', ['reset-password', 'id' => $model->id], ['class' => 'w3-btn w3-blue w3-padding']) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => 'w3-table-all'],
                'attributes' => [            
                    'first_name',
                    'middle_name',
                    'last_name',
                    'address:ntext',
                    'email:email',
                    'phone',
                    [
                        'label' => 'Phone (Alternative Number)',
                        'attribute'=>'phone2',
                        'value'=>$model->phone2,                
                    ],
                    'residence_phone',
                    'employer',
                    'office_phone',
                    'work_address:ntext',
                    'mailing_address:ntext',            
                    'last_login',
                    [
                        'label' => 'Created On',
                        'attribute'=>'created_on',
                        'value'=>date_format(date_create($model->created_on), "M d, Y")               
                    ],
                ],
            ]) ?>

        </div>

        <div class="w3-half">
            <br> <br>
            <h3 class="w3-blue w3-padding w3-margin-top">
                Children / Wards
            </h3>

            <?php if(empty($wards)): ?>
                <p>
                    No Child/Ward is currently linked to this parent.
                    Click <?= Html::a('here', ['link-ward', 'id' => $model->id], ['class' => 'w3-black w3-padding-small']) ?>  to add their wards
                </p>
            <?php endif ?>

            <ul class="w3-ul w3-card w3-white w3-margin-bottom w3-border">
                <?php foreach($wards as $ward): ?>
                    <?php $student = Student::findOne($ward->student_id) ?>
                    <li class="w3-bar">
                        <img src="<?= empty($student->picture) ? '/images/student-avatar.png' : $student->picture ?>" class="w3-bar-item w3-round w3-hide-small" style="width:85px">
                        <div class="w3-bar-item">
                            <span class="w3-large">
                                <?= Html::a($student->getFullName(), ['student/view', 'id' => $student->id]) ?>                     
                            </span><br>
                            <span>
                                <?= SchoolClass::findOne($student->current_class)->class_name ?>
                            </span>
                        </div>
                        <span class="w3-right">
                            <?= Html::a('<i class="fas fa-user-times"></i>', 
                                ['unlink-ward', 'pid' => $model->id, 'sid' => $ward->student_id], 
                                [
                                    'class' => 'w3-btn', 
                                    'title' => 'Detach',
                                    'data-confirm' => 'Confirm to detach this child from parent'
                                ]) 
                            ?>
                        </span>  
                    </li>
                <?php endforeach ?>
            </ul>
        </div>

    </div>

</div>
