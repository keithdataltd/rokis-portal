<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Parents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="w3-col">

    <?php $form = ActiveForm::begin([
        'id' => 'parent-creation-form',
        'options' => [
            'class' => 'w3-border w3-padding w3-light-grey',
            'enctype' => 'multipart/form-data',
            'autocomplete' => 'off'
        ]
    ]); ?>

    <div class="w3-row-padding">
        <div class="w3-third">
           <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?> 
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <fieldset class="w3-margin-top">
        <legend>Home Contact Information</legend>
        <div class="w3-row-padding">
            

                <div class="w3-quarter">
                    <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
                </div>

                <div class="w3-quarter">
                    <?= $form->field($model, 'phone2')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
                </div>

                <div class="w3-quarter">
                    <?= $form->field($model, 'residence_phone')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
                </div>

                <div class="w3-quarter">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
                </div>

                <div class="w3-col">
                    <?= $form->field($model, 'address')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?>
                </div>
            
        </div>
    </fieldset>

    <fieldset class="w3-margin-top">
        <legend>Office Contact Information</legend>

        <div class="w3-row-padding">
            
            <div class="w3-half">
                <?= $form->field($model, 'employer')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
            </div>

            <div class="w3-half">
                <?= $form->field($model, 'office_phone')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
            </div>
            
            <div class="w3-col">
                <?= $form->field($model, 'work_address')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?>
            </div>

            <div class="w3-col">
                <?= $form->field($model, 'mailing_address')->textarea(['rows' => 3, 'class' => 'w3-input w3-border']) ?>
            </div>
            
        </div>
    </fieldset>
    
    <div class="w3-row-padding w3-margin-top">
        <div class="w3-third">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green']) ?>
        </div>        
    </div>

    <?php ActiveForm::end(); ?>

</div>
