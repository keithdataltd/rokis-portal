<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolClass */

$this->title = 'Update School Class';
$this->params['breadcrumbs'][] = ['label' => 'Manage Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->class_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $model->class_name ?></h3>
    <br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
