<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Division;
use app\models\Staff;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolClass */

$this->title = $model->class_name;
$this->params['breadcrumbs'][] = ['label' => 'Manage Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="w3-container w3-twothird">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'w3-btn w3-green']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'class_name',
            [
                'label' => 'Division',
                'value' => Division::findOne($model->class_division)->division_name
            ],

            [
                'label' => 'Class Teacher',
                'value' => empty($model->class_teacher_id) ? '--' : 
                            Staff::findOne($model->class_teacher_id)->getFullName()
            ],

            [
                'label' => 'Assistant Class Teacher',
                'value' => empty($model->assistant_class_teacher_id) ? '--' : 
                            Staff::findOne($model->assistant_class_teacher_id)->getFullName()
            ]
        ],
    ]) ?>

</div>
