<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SchoolClassSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Classes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'w3-table-all'],
        'emptyCell' => '--',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'class_name',
            [
                'attribute' => 'class_division',
                'value' => function($data){
                    return $data->class_division == 1 ? 'Nursery' : 'Primary';
                },
                'filter' => ['1' => 'Nursery', '2' => 'Primary']
            ],
            [
                'class' => 'yii\grid\ActionColumn',

                'template' => '{view} {update}',

                'buttons' => [

                    'view' => function($url, $model){
                        return Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->id ], ['title' => 'View' ]);
                    },

                    'update' => function($url, $model){
                        return Html::a('<i class="far fa-edit"></i>', ['update', 'id' => $model->id ], ['title' => 'Update' ]);
                    }
                ]
            
            ],
        ],
    ]); ?>


</div>
