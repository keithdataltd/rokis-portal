<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Staff;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolClass */
/* @var $form yii\widgets\ActiveForm */

$staffs = Staff::find()->all();
$staffList = [];

foreach($staffs as $staff)
{
    $staffList[$staff->id] = $staff->getFullName();
}
?>

<div class="w3-half">

    <?php $form = ActiveForm::begin(['id' => 'class-creation-form',
        'options' => ['class' => 'w3-container w3-border w3-light-grey w3-padding']
        ]); ?>

        <?= $form->field($model, 'class_name')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>

        <?= $form->field($model, 'class_division')->dropDownList( [ 1 => 'Nursery', 2 => 'Primary'], 
        ['class' => 'w3-select w3-border']) ?>

        <?= $form->field($model, 'class_teacher_id')->widget(Select2::classname(), [
                'data' => $staffList,                    
                'options' => ['placeholder' => 'Select Teacher ...']               
            ]); 
        ?>

        <?= $form->field($model, 'assistant_class_teacher_id')->widget(Select2::classname(), [
                'data' => $staffList,                    
                'options' => ['placeholder' => 'Select Teacher ...']               
            ]); 
        ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
