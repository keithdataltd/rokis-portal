<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Staff;
use app\models\Session;

/* @var $this yii\web\View */
/* @var $model app\models\Calendar */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Calendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$term = empty($model->term) ? '--' : Yii::$app->formatter->asOrdinal($model->term) . ' Term';
?>
<div class="w3-container w3-half">

    <h1>Calendar Event</h1>
    <h3><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'w3-btn w3-blue']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'w3-btn w3-red',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'w3-table-all'],
        'attributes' => [
            'id',
            'title',
            'description:ntext',
            [
                'label' => 'Start Date',
                'attribute' => 'start_date',
                'value' => date_format(date_create($model->start_date), "M d, Y")             
            ],
            [
                'label' => 'End Date',
                'attribute' => 'end_date',
                'value' => empty($model->end_date) ? 'N/A' : date_format(date_create($model->end_date), "M d, Y")             
            ],
            [
                'label' => 'Session',
                'attribute' => 'session_id',
                'value' => empty(Session::findOne($model->session_id)->session_title) ? '--' : Session::findOne($model->session_id)->session_title             
            ],
            [
                'label' => 'Term',
                'attribute' => 'term',
                'value' => $term            
            ],
            [
                'label' => 'Created By',
                'attribute' => 'created_by',
                'value' => Staff::findOne($model->created_by)->getFullName()             
            ],
            'created_on:datetime',
        ],
    ]) ?>

</div>
