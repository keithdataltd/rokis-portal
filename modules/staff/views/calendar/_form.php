<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="w3-col">

    <?php $form = ActiveForm::begin([
        'id' => 'calendar-creation-form',
        'options' => ['class' => 'w3-border w3-border-grey w3-light-grey']
    ]); ?>

    <div class="w3-row-padding">
        <div class="w3-col w3-margin-top">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'class' => 'w3-input w3-border']) ?>
        </div>
    </div>
    
    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
        </div>
    </div>
        
    <div class="w3-row-padding">
        <div class="w3-half">
            <?= $form->field($model, 'start_date')->widget(DatePicker::className(),
                ['clientOptions' => ['changeYear' => true],
                'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border']) 
            ?>
        </div>

        <div class="w3-half">
            <?= $form->field($model, 'end_date')->widget(DatePicker::className(),
                ['clientOptions' => ['changeYear' => true],
                'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border']) 
            ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third w3-margin-bottom">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green']) ?>
        </div>        
    </div>

    <?php ActiveForm::end(); ?>

</div>
