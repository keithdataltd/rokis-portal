<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Calendar */

$this->title = 'Update Calendar Event';
$this->params['breadcrumbs'][] = ['label' => 'Manage Calendar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container w3-twothird">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?=$model->title ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
