<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use app\models\ContinuousAssessmentResult;
    use app\models\ContinuousAssessmentType;
    use app\models\Subject;
    use app\models\SchoolClass;
    use app\models\Student;
    use app\models\Session;

    $student = Student::findOne($reportCard->student_id)->getFullName();

    $this->title = 'Primary Report Cards';
    $this->params['breadcrumbs'][] = ['label' => 'Result Management', 'url' => ['report-cards/index']];
    $this->params['breadcrumbs'][] = $this->title;
    $this->params['breadcrumbs'][] = ['label' => 'Continuous Assessment Sheet - ' . $student, 'url' => ['primary-report-cards/view-continuous-assessment', 'id' => $reportCard->id]];
    $this->params['breadcrumbs'][] = ['label' => 'Update', 'url' => ['primary-report-cards/update-continuous-assessment', 'caid' => $ca->id]];

?>

<div class="w3-container w3-twothird">
    <h1>Update Continuous Assessment</h1>
    <h3><?= $student ?></h3>
    <h5 class="w3-text-blue">
        Subject: <?= Subject::findOne($ca->subject_id)->subject_name ?> <br>
        C.A: <?= ContinuousAssessmentType::findOne($ca->ca_type)->ca_title ?> <br>
        Class: <?= SchoolClass::findOne($reportCard->class_id)->class_name ?> <br>
        Term: <?= Yii::$app->formatter->asOrdinal($reportCard->term) . ' Term' ?> <br> 
        Session: <?= Session::findOne($reportCard->session_id)->session_title . ' Session' ?>
    </h5>

    <?php $form = ActiveForm::begin([
        'id' => 'ca-update-form',
        'options' => [
            'class' => 'w3-light-grey w3-margin-top w3-border',            
            'autocomplete' => 'off'
        ]
    ]); ?>

        <div class="w3-row-padding">            
            <div class="w3-third">
                <?= $form->field($ca, 'obtained_score')->textInput(['class' => 'w3-input w3-border']) ?>
            </div>

            <div class="w3-third">
                <?= $form->field($ca, 'obtainable_score')->textInput(['class' => 'w3-input w3-dark-grey w3-border', 'readonly' => true]) ?>
            </div>            
        </div>

        <div class="w3-row-padding">
            <div class="w3-third">
                <?= Html::submitButton('Update', ['class' => 'w3-button w3-margin-bottom w3-blue']) ?>
            </div>
        </div>


    <?php ActiveForm::end(); ?>
</div>

