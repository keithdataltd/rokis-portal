<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\GeneralSettings;
$resultsCount = 0;

$this->title = 'Create Continuous Assessment';
$this->params['breadcrumbs'][] = 'Result Management';
$this->params['breadcrumbs'][] = ['label' => 'Primary Report Cards', 'url' => ['report-cards/index']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>


<div class="w3-container w3-twothird">

    <?php $form = ActiveForm::begin([
        'id' => 'primary-ca-form',
        'enableClientValidation' => false,
        'options' => [
            'class' => 'w3-padding w3-border w3-margin-top',            
            'autocomplete' => 'off'
        ]
    ]); ?>

    <h1 class="w3-blue w3-padding"><?= $this->title ?></h1>

    <div class="w3-row-padding w3-padding w3-border">
        
        <div class="w3-half">
            <label for="subjectList">Subject</label>
            <?= Html::dropDownList('subject', null, $subjects, ['class'=> 'w3-select w3-border', 'prompt' => '-- Select Subject --', 'required' => true]) ?>
        </div>

        <div class="w3-half">
            <label for="caTypesList">C.A Type</label>
            <?= Html::dropDownList('caType', null, $caTypesList, ['class'=> 'w3-select w3-border', 'prompt' => '-- Select Subject --', 'required' => true]) ?>
        </div>

        <div class="w3-col w3-margin-top">
            <label for="caDescription">Description</label>
            <?= Html::input('text', 'caDescription', null, ['class'=> 'w3-input w3-border']) ?>
        </div>
    </div>

    <br>

    <div class="w3-row-padding">
        <div class="w3-col">
            <table class="w3-table-all">
                <thead>
                    <tr class="w3-blue">
                        <th>Attendance</th>
                        <th>Name</th>
                        <th>Score</th>
                        <th>Obtainable Score</th>
                        
                    </tr>
                </thead>

                <tbody>
                    <?php foreach($caResults as $index => $result): ?>
                        <tr>
                            <td>
                                <?= $form->field($result, "[$index]isAbsent")->checkbox([], false)->label(false) ?>
                            </td>
                            <td>
                                <?= $students[$index]->getFullName() ?>
                                <?= $form->field($result, "[$index]student_id")
                                    ->hiddenInput(['value' => $students[$index]->id])->label(false) ?>
                            </td>

                            <td>
                                <?= 
                                    $form->field($result, "[$index]obtained_score")->textInput(['class' => 'w3-input w3-border w3-half'])
                                    ->label('') 
                                ?>
                            </td>
                            
                            <td>
                                <?= 
                                    $form->field($result, "[$index]obtainable_score")->textInput(['class' => 'w3-input w3-border w3-half'])
                                    ->label('') 
                                ?>
                            </td>


                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>

    
    

    
    <br>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green w3-padding']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>