<?php
	use yii\helpers\Html;
	use app\models\Subject;
	use app\models\Student;
	use app\models\Session;
	use app\models\SchoolClass;
	use app\models\ContinuousAssessmentType;

	$caTypes = ContinuousAssessmentType::find()->all();
	$fullName = Student::findOne($reportCard->student_id)->getFullName();

	$this->title = 'Update Report Card';
	$this->params['breadcrumbs'][] = 'Result Management';
	$this->params['breadcrumbs'][] = ['label' => 'Primary Report Cards', 'url' => ['report-cards/index']];
	$this->params['breadcrumbs'][] = "Incomplete Report Card ({$fullName})";
?>

<div class="w3-container w3-margin-top">
	<h2 class="w3-red w3-center">
		Report Card Incomplete!
		<br>
		<span class="w3-large">
			Update the missing scores to generate report card
		</span>
	</h2>
</div>

<div class="w3-container">
	<h3><?= $fullName ?></h3>
	<h5>
		<?= SchoolClass::findOne($reportCard->class_id)->class_name ?>,
		<?= Yii::$app->formatter->asOrdinal($reportCard->term) ?> Term, 
		<?= Session::findOne($reportCard->session_id)->session_title ?> Session

	</h5>
</div>

<div class="w3-container">

	<table class="w3-table-all">
		<thead>
			<tr class="w3-blue">
				<th>Subject</th>
				<?php foreach ($caTypes as $ca): ?>
					<th><?= $ca->ca_title ?> (<?= $ca->ca_percentage ?>%)</th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php foreach($incompleteRecords as $k => $v): ?>
				<tr>
					<td class="w3-text-blue">
						<?= Subject::findOne($k)->subject_name ?></td>
					<td>
						<?= empty($v['test1']) ? '<span class="w3-badge w3-red">x</span>' : $v['test1'] ?>
					</td>
					<td>
						<?= empty($v['test2']) ? '<span class="w3-badge w3-red">x</span>' : $v['test2'] ?>
					</td>
					<td>
						<?= empty($v['test3']) ? '<span class="w3-badge w3-red">x</span>' : $v['test3'] ?>
					</td>
					<td>
						<?= empty($v['test4']) ? '<span class="w3-badge w3-red">x</span>' : $v['test4'] ?>
					</td>
					<td>
						<?= empty($v['homework']) ? '<span class="w3-badge w3-red">x</span>' : $v['homework'] ?>
					</td>
					<td>
						<?= empty($v['classwork']) ? '<span class="w3-badge w3-red">x</span>' : $v['classwork'] ?>
					</td>
					<td>
						<?= empty($v['project']) ? '<span class="w3-badge w3-red">x</span>' : $v['project'] ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>