<?php
    use yii\grid\GridView;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Html;
    use app\models\ContinuousAssessmentResult;
    use app\models\ContinuousAssessmentType;
    use app\models\Subject;
    use app\models\SchoolClass;
    use app\models\Student;
    use app\models\Session;
    use app\models\GeneralSettings;

    
    $dataProvider = new ActiveDataProvider([
        'query' => ContinuousAssessmentResult::find()->where(['report_card_id' => $id]),
        'pagination' => [
            'pageSize' => 7,
        ],
    ]);

    $student = Student::findOne($reportCard->student_id);

    $this->title = 'Primary Report Cards';
    $this->params['breadcrumbs'][] = ['label' => 'Result Management', 'url' => ['report-cards/index']];
    $this->params['breadcrumbs'][] = $this->title;
    $this->params['breadcrumbs'][] = ['label' => 'Continuous Assessment Sheet - ' . $student->getFullName(), 'url' => ['primary-report-cards/view-continuous-assessment', 'id' => $id]];
    
?>

<div class="w3-container w3-twothird">

    <h1>Continuous Assessment Sheet</h1>
    <h3><?= $student->getFullName() ?></h3>
    <h5 class="w3-text-blue">
        <?= SchoolClass::findOne($reportCard->class_id)->class_name ?>, 
        <?= Yii::$app->formatter->asOrdinal($reportCard->term) . ' Term' ?>, 
        <?= Session::findOne($reportCard->session_id)->session_title . ' Session' ?>
    </h5>
    <p>
        <?= 
            Html::a('Create C.A', ['primary-report-card/create-single-ca', 'id' => $student->id], ['class' => 'w3-btn w3-green'])
        ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Subject',
                'value' => function ($data) {
                    return Subject::findOne($data->subject_id)->subject_name;
                },
            ],

            [
                'label' => 'Score',
                'value' => function ($data) {
                    return "{$data->obtained_score}/{$data->obtainable_score}";
                },
            ],

            [
                'label' => 'Percentage (%)',
                'value' => function ($data) {
                    return "{$data->obtained_percentage}/{$data->obtainable_percentage}";
                },
            ],

            [
                'label' => 'C.A',
                'value' => function ($data) {
                    return ContinuousAssessmentType::findOne($data->ca_type)->ca_title;
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update-continuous-assessment}',
                'buttons' => [
                    'update-continuous-assessment' => function($url, $model){
                        $currentSettings = GeneralSettings::findOne(1);
                        $currentTerm = $currentSettings->current_term;
                        $currentSession = $currentSettings->current_session;
                        return ($currentTerm == $model->term && $currentSession == $model->session_id ) ? 
                            Html::a('Update', ['primary-report-card/update-continuous-assessment', 'caid' => $model->id], ['class' => 'w3-btn w3-blue']) : '**';
                    }
                ]
                
            ]

        ]
    ]) ?>

</div>