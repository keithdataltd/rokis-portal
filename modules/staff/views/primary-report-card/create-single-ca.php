<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\GeneralSettings;
$resultsCount = 0;

$this->title = 'Create Continuous Assessment';
$this->params['breadcrumbs'][] = 'Result Management';
$this->params['breadcrumbs'][] = ['label' => 'Primary Report Cards', 'url' => ['report-cards/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="w3-container w3-twothird">

    <?php $form = ActiveForm::begin([
        'id' => 'primary-ca-form',
        'options' => [
            'class' => 'w3-padding w3-border w3-margin-top w3-light-grey',            
            'autocomplete' => 'off'
        ]
    ]); ?>

    <h1 class="w3-blue w3-padding-small">
        <?= $this->title ?> 
    </h1>
    <h3><?= $student->getFullName() ?></h3>

    <div class="w3-row-padding w3-padding w3-border">
        
        <div class="w3-third">
            <label for="subjectList">Subject</label>
            <?= $form->field($caResult, "subject_id")
                ->dropDownList($subjects, ['class' => 'w3-select w3-border', 'prompt' => 'Select Subject'])
                ->label(false) 
            ?>
        </div>

        <div class="w3-third">
            <label for="caTypesList">C.A Type</label>
            <?= $form->field($caResult, "ca_type")
                ->dropDownList($caTypesList, ['class' => 'w3-select w3-border', 'prompt' => 'Select C.A'])
                ->label(false) 
            ?>
        </div>

        <div class="w3-third">
            <label for="obtainableScore">Obtainable Score</label>
            <?= $form->field($caResult, "obtainable_score")
                ->textInput(['class' => 'w3-input w3-border'])
                ->label(false)
            ?>
        </div>

        <div class="w3-col w3-margin-top">
            <label for="caDescription">Description</label>
            <?= $form->field($caResult, "ca_description")
                ->textInput(['class' => 'w3-input w3-border'])
                ->label(false)
            ?>
        </div>

        <hr>

        <div class="w3-third w3-margin-top">
            <label for="caDescription">Score</label>
            <?= $form->field($caResult, "obtained_score")
                ->textInput(['class' => 'w3-input w3-border'])
                ->label(false)
            ?>
        </div>
    </div>

    <br>

    <div class="w3-row">
        <div class="w3-third">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green w3-padding']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>