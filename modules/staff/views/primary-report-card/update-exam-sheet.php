<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\GeneralSettings;
use yii\helpers\ArrayHelper;
use app\models\Student;
use app\models\SchoolClass;
use app\models\Session;

/* @var $this yii\web\View */
/* @var $model app\models\Subject */
/* @var $form yii\widgets\ActiveForm */
//var_dump($results);
$resultsCount = 0;

$this->title = 'Update Exam Result Sheet';
$this->params['breadcrumbs'][] = 'Result Management';
$this->params['breadcrumbs'][] = ['label' => 'Primary Report Cards', 'url' => ['report-cards/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="w3-container w3-col">

    <?php $form = ActiveForm::begin([
        'id' => 'primary-report-card-update-form',
        'enableClientValidation' => false,
        'options' => [
            'class' => 'w3-container w3-margin-top',            
            'autocomplete' => 'off'
        ]
    ]); ?>

    <h1 class="w3-blue w3-padding"><?= $this->title ?></h1>

    <div class="w3-row-padding">
        <div class="w3-panel w3-padding w3-border w3-card">
            <div class="w3-col w3-padding">
                <label class="w3-label">Pupil</label>
                <input type="text" class="w3-input w3-border w3-grey" name="" value="<?= $student->getFullName() ?>" readonly>
            </div>
            <div class="w3-third w3-padding">
                <label class="w3-label">Class</label>
                <input type="text" class="w3-input w3-border w3-grey" name="" 
                    value="<?= SchoolClass::findOne($reportCard->class_id)->class_name ?>" readonly>
            </div>

            <div class="w3-third w3-padding">
                <label class="w3-label">Session</label>
                <input type="text" class="w3-input w3-border w3-grey" name="" 
                    value="<?= Session::findOne($reportCard->session_id)->session_title ?>" readonly>
            </div>

            <div class="w3-third w3-padding">
                <label class="w3-label">Term</label>
                <?= Html::textInput('term', Yii::$app->formatter->asOrdinal($reportCard->term) . ' Term', ['class' => 'w3-input w3-border w3-grey', 'readonly' => true]) ?>
            </div>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <h5 class="w3-tag w3-padding">ATTENDANCE</h5>
            <table class="w3-table-all w3-card">
                <tr>
                    <th>Frequencies</th>
                    <th>Schools Attendance</th>
                </tr>

                
                <tr>
                   <th>Number of times present</th>
                   <td>
                       <?= $form->field($attributes, 'no_of_times_present')->textInput(['class' => 'w3-input w3-border'])->label(false) ?>
                   </td> 
                </tr>

                <tr>
                   <th>Number of times absent</th>
                   <td>
                       <?= $form->field($attributes, 'no_of_times_absent')->textInput(['class' => 'w3-input w3-border'])->label(false) ?>
                   </td> 
                </tr>
            </table>           
        </div>

        
    </div>

    <div class="w3-row-padding w3-margin-top w3-margin-bottom">
        <div class="w3-col">
            <h5 class="w3-tag w3-padding">COGNITIVE ABILITY</h5>
            <table class="w3-table-all w3-card">
                
                <thead>
                    <tr class="w3-teal">
                        <th></th>
                        <th class="w3-half ">Exam Score</th>
                        <th class="w3-half ">Obtainable Score</th>
                        <th>Teacher's Remarks</th>                        
                    </tr> 
                </thead>              

              
                <?php foreach($results as $index => $result): ?>
                    
                    <tr>
                        <th>
                            <?= $subjects[$index]->subject_name ?>
                            <?= $form->field($result, "[$index]subject_id")
                            ->hiddenInput(['value' => $subjects[$index]->id])->label(false) ?>                         
                        </th>
                        <td class="w3-half w3-blue">                           
                            <?=  $form->field($result, "[$index]obtained_score")->textInput(['class' => 'w3-input w3-border w3-text-black'])
                                ->label('Exam Score') ?>
                        </td>

                        <td class="w3-half w3-light-blue">
                           
                            <?=  $form->field($result, "[$index]obtainable_score")->textInput(['class' => 'w3-input w3-border'])
                                ->label('Obtainable Score') ?>
                        </td>         
                        
                        <td>
                            <?=  $form->field($result, "[$index]teachers_remark")->textInput(['class' => 'w3-input w3-border'])->label('Remark') ?>
                        </td>
                    </tr>
                    
                <?php endforeach ?>
            </table>
        </div>
    </div>

    <div class="w3-row-padding w3-margin-top">
        <div class="w3-col">
            <div class="w3-panel w3-padding w3-border w3-white">
                <label class="w3-label">SCALE:</label>
                <span class="w3-tag w3-green">5 - Excellent</span>
                <span class="w3-tag w3-light-green">4 - Good</span>
                <span class="w3-tag w3-yellow">3 - Fair</span>
                <span class="w3-tag w3-amber">2 - Poor</span>
                <span class="w3-tag w3-red">1 - Very Poor</span>
            </div>
        </div>

        <div class="w3-half">
            <h5 class="w3-tag w3-padding">PSYCHOMOTOR SKILLS</h5>
            <table class="w3-table-all w3-card">
                <tr>
                    <td>
                        <?= $form->field($attributes, 'handwriting')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'verbal_fluency')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'games')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'sports')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'handling_tools')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'drawing_and_painting')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'musical_skills')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>
            </table>
        </div>

        <div class="w3-half">
            <h5 class="w3-tag w3-padding">AFFECTIVE AREAS</h5>
            <table class="w3-table-all w3-card">
                <tr>
                    <td>
                        <?= $form->field($attributes, 'punctuality')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'neatness')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'politeness')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'honesty')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'cooperation')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'leadership')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'helping_others')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'emotional_stability')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'health')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'attitude_to_school_work')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'attentiveness')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'perseverance')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?= $form->field($attributes, 'speaking_handwriting')->radioList([
                         1 => '1',
                         2 => '2',
                         3 => '3',
                         4 => '4',
                         5 => '5' ],
                         ['separator' => '&nbsp; &nbsp;']) ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    
    <div class="w3-row-padding">
        <div class="w3-panel w3-border w3-margin-top w3-margin-bottom w3-card">
            <div class="w3-col">
                <?= $form->field($attributes, 'class_teacher_comment')->textarea(['rows' => 2, 'class' => 'w3-input w3-border']) ?>
            </div>

            <div class="w3-col">
                <?= $form->field($attributes, 'headteacher_comment')->textarea(['rows' => 2, 'class' => 'w3-input w3-border']) ?>
            </div>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green w3-padding']) ?>
        </div>
    </div> 
    
    <?php ActiveForm::end(); ?>

</div>
