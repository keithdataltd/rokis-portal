<?php 
	
	use Yii;
	use yii\helpers\Html;	
	use app\models\SchoolClass;
	use app\models\Student;
	use app\models\Subject;
	use app\models\PrimaryReportCardAttributes;
	use app\models\ExamResult;
	use app\models\PrimaryReportCard;
	use app\models\GeneralSettings;
	use app\models\ClassTermHistory;
	use app\models\Session;
	
	$currentSettings = GeneralSettings::findOne(1);
	$currentTerm = $currentSettings->current_term;
	$currentSession = $currentSettings->current_session;
	$nextTermStartDate = ClassTermHistory::findOne(['term' => $reportCard->term , 'session_id' => $reportCard->session_id ])->next_term_start_date;
	
	$this->title = "{$student->getFullName()}'s Exam Sheet";
	$this->params['breadcrumbs'][] = 'Result Management';
	$this->params['breadcrumbs'][] = ['label' => 'Primary Report Cards', 'url' => ['report-cards/index']];
	$this->params['breadcrumbs'][] = $this->title;
	
?>

<div class="w3-container w3-white">
	
	<h1 class="w3-light-blue w3-padding-small w3-center"><?= $this->title ?></h1>

	<div class="w3-panel w3-border w3-padding ">
		<?= ($currentTerm == $reportCard['term'] && $currentSession == $reportCard['session_id'] ) ?
			Html::a('Update <i class="fa fa-edit"></i>', ['update-exam-sheet', 'id' => $reportCard->id ], ['class' => 'w3-btn w3-blue']) : 'Archive'
		?>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<h3>
			<label class="w3-label w3-text-blue w3-padding-small">NAME:</label>
			<?= $student->getFullName() ?>
		</h3>

		<p>
			<span class="w3-tag w3-blue w3-padding-small">
				<?= "CLASS: " . SchoolClass::findOne($reportCard->class_id)->class_name ?>
			</span>

			<span class="w3-tag w3-light-blue w3-padding-small">
				<?= "TERM: " . Yii::$app->formatter->asOrdinal($reportCard->term) . " Term" ?>
			</span>

			<span class="w3-tag w3-blue w3-padding-small">
				<?= "SESSION: " . Session::findOne($reportCard->session_id)->session_title ?>
			</span>
		</p>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<div class="w3-row-padding">
			<div class="w3-half">
				<span class="w3-tag w3-padding-small w3-margin-bottom">ATTENDANCE</span>
				<table class="w3-table-all">
					<tr>
						<th>Frequencies</th><th>Schools Attendance</th>
					</tr>
					<tr>
						<th>Number of Times School Opened</th>
						<td>
							<?= ClassTermHistory::findOne(['term' => $reportCard->term , 'session_id' => $reportCard->session_id ])->times_school_opened ?>
						</td>
					</tr>
					<tr>
						<th>Number of Times Present</th>
						<td>
							<strong><?= $attributes->no_of_times_present ?></strong>
						</td>
					</tr>
					<tr>
						<th>Number of Times Absent</th>
						<td>
							<strong><?= $attributes->no_of_times_absent ?></strong>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<span class="w3-tag w3-padding-small w3-margin-bottom">
			COGNITIVE ABILITY
		</span>

		<table class="w3-table-all">
			<thead>
				<tr>
					<th>Subject</th>					
					<th>Exam Score</th>
					<th>%</th>
				</tr>
			</thead>

			<tbody>
				<?php foreach($results as $result): ?>
					<tr>
						<th>
							<?= Subject::findOne($result->subject_id)->subject_name ?>
						</th>						

						<td>
							<?= $result['obtained_score'] ?> /
							<?= $result['obtainable_score'] ?>
						</td>
						<td>
							<?= $result['obtained_percentage'] ?> /
							<?= $result['obtainable_percentage'] ?>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<div class="w3-row-padding">
			
			<div class="w3-half">
		
				<span class="w3-tag w3-padding-small w3-margin-bottom">
					PSYCHOMOTOR SKILLS
				</span>

				<!-- * Psychomotor Table -->
				<table class="w3-table-all">
					<tr>
						<td></td>
						<th>GRADE</th>						
					</tr>

					<tr>
						<th>
							Handwriting
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->handwriting ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Verbal Fluency
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->verbal_fluency ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Games
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->games ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Sports
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->sports ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Handling Tools
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->handling_tools ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Drawing &amp; Painting
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->drawing_and_painting ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Musical Skills
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->musical_skills ?>
							</span>
						</td>						
					</tr>
				</table>
				<!--  Psychomotor Table # -->

				<!-- * Grade Scale -->
				<div class="w3-panel">
					<span class="w3-tag w3-padding-small w3-margin-bottom">SCALE</span>
					<p>
						<span class="w3-badge">5</span> &nbsp;<label class="w3-label">  Excellent</label> <br>
						<span class="w3-badge">4</span> &nbsp;<label class="w3-label">  Good</label> <br>
						<span class="w3-badge">3</span> &nbsp;<label class="w3-label">  Fair</label> <br>
						<span class="w3-badge">2</span> &nbsp;<label class="w3-label">  Poor</label> <br>
						<span class="w3-badge">1</span> &nbsp;<label class="w3-label">  Very Poor</label> <br>
					</p>
				</div>
				<!--  Grade Scale # -->
			</div>
			

			<!-- * Affective Areas Table -->
			<div class="w3-half">
				<span class="w3-tag w3-padding-small w3-margin-bottom">
					AFFECTIVE AREAS
				</span>

				<table class="w3-table-all">
					<tr>
						<td></td>
						<th>GRADE</th>						
					</tr>

					<tr>
						<th>
							Punctuality
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->punctuality ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Neatness
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->neatness ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Politeness
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->politeness ?>
							</span>
						</td>
					</tr>

					<tr>
						<th>
							Honesty
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->honesty ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Co-operation with Others
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->cooperation ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Leadership
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->leadership ?>
							</span>
						</td>
						
					</tr>

					<tr>
						<th>
							Helping Others
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->helping_others ?>
							</span>
						</td>
						
					</tr>

					<tr>
						<th>
							Emotional Stability
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->emotional_stability ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Health
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->health ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Attitude to School Work
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->attitude_to_school_work ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Attentiveness
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->attentiveness ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Perseverance
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->perseverance ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Speaking/Handwriting
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->speaking_handwriting ?>
							</span>
						</td>						
					</tr>
				</table>
			</div>
			<!--  Affective Areas Table # -->
		</div>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<div class="w3-row-padding">
			<div class="w3-half">
                <table class="w3-table-all">                   
                    <tr>
                        <th>
                            Class Teacher's Comment 
                        </th>
                        <td>
                            <?= $attributes->class_teacher_comment ?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Head Teacher's Comment 
                        </th>
                        <td>
                            <?= $attributes->headteacher_comment ?>
                        </td>
                    </tr>
				</table>
            </div>

			<div class="w3-half">
				<p>
					<label for="">Next Term Begins</label> <br>
					<strong>
						<?= empty($nextTermStartDate) ? '--' : date_format(date_create($nextTermStartDate), 'l d F, Y') ?>					
					</strong>
				</p>
			</div>
		</div>
	</div>
</div>
