<div class="w3-container">
    
    <div class="w3-panel w3-red w3-center">
        <h2><?= $error ?></h2>
        <p> <?= $message ?> </p>
    </div>
</div>