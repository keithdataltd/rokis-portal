<?php 
	
	//use Yii;
	use yii\helpers\Html;	
	use app\models\SchoolClass;
	use app\models\Student;
	use app\models\Subject;
	use app\models\PrimaryReportCardAttributes;
	use app\models\ExamResult;
	use app\models\PrimaryReportCard;
	use app\models\GeneralSettings;
	use app\models\ClassTermHistory;
	use app\models\Session;

	$currentSettings = GeneralSettings::findOne(1);
	$currentTerm = $currentSettings->current_term;
	$currentSession = $currentSettings->current_session;	
	$student = $reportCard['student'];
	$attributes = $reportCard['attributes'];
	$subjectAverages = [];
	$subjectPositions = [];
	$nextTermStartDate = ClassTermHistory::findOne(['term' => $reportCard['term'] , 'session_id' => $reportCard['session']->id ])->next_term_start_date;
	
	$this->title = "{$student->getFullName()}'s Report Card";
	$this->params['breadcrumbs'][] = 'Result Management';
	$this->params['breadcrumbs'][] = ['label' => 'Primary Report Cards', 'url' => ['report-cards/index']];
	$this->params['breadcrumbs'][] = $this->title;
	//var_dump($reportCard['exam_results']);
	//exit();
?>

<div class="w3-container w3-white">
	
	<h1 class="w3-light-blue w3-padding-small w3-center"><?= $this->title ?></h1>

	<div class="w3-panel w3-border w3-padding ">
		<?= ($currentTerm == $reportCard['term'] && $currentSession == $reportCard['session']->id ) ?
			Html::a('Update <i class="fa fa-edit"></i>', ['update-exam-sheet', 'id' => $reportCard['report_card_id'] ], ['class' => 'w3-btn w3-blue']) : 'Archive'
		?>
		<?= 
			Html::a('Download PDF <i class="far fa-file-pdf w3-text-red"></i>', ['/site/reportcard-pdf', 'id' => $reportCard['report_card_id'], 'type' => 'PRI' ], 
			[
				'class' => 'w3-btn w3-black',
				'target' => '_blank'
			])
		?>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<h3>
			<label class="w3-label w3-text-blue w3-padding-small">NAME:</label>
			<?= $student->getFullName() ?>
		</h3>

		<p>
			<span class="w3-tag w3-blue w3-padding-small">
				<?= "CLASS: " . $reportCard['class'] ?>
			</span>

			<span class="w3-tag w3-light-blue w3-padding-small">
				<?= "TERM: " . Yii::$app->formatter->asOrdinal($reportCard['term']) . " Term" ?>
			</span>

			<span class="w3-tag w3-blue w3-padding-small">
				<?= "SESSION: " . $reportCard['session']->session_title ?>
			</span>
		</p>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<div class="w3-row-padding">
			<div class="w3-half">
				<span class="w3-tag w3-padding-small w3-margin-bottom">ATTENDANCE</span>
				<table class="w3-table-all">
					<tr>
						<th>Frequencies</th><th>Schools Attendance</th>
					</tr>
					<tr>
						<th>Number of Times School Opened</th>
						<td>
							<?= ClassTermHistory::findOne(['term' => $reportCard['term'] , 'session_id' => $reportCard['session']->id ])->times_school_opened ?>
						</td>
					</tr>
					<tr>
						<th>Number of Times Present</th>
						<td>
							<strong><?= $attributes->no_of_times_present ?></strong>
						</td>
					</tr>
					<tr>
						<th>Number of Times Absent</th>
						<td>
							<strong><?= $attributes->no_of_times_absent ?></strong>
						</td>
					</tr>
				</table>
			</div>
			<div class="w3-half">
				<span class="w3-tag w3-padding-small w3-margin-bottom">TOTAL</span>
				<table class="w3-table-all">
					
					<tr>
						<th>Total</th>
						<td>
							<strong><?= number_format($reportCard['report_card_total'], 2) ?></strong>
						</td>
					</tr>
					<tr>
						<th>Average</th>
						<td>
							<strong><?= number_format($reportCard['report_card_average'], 2) ?></strong>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<span class="w3-tag w3-padding-small w3-margin-bottom">
			COGNITIVE ABILITY
		</span>

		<table class="w3-table-all">
			<thead>
				<tr class="w3-black">
					<th>Subject</th>
					<th>1<sup>st</sup> Half <br/>(40%)</th>
					<th>2<sup>nd</sup> Half <br/>(60%)</th>
					<th>Final Result <br/>(100%)</th>
					<th>Class Average</th>
					<th>Position</th>
					<th>Teacher's Remarks</th>
				</tr>
			</thead>

			<tbody>
				<?php foreach($reportCard['exam_results'] as $result): ?>
					<tr>
						<th>
							<?= Subject::findOne($result->subject_id)->subject_name ?>
						</th>

						<td>
							<?= $reportCard[$result->subject_id]['ca']['total'] ?> 
						</td>

						<td>
							<?= $reportCard[$result->subject_id]['exam'] ?>
						</td>

						<td>
							<?= $reportCard[$result->subject_id]['ca']['total'] + $reportCard[$result->subject_id]['exam'] ?>
						</td>

						<td>
							<?= $reportCard[$result->subject_id]['class_average_mark'] ?>
						</td>

						<td>
							<?= Yii::$app->formatter->asOrdinal($reportCard[$result->subject_id]['subject_position']) ?>
						</td>

						<td>
							<?= $reportCard[$result->subject_id]['remark']  ?>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<div class="w3-row-padding">
			
			<div class="w3-half">
		
				<span class="w3-tag w3-padding-small w3-margin-bottom">
					PSYCHOMOTOR SKILLS
				</span>

				<!-- * Psychomotor Table -->
				<table class="w3-table-all">
					<tr>
						<td></td>
						<th>GRADE</th>						
					</tr>

					<tr>
						<th>
							Handwriting
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->handwriting ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Verbal Fluency
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->verbal_fluency ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Games
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->games ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Sports
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->sports ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Handling Tools
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->handling_tools ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Drawing &amp; Painting
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->drawing_and_painting ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Musical Skills
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->musical_skills ?>
							</span>
						</td>						
					</tr>
				</table>
				<!--  Psychomotor Table # -->

				<!-- * Grade Scale -->
				<div class="w3-panel">
					<span class="w3-tag w3-padding-small w3-margin-bottom">SCALE</span>
					<p>
						<span class="w3-badge">5</span> &nbsp;<label class="w3-label">  Excellent</label> <br>
						<span class="w3-badge">4</span> &nbsp;<label class="w3-label">  Good</label> <br>
						<span class="w3-badge">3</span> &nbsp;<label class="w3-label">  Fair</label> <br>
						<span class="w3-badge">2</span> &nbsp;<label class="w3-label">  Poor</label> <br>
						<span class="w3-badge">1</span> &nbsp;<label class="w3-label">  Very Poor</label> <br>
					</p>
				</div>
				<!--  Grade Scale # -->
			</div>
			

			<!-- * Affective Areas Table -->
			<div class="w3-half">
				<span class="w3-tag w3-padding-small w3-margin-bottom">
					AFFECTIVE AREAS
				</span>

				<table class="w3-table-all">
					<tr>
						<td></td>
						<th>GRADE</th>						
					</tr>

					<tr>
						<th>
							Punctuality
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->punctuality ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Neatness
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->neatness ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Politeness
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->politeness ?>
							</span>
						</td>
					</tr>

					<tr>
						<th>
							Honesty
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->honesty ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Co-operation with Others
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->cooperation ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Leadership
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->leadership ?>
							</span>
						</td>
						
					</tr>

					<tr>
						<th>
							Helping Others
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->helping_others ?>
							</span>
						</td>
						
					</tr>

					<tr>
						<th>
							Emotional Stability
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->emotional_stability ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Health
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->health ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Attitude to School Work
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->attitude_to_school_work ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Attentiveness
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->attentiveness ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Perseverance
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->perseverance ?>
							</span>
						</td>						
					</tr>

					<tr>
						<th>
							Speaking/Handwriting
						</th>
						<td>
							<span class="w3-badge">
								<?= $attributes->speaking_handwriting ?>
							</span>
						</td>						
					</tr>
				</table>
			</div>
			<!--  Affective Areas Table # -->
		</div>
	</div>

	<div class="w3-panel w3-border w3-padding">
		<div class="w3-row-padding">
			<div class="w3-half">
                <table class="w3-table-all">
                    <tr>
                        <th>
                            Number of Pupils in Class 
                        </th>
                        <td>
							<?= $reportCard['number_of_students_in_class'] ?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Position in Class 
                        </th>
                        <td>
							<span class="w3-tag">
								<?= Yii::$app->formatter->asOrdinal($reportCard['position_in_class']) ?>
							</span>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Class Teacher's Comment 
                        </th>
                        <td>
                            <?= $attributes->class_teacher_comment ?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Head Teacher's Comment 
                        </th>
                        <td>
                            <?= $attributes->headteacher_comment ?>
                        </td>
                    </tr>
				</table>
            </div>

			<div class="w3-half">
				<p>
					<label for="">Next Term Begins</label> <br>
					<strong>
						<?= empty($nextTermStartDate) ? '--' : date_format(date_create($nextTermStartDate), 'l d F, Y') ?>					
					</strong>
				</p>
			</div>

			
			<div class="w3-col w3-margin-top">
				<?php if($reportCard['approval_status'] == 'pending'): ?>
                	<?= Html::a('Approve',['approve-report-card', 'id' => $reportCard['report_card_id']], ['class' => 'w3-btn w3-blue w3-padding']) ?>                
	            <?php endif; ?>
	            <?php if($reportCard['approval_status'] == 'approved'): ?>
	                <span class="w3-blue w3-padding-small">This report card has been APPROVED</span>                
	            <?php endif; ?>
	        </div>
			
		</div>
	</div>
</div>
