<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Session;

/* @var $this yii\web\View */
/* @var $model app\models\ClassTermHistory */
$term = Yii::$app->formatter->asOrdinal($model->term) . ' Term';
$session = Session::findOne($model->session_id)->session_title . ' Session';
$this->title = $term . ' | ' . $session;
$this->params['breadcrumbs'][] = ['label' => 'Term History Configuration', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="w3-container w3-row-padding">
    <div class="w3-half">
        <h1><?= Html::encode($this->title) ?></h1>
        
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'w3-btn w3-green']) ?>
        </p>

        <table class="w3-table-all">
            <tr>
                <th>Term</th>
                <td><?= Yii::$app->formatter->asOrdinal($model->term) . ' Term' ?></td>
            </tr>

            <tr>
                <th>Session</th>
                <td><?= Session::findOne($model->session_id)->session_title; ?></td>
            </tr>

            <tr>
                <th>Term Start Date</th>
                <td><?= $model->start_date ?></td>
            </tr>

            <tr>
                <th>Term End Date</th>
                <td><?= $model->end_date ?></td>
            </tr>

            <tr>
                <th>Next Term Commencement</th>
                <td><?= $model->next_term_start_date ?></td>
            </tr>

            <tr>
                <th>No. of Times School Opened</th>
                <td><?= $model->times_school_opened ?></td>
            </tr>

            <tr>
                <th>Creation Date</th>
                <td><?= $model->creation_date ?></td>
            </tr>
        </table>
    </div>
     

</div>
