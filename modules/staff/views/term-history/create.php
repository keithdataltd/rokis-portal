<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClassTermHistory */

$this->title = 'Create Term History';
$this->params['breadcrumbs'][] = 'System Settings';
$this->params['breadcrumbs'][] = ['label' => 'Term History Configuration', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
