<?php

use yii\helpers\Html;
use app\models\Session;

/* @var $this yii\web\View */
/* @var $model app\models\ClassTermHistory */

$term = Yii::$app->formatter->asOrdinal($model->term) . ' Term';
$session = Session::findOne($model->session_id)->session_title . ' Session';

$this->title = 'Update Class Term History';
$this->params['breadcrumbs'][] = ['label' => 'Term History Configuration', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "{$term}, {$session}", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container w3-half">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= "{$term}, {$session}" ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
