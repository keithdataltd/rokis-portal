<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Session;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ClassTermHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Term History Configuration';
$this->params['breadcrumbs'][] = 'System Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create New Term History', ['create'], ['class' => 'w3-btn w3-green']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'w3-table-all'],
        'emptyCell' => '--',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'term',
                'value' => function($data){
                    return Yii::$app->formatter->asOrdinal($data->term) .' Term';
                },
                'filter' => [
                    1 => '1st Term',
                    2 => '2nd Term',
                    3 => '3rd Term'
                ]
            ],
            [
                'label' => 'Session',
                'attribute' => 'session_id',
                'value' => function($data){
                    return Session::findOne($data->session_id)->session_title;
                },
                'filter' => Session::getSessionList()
            ],
            'start_date',
            'end_date',
            //'next_term_start_date',
            //'creation_date',

            [
                'class' => 'yii\grid\ActionColumn',

                'template' => '{view} {update}',

                'buttons' => [

                    'view' => function($url, $model){
                        return Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->id ], ['title' => 'View' ]);
                    },

                    'update' => function($url, $model){
                        return Html::a('<i class="far fa-edit"></i>', ['update', 'id' => $model->id ], ['title' => 'Update' ]);
                    }
                ]
            
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
