<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Session;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ClassTermHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="">

    <?php $form = ActiveForm::begin([
        'id' => 'term-history-form',
        'options' => [
            'class' => 'w3-container w3-border w3-light-grey w3-border-grey w3-padding w3-margin-top',            
            'autocomplete' => 'off'
        ]
    ]); ?>

    <div class="w3-row-padding">
        <div class="w3-half">
            <?= $form->field($model, 'term')->dropDownList([ 
                1 => '1st Term', 
                2 => '2nd Term', 
                3 => '3rd Term', ], ['prompt' => '', 'class' => 'w3-select w3-border']) 
            ?>
        </div>

        <div class="w3-half">
            <?= $form->field($model, 'session_id')->dropDownList(
                Session::getSessionList(),
                ['prompt' => 'Select Session', 'class' => 'w3-select w3-border']
            ) 
            ?>
        </div>
    </div>

    

    <div class="w3-row-padding">
        <div class="w3-half">
            <?= $form->field($model, 'start_date')->widget(DatePicker::className(),
                ['clientOptions' => ['changeYear' => true],
                'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border']) 
            ?>        
        </div>

        <div class="w3-half">
            <?= $form->field($model, 'end_date')->widget(DatePicker::className(),
                ['clientOptions' => ['changeYear' => true],
                'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border']) 
            ?>        
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-half">
            <?= $form->field($model, 'next_term_start_date')->widget(DatePicker::className(),
                ['clientOptions' => ['changeYear' => true],
                'dateFormat' => 'y-MM-dd' ])->textInput(['class' => 'w3-input w3-border']) 
            ?>        
        </div>

        <div class="w3-half">
            <?= $form->field($model, 'times_school_opened')->textInput(['class' => 'w3-input w3-border'])
                ->label('No. of Times School Opened')
            ?>        
        </div>
    </div>
    

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
