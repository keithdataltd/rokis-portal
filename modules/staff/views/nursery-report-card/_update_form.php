<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\NurseryReportCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="w3-twothird">

    <?php $form = ActiveForm::begin([
        'id' => 'nursery-report-card-form',
        'options' => [
            'class' => 'w3-padding w3-border w3-light-grey',            
            'autocomplete' => 'off'
        ]
    ]); ?>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'student_id')
            ->hiddenInput()->label(false) ?>
        </div>
        
        <div class="w3-third w3-padding">
            <label class="w3-label">Class</label>
            <?= Html::textInput('class', $class, ['class' => 'w3-input w3-border', 'readonly' => true]) ?>

            <?= $form->field($model, 'class')
            ->hiddenInput()->label(false) ?>
        </div>

        <div class="w3-third w3-padding">
            <label class="w3-label">Session</label>
            <?= Html::textInput('currentSession', $currentSession->session_title, ['class' => 'w3-input w3-border', 'readonly' => true]) ?>
            
            <?= $form->field($model, 'session')
            ->hiddenInput(['value' => $currentSession->id])->label(false) ?>
        </div>

        <div class="w3-third w3-padding">
            <label class="w3-label">Term</label>
            <?= Html::textInput('currentTerm', Yii::$app->formatter->asOrdinal($currentTerm) . ' Term', ['class' => 'w3-input w3-border', 'readonly' => true]) ?>

            <?= $form->field($model, 'term')
            ->hiddenInput(['value' => $currentTerm])->label(false) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'weight')->textInput(['class' => 'w3-input w3-border'])->label('Weight (Kg)') ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'height')->textInput(['class' => 'w3-input w3-border'])->label('Height (cm)') ?>
        </div>       
    </div>
    
    <div class="w3-row-padding">        
    
        <table class="w3-table-all">
            <tr>
                <th>ACTIVITIES</th>
                <th>TEACHER'S OBSERVATIONS</th>
            </tr>

            <tr>
                <th>
                   READING ACTIVITIES 
                </th>
                <td>
                   <?= $form->field($model, 'reading_activities')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 
                </td>
            </tr>

            <tr>
                <th>
                    NUMBERING ACTIVITIES
                </th>
                <td>
                   <?= $form->field($model, 'numbering_activities')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 
                </td>
            </tr>

            <tr>
                <th>
                   CREATIVE ACTIVITIES 
                </th>
                <td>
                    <?= $form->field($model, 'creative_activities')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?>
                </td>
            </tr>

            <tr>
                <th>
                   NURSERY RHYMES 
                </th>
                <td>
                   <?= $form->field($model, 'nursery_rhymes')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 
                </td>
            </tr>

            <tr>
                <th>
                   MUSIC 
                </th>
                <td>
                   <?= $form->field($model, 'music')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 
                </td>
            </tr>

            <tr>
                <th>
                    PHYSICAL ACTIVITIES
                </th>
                <td>
                   <?= $form->field($model, 'physical_activities')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 
                </td>
            </tr>

            <tr>
                <th>
                    GAMES
                </th>
                <td>
                   <?= $form->field($model, 'games')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 

                </td>
            </tr>
        </table>

    </div>

    <br>

    <div class="w3-row-padding">
        <table class="w3-table-all">
            <tr>
                <th>PERSONAL QUALITIES</th>
                <th>TEACHER'S OBSERVATIONS</th>
            </tr>

            <tr>
                <th>
                    SELF CONFIDENCE
                </th>
                <td>
                   <?= $form->field($model, 'self_confidence')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 

                </td>
            </tr>

            <tr>
                <th>
                    SOCIABILITY
                </th>
                <td>
                   <?= $form->field($model, 'sociability')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 

                </td>
            </tr>

            <tr>
                <th>
                    INTELLIGENCE
                </th>
                <td>
                   <?= $form->field($model, 'intelligence')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 

                </td>
            </tr>

            <tr>
                <th>
                    INTEREST AND CURIOSITY
                </th>
                <td>
                   <?= $form->field($model, 'interest_and_curiosity')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 

                </td>
            </tr>

            <tr>
                <th>
                    COMPREHENSION
                </th>
                <td>
                   <?= $form->field($model, 'comprehension')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 

                </td>
            </tr>

            <tr>
                <th>
                    PERSONAL CLEANLINESS
                </th>
                <td>
                   <?= $form->field($model, 'personal_cleanliness')->textarea(['rows' => 2, 'class' => 'w3-input w3-border'])->label(false) ?> 

                </td>
            </tr>
        </table>
    </div>

    <div class="w3-row-padding">
        <div class="w3-third">
            <?= $form->field($model, 'total_attendance')->textInput(['class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'days_absent')->textInput(['class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'punctuality')->textInput(['class' => 'w3-input w3-border']) ?>
        </div>
    </div>

    <div class="w3-row-padding">
        <div class="w3-col">
            <?= $form->field($model, 'special_remarks')->textarea(['rows' => 2, 'class' => 'w3-input w3-border']) ?>
        </div>

        <div class="w3-third">
            <?= $form->field($model, 'promoted_to')->dropDownList([
                2 => 'Play Group',
                3 => 'Reception',
                4 => 'Nursery 1',
                5 => 'Nursery 2'
            ], ['class' => 'w3-select w3-border', 'prompt' => 'Promoted to...'
            ]) ?>
        </div>
    </div>      


    <div class="w3-row-padding">
        <div class="w3-col">
            <?= Html::submitButton('Save', ['class' => 'w3-btn w3-green w3-margin-bottom']) ?>
            <?php if($model->approval_status == 'pending'): ?>
                <?= Html::a('Approve',['approve-report-card', 'id' => $model->id], ['class' => 'w3-btn w3-blue w3-margin-bottom']) ?>                
            <?php endif; ?>
            <?php if($model->approval_status == 'approved'): ?>
                <span class="w3-blue w3-padding-small">This report card has been APPROVED</span>                
            <?php endif; ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
