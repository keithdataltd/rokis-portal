<?php

use yii\helpers\Html;
use app\models\Student;
use app\models\SchoolClass;

$student = Student::findOne($model->student_id);
$class = SchoolClass::findOne($model->class)->class_name;
$reportCardTitle = "{$student->first_name}'s Report Card";
/* @var $this yii\web\View */
/* @var $model app\models\NurseryReportCard */

$this->title = 'Update Report Card';
$this->params['breadcrumbs'][] = 'Result Management';
$this->params['breadcrumbs'][] = ['label' => 'Nursery Report Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $reportCardTitle, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $student->getFullName() ?></h3>
    <br>

    <?= $this->render('_update_form', [
        'class' => $class,
        'model' => $model,
        'currentSession'=> $currentSession,
    	'currentTerm'	=> $currentTerm
    ]) ?>

</div>
