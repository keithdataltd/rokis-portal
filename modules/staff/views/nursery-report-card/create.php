<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NurseryReportCard */

$this->title = 'Create Report Card';
$this->params['breadcrumbs'][] = 'Result Management' ;
$this->params['breadcrumbs'][] = ['label' => 'Nursery Report Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'studentList' => $studentList,
        'currentSession'=> $currentSession,
    	'currentTerm'	=> $currentTerm
    ]) ?>

</div>
