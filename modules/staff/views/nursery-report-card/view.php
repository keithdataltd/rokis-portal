<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Student;
use app\models\SchoolClass;
use app\models\ClassTermHistory;
use app\models\Session;

/* @var $this yii\web\View */
/* @var $model app\models\NurseryReportCard */

$student = Student::findOne($model->student_id);
$this->title = $student->getFullName() ;
$this->params['breadcrumbs'][] = 'Result Management';
$this->params['breadcrumbs'][] = ['label' => 'Report Cards (Nursery)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$nextTermStartDate = ClassTermHistory::findOne(['term' => $model->term , 'session_id' => $model->session ])->next_term_start_date;

\yii\web\YiiAsset::register($this);
?>

<div class="w3-container w3-padding ">
    
    <div class="w3-row-padding">
    
    <h1 class="w3-light-blue w3-padding w3-center">TERMINAL SCHOOL RECORD</h1>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'w3-btn w3-green']) ?>
        <?= 
            Html::a('Download PDF <i class="far fa-file-pdf w3-text-red"></i>', ['/site/reportcard-pdf', 'id' => $model->id, 'type' => 'NUR' ], 
                    [
                        'class' => 'w3-btn w3-black',
                        'target' => '_blank'
                    ])
		?>
    </p>
        <div class="w3-twothird">
            <span class="w3-tag">GENERAL INFO</span>
            <table class="w3-table-all w3-margin-bottom">
                <tr>
                    <th>NAME</th> <td><?= $this->title  ?></td> 
                </tr>
                <tr>
                    <th>WEIGHT</th> <td><?= $model->weight . '(kg)'  ?></td> 
                </tr>
                <tr>
                    <th>HEIGHT</th> <td><?= $model->height . '(cm)'  ?></td> 
                </tr>
                <tr>
                    <th>CLASS</th> <td><?= SchoolClass::findOne($model->class)->class_name  ?></td> 
                </tr>
                <tr>
                    <th>TERM</th> <td><?= Yii::$app->formatter->asOrdinal($model->term) . " Term"  ?></td> 
                </tr>
                <tr>
                    <th>SESSION</th> <td><?= Session::findOne($model->session)->session_title  ?></td> 
                </tr>
            </table>

            <span class="w3-tag">ACTIVITIES</span>
            <table class="w3-table-all w3-margin-bottom">
                <tr>
                    <th>ACTIVITY</th> <th>TEACHER'S OBSERVATION</th>
                </tr>
                <tr>
                    <th>READING ACTIVITIES</th> <td><?= $model->reading_activities  ?></td> 
                </tr>
                <tr>
                    <th>NUMBERING ACTIVITIES</th> <td><?= $model->numbering_activities  ?></td> 
                </tr>
                <tr>
                    <th>CREATIVE ACTIVITIES</th> <td><?= $model->creative_activities ?></td> 
                </tr>
                <tr>
                    <th>NURSERY RHYMES</th> <td><?= $model->nursery_rhymes  ?></td> 
                </tr>
                <tr>
                    <th>MUSIC</th> <td><?= $model->music  ?></td> 
                </tr>
                <tr>
                    <th>PHYSICAL ACTIVITIES</th> <td><?= $model->physical_activities  ?></td> 
                </tr>
                <tr>
                    <th>GAMES</th> <td><?= $model->games  ?></td> 
                </tr>
            </table>

            <span class="w3-tag">PERSONAL QUALITITES</span>
            <table class="w3-table-all w3-margin-bottom">                
                <tr>
                    <th>SELF CONFIDENCE</th> <td><?= $model->self_confidence  ?></td> 
                </tr>
                <tr>
                    <th>SOCIABILITY</th> <td><?= $model->sociability  ?></td> 
                </tr>
                <tr>
                    <th>INTELLIGENCE</th> <td><?= $model->intelligence ?></td> 
                </tr>
                <tr>
                    <th>INTEREST AND CURIOSITY</th> <td><?= $model->interest_and_curiosity  ?></td> 
                </tr>
                <tr>
                    <th>COMPREHENSION</th> <td><?= $model->comprehension  ?></td> 
                </tr>
                <tr>
                    <th>PERSONAL CLEANLINESS</th> <td><?= $model->personal_cleanliness  ?></td> 
                </tr>
                <tr>
                    <th>TOTAL ATTENDANCE</th> 
                    <td>
                        <p class="w3-left">
                            <sup><strong>DAYS ABSENT</strong></sup><br>
                            <?= $model->days_absent ?>
                        </p>

                        <p class="w3-right">
                            <sup><strong>PUNCTUALITY</strong></sup><br>
                            <?= $model->punctuality ?>
                        </p>
                    </td>
                </tr>
            </table>

            <table class="w3-table-all w3-margin-bottom">
                <tr>
                    <td>
                        <p class="w3-left">
                            <sup><strong>SPECIAL REMARKS</strong></sup><br>
                            <?= $model->special_remarks ?>
                        </p>
                    </td> 
                    <td>
                        <p class="w3-left">
                            <sup><strong>PROMOTED TO</strong></sup><br>
                            <?= $model->promoted_to ?>
                        </p>

                        <p class="w3-right">
                            <sup><strong>NEXT TERM BEGINS</strong></sup><br>
                            <?= empty($nextTermStartDate) ? '--' : date_format(date_create($nextTermStartDate), 'l d F, Y') ?>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
