<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NurseryReportCardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nursery-report-card-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'student_id') ?>

    <?= $form->field($model, 'weight') ?>

    <?= $form->field($model, 'height') ?>

    <?= $form->field($model, 'reading_activities') ?>

    <?php // echo $form->field($model, 'numbering_activities') ?>

    <?php // echo $form->field($model, 'creative_activities') ?>

    <?php // echo $form->field($model, 'nursery_rhymes') ?>

    <?php // echo $form->field($model, 'music') ?>

    <?php // echo $form->field($model, 'physical_activities') ?>

    <?php // echo $form->field($model, 'games') ?>

    <?php // echo $form->field($model, 'self_confidence') ?>

    <?php // echo $form->field($model, 'sociability') ?>

    <?php // echo $form->field($model, 'intelligence') ?>

    <?php // echo $form->field($model, 'interest_and_curiosity') ?>

    <?php // echo $form->field($model, 'comprehension') ?>

    <?php // echo $form->field($model, 'personal_cleanliness') ?>

    <?php // echo $form->field($model, 'total_attendance') ?>

    <?php // echo $form->field($model, 'days_absent') ?>

    <?php // echo $form->field($model, 'punctuality') ?>

    <?php // echo $form->field($model, 'special_remarks') ?>

    <?php // echo $form->field($model, 'promoted_to') ?>

    <?php // echo $form->field($model, 'next_term_begins') ?>

    <?php // echo $form->field($model, 'term') ?>

    <?php // echo $form->field($model, 'session') ?>

    <?php // echo $form->field($model, 'class') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
