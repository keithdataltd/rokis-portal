<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Session;
use app\models\SchoolClass;
use app\models\Student;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\NurseryReportCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Nursery Report Cards';
$this->params['breadcrumbs'][] = 'Result Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="w3-container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create New Report Card', ['create'], ['class' => 'w3-btn w3-green w3-padding']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'w3-table-all'],
        'emptyCell' => '--',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'label' => 'Name',
                //'attribute' => 'student_id',
                'value' => function($data){
                    return Student::findOne($data->student_id)->getFullName();
                }
            ],
            
            [
                'label' => 'Class',
                'attribute' => 'class',
                'value' => function($data){
                    return SchoolClass::findOne($data->class)->class_name;
                },
                'filter' => ArrayHelper::map( SchoolClass::find()->where([ 'class_division' => '1'])->all(), 'id', 'class_name')
            ],
            
            [
                'label' => 'Session',
                'attribute' => 'session',
                'value' => function($data){
                    return Session::findOne($data->session)->session_title;
                },
                'filter' => Session::getSessionList()
            ],

            [
                'attribute' => 'term',
                'value' => function($data){
                    return Yii::$app->formatter->asOrdinal($data->term) .' Term';
                },
                'filter' => [
                    1 => '1st Term',
                    2 => '2nd Term',
                    3 => '3rd Term'
                ]
            ],

            [
                'attribute' => 'approval_status',
                'value' => function($data){
                    return ucwords($data->approval_status);
                },
                'filter' => [
                    'pending' => 'Pending',
                    'approved' => 'Approved'  
                ]
            ],
    
            [
                'class' => 'yii\grid\ActionColumn',

                'template' => '{view} {update}',

                'buttons' => [

                    'view' => function($url, $model){
                        return Html::a('<i class="far fa-eye"></i>', ['view', 'id' => $model->id ], ['title' => 'View' ]);
                    },

                    'update' => function($url, $model){
                        return Html::a('<i class="far fa-edit"></i>', ['update', 'id' => $model->id ], ['title' => 'Update' ]);
                    }
                ]
            
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
