<?php

namespace app\modules\staff\controllers;

use Yii;
use app\models\SchoolClass;
use app\models\Student;
use app\models\Subject;
use app\models\PrimaryReportCardAttributes;
use app\models\ExamResult;
use app\models\ContinuousAssessmentResult;
use app\models\ContinuousAssessmentType;
use app\models\PrimaryReportCard;
use app\models\GeneralSettings;
use app\models\Session;
use yii\db\Transaction;
use yii\base\Model;

class PrimaryReportCardController extends \yii\web\Controller
{
    public $layout = '@app/views/layouts/staffLayout.php';
    
    public function actionIndex()
    {
    	return $this->render('index');
    }

    public function actionCreateContinuousAssessment()
    {
        //Check if user is class teacher
        $classId = Yii::$app->toolbox->isClassTeacher(Yii::$app->staff->getId());

        if($classId)
        {
            $generalSettings = GeneralSettings::findOne(1);
    	    $currentSession = $generalSettings->current_session;
            $currentTerm = $generalSettings->current_term;
            
            $students = Student::findAll(['current_class' => $classId]);
            $subjects = Subject::getPrimarySubjects();
            $caTypesList = ContinuousAssessmentType::getCaTypesList();
            

            foreach($students as $student)
    	    {
    		    $caResults[] = new ContinuousAssessmentResult(['scenario' => ContinuousAssessmentResult::SCENARIO_CREATE]);
            }
            
            if( Model::loadMultiple($caResults, Yii::$app->request->post()) && 
                Model::validateMultiple($caResults) )
            {
                
                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                
                try{
                
                    $totalScore = 0;
                    $caType = ContinuousAssessmentType::findOne(['ca_type' => Yii::$app->request->post('caType')]);    
                                    
                    foreach($caResults as $result)
                    {
                        //Do not process pupils marked as 'Absent'
                        if($result->isAbsent) continue;

                        else 
                        {
                            //Check if report card is created
                            if($this->isReportCardExist($result->student_id, $classId, $currentTerm, $currentSession))
                            {
                                $reportCard = PrimaryReportCard::findOne([
                                    'student_id' => $result->student_id,
                                    'class_id' => $classId,
                                    'session_id' => $currentSession,
                                    'term' => $currentTerm]);
                            }

                            else
                            {
                                $reportCard = $this->createReportCard($result->student_id, $classId, $currentTerm, $currentSession);
                            }
                            
                            //Process Result
                            $result->report_card_id = $reportCard->id;
                            $result->subject_id = Yii::$app->request->post('subject');
                            $result->ca_type = Yii::$app->request->post('caType');
                            $result->class_id = $classId;
                            $result->ca_description = Yii::$app->request->post('caDescription');
                            //$result->obtainable_score = Yii::$app->request->post('obtainableScore');
                            $result->obtainable_percentage = $caType->ca_percentage;
                            $result->obtained_percentage = Yii::$app->toolbox->convertScoreToPercentage($result->obtained_score, $result->obtainable_score, $result->obtainable_percentage);
                            $result->session_id = $currentSession;
                            $result->term = $currentTerm;
                            $result->created_by = Yii::$app->staff->getId();
                            $result->created_on = date('Y-m-d H:i:s');
                            $result->save();
                        }
                    }
                    
                    $transaction->commit();
                }
    
                catch(Exception $e)
                {
                    $transaction->rollback();
                }
            }

            return $this->render('create-continuous-assessment', [
                'students' => $students,
                'subjects' => $subjects,
                'caTypesList' => $caTypesList,
                'caResults' => $caResults
            ]);            
        }

        else
        {
            //Render exception
            return $this->render('@app/modules/staff/views/primary-report-card/reportcard-error', [
                'error' => 'Permission Restriction!',
                'message' => 'You are not enabled to perform this action'
            ]);    
        }
    }

    public function actionCreateSingleCa($id)
    {
        //Check if user is class teacher
        $classId = Yii::$app->toolbox->isClassTeacher(Yii::$app->staff->getId());

        if($classId)
        {
            $generalSettings = GeneralSettings::findOne(1);
            $currentSession = $generalSettings->current_session;
            $currentTerm = $generalSettings->current_term;
            
            $student = Student::findOne($id);
            $subjects = Subject::getPrimarySubjects();
            $caTypesList = ContinuousAssessmentType::getCaTypesList();
            $reportCard = PrimaryReportCard::findOne([
                                    'student_id' => $student->id,
                                    'class_id' => $classId,
                                    'session_id' => $currentSession,
                                    'term' => $currentTerm
                            ]);
            $result = new ContinuousAssessmentResult(['scenario' => ContinuousAssessmentResult::SCENARIO_CREATE]);

            if($result->load(Yii::$app->request->post()) && $result->validate())
            {
                $caType = ContinuousAssessmentType::findOne(['ca_type' => $result->ca_type]);
                //Process Result
                $result->report_card_id = $reportCard->id;
                $result->student_id = $id;
                $result->class_id = $classId;
                $result->obtainable_percentage = $caType->ca_percentage;
                $result->obtained_percentage = Yii::$app->toolbox->convertScoreToPercentage($result->obtained_score, $result->obtainable_score, $result->obtainable_percentage);
                $result->created_by = Yii::$app->staff->getId();
                $result->created_on = date('Y-m-d H:i:s');
                $result->session_id = $currentSession;
                $result->term = $currentTerm;
                $result->save();

                return $this->redirect(['view-continuous-assessment', 'id' => $reportCard->id]);
            }

            return $this->render('create-single-ca', [
                'student' => $student,
                'subjects' => $subjects,
                'caTypesList' => $caTypesList,
                'caResult' => $result
            ]);
        }

        else
        {
            //Render exception
            return $this->render('@app/modules/staff/views/primary-report-card/reportcard-error', [
                'error' => 'Permission Restriction!',
                'message' => 'You are not enabled to perform this action'
            ]);
        }
    }

    public function actionCreateExamResultCard()
    {
        //Check if user is class teacher
        $classId = Yii::$app->toolbox->isClassTeacher(Yii::$app->staff->getId());

        if($classId)
        {
            $generalSettings = GeneralSettings::findOne(1);
            $currentSession = $generalSettings->current_session;
            $currentTerm = $generalSettings->current_term;
            $subjects = Subject::find()->where(['division_id' => 2])->all();
            $students = Student::getPrimaryStudentsDropDownList($classId);
            $reportCardAttributes = new PrimaryReportCardAttributes();

            foreach($subjects as $subject)
            {
                $examResults[] = new ExamResult(['scenario' => ExamResult::SCENARIO_CREATE]);
            }

            if( Model::loadMultiple($examResults, Yii::$app->request->post()) && 
                Model::validateMultiple($examResults) && 
                $reportCardAttributes->load(Yii::$app->request->post()) &&
                $reportCardAttributes->validate() )
            {

                $studentId = Yii::$app->request->post('student_id');
                $reportCard = PrimaryReportCard::findOne([
                                    'student_id' => $studentId,
                                    'class_id' => $classId,
                                    'session_id' => $currentSession,
                                    'term' => $currentTerm
                            ]);


                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                
                try{               
                    
                    #Process Subject Results
                    foreach($examResults as $result)
                    {    
                        //Process Result
                        $result->student_id = $studentId;
                        $result->report_card_id = $reportCard->id; 
                        $result->class_id = $classId;
                        $result->term = $currentTerm;
                        $result->session_id = $currentSession;                  
                        $result->obtainable_percentage = Subject::findOne($result->subject_id)->second_half_obtainable_marks;
                        $result->obtained_percentage = Yii::$app->toolbox->convertScoreToPercentage($result->obtained_score, $result->obtainable_score, $result->obtainable_percentage);
                        $result->created_by = Yii::$app->staff->getId();
                        $result->created_on = date('Y-m-d H:i:s');
                        $result->save();                       

                    }

                    #Process Attributes
                    $reportCardAttributes->primary_report_card_id = $reportCard->id;
                    $reportCardAttributes->student_id = $studentId;
                    $reportCardAttributes->save();
                    
                    $transaction->commit();                    
                }
    
                catch(Exception $e)
                {
                    $transaction->rollback();
                }

            }

            return $this->render('create-exam-result-card',[
                'students' => $students,
                'subjects' => $subjects,
                'examResults' => $examResults,
                'currentTerm' =>  $currentTerm,
                'currentSession' => Session::findOne($currentSession),
                'attributes' => $reportCardAttributes
            ]);

        }

        else
        {
            //Render exception
            return $this->render('@app/modules/staff/views/primary-report-card/reportcard-error', [
                'error' => 'Permission Restriction!',
                'message' => 'You are not enabled to perform this action'
            ]);
        }

    }

    public function actionViewExamSheet($id)
    {
        $reportCard = PrimaryReportCard::findOne($id);
        $student = Student::findOne($reportCard->student_id);
        $results = ExamResult::findAll(['report_card_id' => $reportCard->id]);
        $attributes = PrimaryReportCardAttributes::findOne(['primary_report_card_id' => $reportCard->id]);
        return $this->render('view-exam-sheet', [
            'reportCard' => $reportCard,
            'attributes' => $attributes,
            'student' => $student,
            'results' => $results
        ]);
    }

    public function actionViewReportCard($id)
    {
        $reportCard = new PrimaryReportCard();

        $rc = $reportCard->fetchReportCard($id);
                       
        if($rc['completeness'])
        {
            return $this->render('full-report-card', [
                'reportCard' => $rc,
                'attributes' => PrimaryReportCardAttributes::findOne(['primary_report_card_id' => $id])
            ]);
        }

        else
        {
            return $this->render('incomplete-report-card',[
                'incompleteRecords' => $rc['incomplete_CAs'],
                'reportCard' => PrimaryReportCard::findOne($id)
            ]);
        }
    }

    public function actionViewContinuousAssessment($id)
    {

        $reportCard = PrimaryReportCard::findOne($id);

        return $this->render('view-continuous-assessment', ['id' => $id, 'reportCard' => $reportCard]);
    }

    public function actionUpdateExamSheet($id)
    {
        $classId = Yii::$app->toolbox->isClassTeacher(Yii::$app->staff->getId());
        $isAdmin = Yii::$app->toolbox->isAdmin();

        if($classId || $isAdmin)
        {
            $reportCard = PrimaryReportCard::findOne($id);
            $student = Student::findOne($reportCard->student_id);

            $results = ExamResult::findAll(['report_card_id' => $reportCard->id]);
            foreach($results as $r)
            {
                $r->scenario = ExamResult::SCENARIO_UPDATE;
            }
            

            $subjects = Subject::find()->where(['division_id' => 2])->all();
            $attributes = PrimaryReportCardAttributes::findOne(['primary_report_card_id' => $reportCard->id]);

            if( Model::loadMultiple($results, Yii::$app->request->post()) && 
                Model::validateMultiple($results) &&
                $attributes->load(Yii::$app->request->post()) &&
                $attributes->validate() )
            {
                foreach($results as $result)
                {
                    $result->obtainable_percentage = Subject::findOne($result->subject_id)->second_half_obtainable_marks;
                    $result->obtained_percentage = Yii::$app->toolbox->convertScoreToPercentage($result->obtained_score, $result->obtainable_score, $result->obtainable_percentage);
                    $result->save(false);
                }

                return $this->redirect(['view-exam-sheet', 'id' => $reportCard->id]);

            }

            return $this->render('update-exam-sheet', [
                'reportCard' => $reportCard,
                'attributes' => $attributes,
                'subjects' => $subjects,
                'student' => $student,
                'results' => $results
            ]);
        }

        else
        {
            //Render exception
            return $this->render('@app/modules/staff/views/primary-report-card/reportcard-error', [
                'error' => 'Permission Restriction!',
                'message' => 'You are not enabled to perform this action'
            ]);
        }
    }

    public function actionUpdateContinuousAssessment($caid)
    {
        $classId = Yii::$app->toolbox->isClassTeacher(Yii::$app->staff->getId());
        $isAdmin = Yii::$app->toolbox->isAdmin();

        if($classId || $isAdmin)
        {
            $ca = ContinuousAssessmentResult::findOne($caid);
            $ca->scenario = ContinuousAssessmentResult::SCENARIO_UPDATE;
            $reportCard = PrimaryReportCard::findOne($ca->report_card_id);

            if($ca->load(Yii::$app->request->post()))
            {
                $ca->obtained_percentage = Yii::$app->toolbox->convertScoreToPercentage($ca->obtained_score, $ca->obtainable_score, $ca->obtainable_percentage);
                
                if($ca->save()) return $this->redirect(['view-continuous-assessment', 'id' => $ca->report_card_id]);
            }

            return $this->render('update-continuous-assessment', [
                'ca' => $ca, 
                'reportCard' => $reportCard
            ]);
        }

        else
        {
            //Render exception
            return $this->render('@app/modules/staff/views/primary-report-card/reportcard-error', [
                'error' => 'Permission Restriction!',
                'message' => 'You are not enabled to perform this action'
            ]);
        }
    }
   

    public function actionApproveReportCard($id)
    {
        $model = PrimaryReportCard::findOne($id);
        $model->approval_status = 'approved';
        $model->approved_by = Yii::$app->staff->getId();
        $model->approval_date = date('Y-m-d H:i:s');
        $model->save(false);

        return $this->redirect(['view-report-card', 'id' => $id]);
    }

    protected function isReportCardExist($studentId, $class, $term, $session)
    {
        return PrimaryReportCard::find()
                ->where([
                    'student_id' => $studentId, 
                    'class_id' => $class,
                    'term' => $term,
                    'session_id' => $session
                    ])
                ->exists();
    }

    protected function createReportCard($studentId, $class, $term, $session)
    {
        $reportCard = new PrimaryReportCard();
        $reportCard->student_id = $studentId;
        $reportCard->class_id = $class;
        $reportCard->session_id = $session;
        $reportCard->term = $term;
        $reportCard->creation_date = date('Y-m-d H:i:s');
        $reportCard->created_by = Yii::$app->staff->getId();
        $reportCard->save(false);

        return $reportCard;
    }

    protected function findModel($id)
    {
        
    }
}


            