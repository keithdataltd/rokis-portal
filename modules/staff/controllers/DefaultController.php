<?php

namespace app\modules\staff\controllers;

use Yii;
use yii\web\Controller;
use app\models\StaffLoginForm;
use app\models\Staff;
use app\models\Lgas;
use app\models\PasswordResetForm;
use app\models\GeneralSettings;

/**
 * Default controller for the `staff` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
    	$this->layout = false;
        

        $model = new StaffLoginForm();

        if($model->load(Yii::$app->request->post()) && $model->login())
        {
           return $this->redirect(['dashboard']);
        }

        $model->password = '';
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionPdftest()
    {
        $this->layout = false;
        return $this->render('@app/modules/staff/views/primary-report-card/primary-report-card-pdf');
    }

    public function actionTermSessionStatus()
    {
        $this->layout = '@app/views/layouts/staffLayout';

        $model = GeneralSettings::findOne(1);

        return $this->render('term-session-settings', ['currentSettings' => $model]);
    }

    public function actionConfigureTermSession()
    {
        $this->layout = '@app/views/layouts/staffLayout';

        $model = GeneralSettings::findOne(1);

        if($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['term-session-status']);
        }

        return $this->render('session-configuration', ['model' => $model]);
    }

    public function actionDashboard()
    {
    	$this->layout = '@app/views/layouts/staffLayout';

    	return $this->render('dashboard');
    }

    public function actionMyProfile()
    {
        $this->layout = '@app/views/layouts/staffLayout';

        $id = Yii::$app->staff->getId();
        $staff = Staff::findOne($id);        

        return $this->render('my-profile', ['model' => $staff]);
    }

    public function actionUpdateMyProfile()
    {
        $this->layout = '@app/views/layouts/staffLayout';

        $id = Yii::$app->staff->getId();

        $staff = Staff::findOne($id);
        $staff->scenario = Staff::SCENARIO_UPDATE;

        if ($staff->load(Yii::$app->request->post()) && $staff->save()) {
            return $this->redirect(['my-profile']);
        }

        return $this->render('update-my-profile', ['model' => $staff]);
    }

    public function actionChangeMyPassword()
    {
        $this->layout = '@app/views/layouts/staffLayout.php';
        $model = new PasswordResetForm();
        $staff = Staff::findOne(['id' => Yii::$app->staff->getId()]);

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $hash = hash('sha256', $model->password);
            $staff->password = $hash;
            if($staff->save(false)) Yii::$app->session->setFlash('success', "Password reset successful!");
         
            return $this->render('change-my-password', ['model' => $model, 'user' => $staff]);

        }
        return $this->render('change-my-password', ['model' => $model, 'user' => $staff]);
    }

    public function actionLogout()
    {
        Yii::$app->staff->logout();

        return $this->redirect(['index']);
    }

    public function actionTimetable($class)
    {
        $this->layout = '@app/views/layouts/staffLayout';
        $page = '';
        $title = '';

        switch ($class) 
        {
            case 'pre-nursery':
                $page = '@app/views/timetables/pre-nursery-timetable';
                $title = "Pre-Nursery Timetable";
                break;
            
            case 'nursery1':
                $page = '@app/views/timetables/nursery-1-timetable';
                $title = "Nursery One Timetable";               
                break;
            
            case 'nursery2':
                $page = '@app/views/timetables/nursery-2-timetable';
                $title = "Nursery Two Timetable";                
                break;
            
            case 'grade1':
                $page = '@app/views/timetables/grade-1-timetable';
                $title = "Grade One Timetable";
                break;
            
            case 'grade2':
                $page = '@app/views/timetables/grade-2-timetable';
                $title = "Grade Two Timetable";
                break;

            case 'grade3':
                $page = '@app/views/timetables/grade-3-timetable';
                $title = "Grade Three Timetable";
                break;
            
            case 'grade4':
                $page = '@app/views/timetables/grade-4-timetable';
                $title = "Grade Four Timetable";
                break;
            
            case 'grade5':
                $page = '@app/views/timetables/grade-5-timetable';
                $title = "Grade Five Timetable";
                break;

            default:
                # code...
                break;
        }


        return $this->render($page, ['title' => $title]);
    }

    public function actionGetStateLgas()
    {
        //$this->enableCsrfValidation = false;
        $lgas = Lgas::find()->where(['state_id' => $_POST['id']])->all();
        $list = '';

        foreach($lgas as $lga)
        {
            $list .= "<option value='".$lga->id."'>".$lga->lga_name."</option>";
        }

        echo $list;

    }

    public function beforeAction($action)
    {            
        if ($action->id == 'get-state-lgas') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
}
