<?php

namespace app\modules\staff\controllers;

use Yii;
use app\models\NurseryReportCard;
use app\models\NurseryReportCardSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Student;
use app\models\GeneralSettings;
use app\models\Session;

/**
 * NurseryReportCardController implements the CRUD actions for NurseryReportCard model.
 */
class NurseryReportCardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NurseryReportCard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        $searchModel = new NurseryReportCardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NurseryReportCard model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NurseryReportCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        //Check if user is class teacher
        $classId = Yii::$app->toolbox->isClassTeacher(Yii::$app->staff->getId());

        if($classId)
        {
            $generalSettings = GeneralSettings::findOne(1);
            $currentSession = Session::findOne($generalSettings->current_session);
            $currentTerm = $generalSettings->current_term;

            $model = new NurseryReportCard();
            $model->created_on = date('Y-m-d H:i:s');
            $model->created_by = Yii::$app->staff->getId();
            $model->class = $model->class;
            $model->term = $currentTerm;
            $model->session = $currentSession->id;
            $model->approval_status = 'pending';

            
            $studentList = Student::getNurseryStudentsDropDownList($classId);            

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
                'studentList' => $studentList,
                'currentSession'=> $currentSession,
                'currentTerm'   => $currentTerm
            ]);
        }

        else
        {
            //Render exception
            return $this->render('@app/modules/staff/views/primary-report-card/reportcard-error', [
                'error' => 'Permission Restriction!',
                'message' => 'You are not enabled to perform this action'
            ]);    
        }
    }

    /**
     * Updates an existing NurseryReportCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        //Check if user is class teacher
        $classId = Yii::$app->toolbox->isClassTeacher(Yii::$app->staff->getId());
        $isAdmin = Yii::$app->toolbox->isAdmin();

        if($classId || $isAdmin)
        {
            $model = $this->findModel($id);
            $generalSettings = GeneralSettings::findOne(1);
    	    $currentSession = Session::findOne($generalSettings->current_session);
    	    $currentTerm = $generalSettings->current_term;


            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
    
            return $this->render('update', [
                'model' => $model,
                'currentSession'=> $currentSession,
        		'currentTerm'	=> $currentTerm
            ]);
        }
        
        else return $this->render('@app/modules/staff/views/default/access-restriction-page');
        
        
    }

    public function actionApproveReportCard($id)
    {
        $model = $this->findModel($id);
        $model->approval_status = 'approved';
        $model->approved_by = Yii::$app->staff->getId();
        $model->approval_date = date('Y-m-d H:i:s');
        $model->save(false);

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Deletes an existing NurseryReportCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NurseryReportCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NurseryReportCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NurseryReportCard::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
