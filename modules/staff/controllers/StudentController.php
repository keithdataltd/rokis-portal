<?php

namespace app\modules\staff\controllers;

use Yii;
use app\models\Student;
use app\models\StudentSearch;
use app\models\SchoolClass;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\models\Lgas;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        $model = new Student();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $imageName = $model->first_name .'_' .rand(0,99999);
            $model->picture = UploadedFile::getInstance($model, 'picture');

            if(!empty($model->picture))
            {
                $model->picture->saveAs('images/student_pix/' . $imageName  . '.' . $model->picture->extension);
                $model->picture = '/images/student_pix/' . $imageName  . '.' . $model->picture->extension; 
            }
            
            $model->status = 'active';
            $model->created_on = date('Y-m-d');

            if($model->save(false)) return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';
        
        $model = $this->findModel($id);
        $currentImage = $model->picture;

        $lgas = empty($model->state_of_origin) ? [] : ArrayHelper::map(Lgas::find()->where(['state_id' => $model->state_of_origin])->all(), 'id', 'lga_name');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $imageName = $model->first_name .'_' .rand(0,99999);
            $model->picture = UploadedFile::getInstance($model, 'picture');

            if(isset($model->picture->extension))
            {
                $model->picture->saveAs('images/student_pix/' . $imageName  . '.' . $model->picture->extension);
                $model->picture = '/images/student_pix/' . $imageName  . '.' . $model->picture->extension; 
            }

            else $model->picture = $currentImage;

            if($model->save(false)) return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'lgas' => $lgas
        ]);
    }

    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAjaxGetClass($studentId)
    {
        $currentClass = Student::findOne($studentId)->current_class;
        
        $class = SchoolClass::findOne($currentClass);

        return $this->asJson([
            'class_id' => $class->id,
            'class_name' => $class->class_name,
            'class_division' => $class->class_division
        ]);
    }

    
}
