<?php

namespace app\modules\staff\controllers;

use Yii;
use app\models\Parents;
use app\models\ParentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ParentStudent;
use app\models\Student;
use app\models\PasswordResetForm;
use app\models\GeneralSettings;

/**
 * ParentsController implements the CRUD actions for Parents model.
 */
class ParentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Parents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        $searchModel = new ParentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Parents model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Parents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        $model = new Parents();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {   
            $passwordString = GeneralSettings::getRandomString();
            $model->password = hash('sha256', $passwordString);
            $model->created_on = date('Y-m-d');

            if($model->save(false))
            {
                Yii::$app->mailer
                ->compose('new-parent-credentials', ['parent' => $model, 'password' => $passwordString])
                ->setFrom('jiddaknight@yahoo.com')
                ->setTo($model->email)        
                ->setSubject('RKIS Parent Portal Profile')
                ->send();
               return $this->redirect(['view', 'id' => $model->id]); 
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Parents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Parents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionLinkWard($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        $model = new ParentStudent();

        $students = Student::find()->orderBy('first_name')->all();
    	$studentList = [];

    	foreach($students as $student)
    	{
    		$studentList[$student->id] = $student->getFullName() . " " . "({$student->getClassName()})";
        }
        
        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $model->creation_date = date('Y-m-d');
            if($model->save(false)) 
                return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('_link_ward_form', 
            [
                'model' => $model, 
                'parent' => $this->findModel($id),
                'studentList' => $studentList
            ]);
    }

    public function actionUnlinkWard($pid, $sid)
    {
        $model = ParentStudent::findOne(['parent_id' => $pid, 'student_id' => $sid])->delete();
        return $this->redirect(['view', 'id' => $pid]);
    }

    public function actionResetPassword($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';
        $parent = Parents::findOne(['id' => $id]);
        

        if(Yii::$app->request->post())
        {
            $passwordString = GeneralSettings::getRandomString();
            $hash = hash('sha256', $passwordString);
            $parent->password = $hash;
           
            if($parent->save(false))
            {
                Yii::$app->session->setFlash('success', "Password reset successful!");

                Yii::$app->mailer
                ->compose('password-reset-notification', ['parent' => $parent, 'password' => $passwordString])
                ->setFrom('info@royalkiddiesschool.com')
                ->setTo($parent->email)        
                ->setSubject('ROKIS Parent Portal Password Reset')
                ->send();

            }

        }
        return $this->render('reset-user-password', ['user' => $parent]);
    }

    /**
     * Finds the Parents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Parents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parents::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
