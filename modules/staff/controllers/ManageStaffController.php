<?php

namespace app\modules\staff\controllers;

use Yii;
use app\models\Staff;
use app\models\StaffSearch;
use app\models\Lgas;
use app\models\PasswordResetForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * ManageStaffController implements the CRUD actions for Staff model.
 */
class ManageStaffController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Staff models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/staffLayout';
        $searchModel = new StaffSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Staff model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = '@app/views/layouts/staffLayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Staff model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '@app/views/layouts/staffLayout';

        if(!Yii::$app->toolbox->isAdmin()) return $this->render('@app/modules/staff/views/default/access-restriction-page');
        
        $model = new Staff();
        $model->scenario = Staff::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {
            $imageName = $model->first_name .'_' .rand(0,99999);
            $model->picture = UploadedFile::getInstance($model, 'picture');

            if(!empty($model->picture))
            {
                $model->picture->saveAs('images/staff_pix/' . $imageName  . '.' . $model->picture->extension);
                $model->picture = '/images/staff_pix/' . $imageName  . '.' . $model->picture->extension; 
            }
            $model->password = hash('sha256', $model->password);
            $model->creation_date = date('Y-m-d');
            $model->status = 'active';

            if($model->save(false))
            {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Staff model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = '@app/views/layouts/staffLayout';
        
        if(!Yii::$app->toolbox->isAdmin()) return $this->render('@app/modules/staff/views/default/access-restriction-page');
        
        $model = $this->findModel($id);
        $model->scenario = Staff::SCENARIO_UPDATE;
        $lgas = empty($model->state_id) ? [] : ArrayHelper::map(Lgas::find()->where(['state_id' => $model->state_id])->all(), 'id', 'lga_name');
        $currentImage = $model->picture;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) 
        {   
            $imageName = $model->first_name  . rand(0,99999);
            $model->picture = UploadedFile::getInstance($model, 'picture');

            if(!empty($model->picture))
            {
                
                $model->picture->saveAs('images/staff_pix/' . $imageName  . '.' . $model->picture->extension);
                $model->picture = '/images/staff_pix/' . $imageName  . '.' . $model->picture->extension; 
            }

            else $model->picture = $currentImage;

            if($model->save(false)) return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'lgas' => $lgas
        ]);
    }

    public function actionResetPassword($id)
    {
        $this->layout = '@app/views/layouts/staffLayout.php';

        if(!Yii::$app->toolbox->isAdmin()) return $this->render('@app/modules/staff/views/default/access-restriction-page');

        $model = new PasswordResetForm();
        $staff = $this->findModel($id);

        if($model->load(Yii::$app->request->post()) && $model->validate())
        {
            $hash = hash('sha256', $model->password);
            $staff->password = $hash;
            
            if($staff->save(false)) Yii::$app->session->setFlash('success', "Password reset successful!");
         
            return $this->render('reset-password', ['model' => $model, 'staff' => $staff]);

        }
        return $this->render('reset-password', ['model' => $model, 'staff' => $staff]);
    }

    /**
     * Deletes an existing Staff model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Staff model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Staff the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Staff::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
